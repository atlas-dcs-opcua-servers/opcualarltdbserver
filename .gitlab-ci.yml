
variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: recursive

stages:
- setup
- config
- generate
- diagram
- generate_doc
- build
- package

.platform_matrix_template: &platform_matrix_definition
  parallel:
    matrix:
      - PLATFORM_TAG: "x86_64-el9"
        PLATFORM_TAG_ALT: "x86_64-alma9"

setup:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: setup
  script:
    - source setup.${PLATFORM_TAG}.sh
  <<: *platform_matrix_definition
    
config-uasdk:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: config
  script:
    - source setup.${PLATFORM_TAG}.sh
    - ./quasar.py set_build_config config-toolkit165-OpcUaLarLtdb-${PLATFORM_TAG}.cmake
    - ./quasar.py prepare_build
  <<: *platform_matrix_definition

diagram:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: diagram
  script:
    - ./quasar.py generate diagram 3
  artifacts:
    name: "Diagram-$CI_JOB_NAME-$CI_PIPELINE_ID"
    expire_in: 1 week
    paths:
      - Design/diagram.pdf
  <<: *platform_matrix_definition

generate_doc:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: generate_doc
  script:
    - ./quasar.py generate config_doc
    - ./quasar.py generate as_doc
    - ./quasar.py doxygen
    - cd Documentation/DoxygenGenerated/latex/
    - make pdf
  artifacts:
    name: "LAR LTDB OPC UA Config Schema and Address Space Documentation"
    paths:
      - Documentation/ConfigDocumentation.html
      - Documentation/AddressSpaceDoc.html
      - Documentation/DoxygenGenerated/latex/refman.pdf
  <<: *platform_matrix_definition

build-uasdk:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: build
  dependencies:
    - diagram
    - generate_doc
  script: 
    - source setup.${PLATFORM_TAG}.sh
    - ./quasar.py set_build_config config-toolkit165-OpcUaLarLtdb-${PLATFORM_TAG}.cmake
    - ./quasar.py build
  artifacts:
    name: "OpcUaLarLtdb-$CI_COMMIT_REF_NAME"
    paths:
      - build/bin/OpcUaLarLtdbServer 
      - bin/ServerConfig.xml
      - configGenerator/ConfigGenerator.sh
      - build/Configuration/Configuration.xsd
      - Design/diagram.pdf
      - Documentation/ConfigDocumentation.html
      - Documentation/AddressSpaceDoc.html
      - Documentation/DoxygenGenerated/latex/refman.pdf
  <<: *platform_matrix_definition

build-uasdk-dbg:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: build
  dependencies:
    - diagram
    - generate_doc
  script: 
    - source setup.${PLATFORM_TAG}.sh
    - ./quasar.py set_build_config config-toolkit165-OpcUaLarLtdb-${PLATFORM_TAG}-dbg.cmake
    - ./quasar.py build
  artifacts:
    name: "OpcUaLarLtdb-$CI_COMMIT_REF_NAME"
    paths:
      - build/bin/OpcUaLarLtdbServer 
      - bin/ServerConfig.xml
      - configGenerator/ConfigGenerator.sh
      - build/Configuration/Configuration.xsd
      - Design/diagram.pdf
      - Documentation/ConfigDocumentation.html
      - Documentation/AddressSpaceDoc.html
      - Documentation/DoxygenGenerated/latex/refman.pdf
  <<: *platform_matrix_definition

package-uasdk:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: package
  needs:
    - job: build-uasdk
      optional: true
  script:
    - ./make_distro.sh $CI_COMMIT_REF_NAME
  environment:
    name: release
  artifacts:
    name: "OpcUaLarLtdb-$CI_COMMIT_REF_NAME-package"
    when: on_success
    expire_in: 1 week
    paths:
      - OpcUaLarLtdbServer-$CI_COMMIT_REF_NAME.tar.gz
  <<: *platform_matrix_definition

package-uasdk-dbg:
  image: gitlab-registry.cern.ch/atlas-dcs-common-software/docker-runners/lar-ltdb-opc-ua.${PLATFORM_TAG}
  stage: package
  needs:
    - job: build-uasdk-dbg
      optional: true
  script:
    - ./make_distro.sh $CI_COMMIT_REF_NAME
  environment:
    name: debug
  artifacts:
    name: "OpcUaLarLtdb-$CI_COMMIT_REF_NAME-package-dbg"
    when: on_success
    expire_in: 1 week
    paths:
      - OpcUaLarLtdbServer-${PLATFORM_TAG}-$CI_COMMIT_REF_NAME.tar.gz
  <<: *platform_matrix_definition

