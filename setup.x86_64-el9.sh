# Usage: source setup.sh

  # using consistent LCG installation for the different packages we take from LCG
  LCG_version="LCG_104c_ATLAS_1"
  GCC_version="x86_64-el9-gcc13-opt"

  TDAQ_VERSION="11-02-00"
  if [[ ${HOSTNAME} == *"pc-lar-felix-ltdb"* ]]; then
    # paths valid at point 1
    echo "Setup paths for P1 servers (without cvmfs access)"
	source /det/lar/project/LTDB/setup.sh
    PATH_TO_LCG=/sw/atlas/sw/lcg/releases/
    PATH_TO_TDAQ_COMMON=/sw/atlas/tdaq-common/tdaq-common-${TDAQ_VERSION}/
  else
    # paths valid elsewhere
    echo "Setup paths for servers outside P1 (with cvmfs access)"
    PATH_TO_LCG=/cvmfs/sft.cern.ch/lcg/releases/
    PATH_TO_TDAQ_COMMON=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-${TDAQ_VERSION}/
    # the latter are only needed for compilation, which we don't do at point 1 anyway
    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.26.2/Linux-x86_64/bin:$PATH
    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/gcc/13/x86_64-el9-gcc13-opt/bin:$PATH
    export PATH=/cvmfs/sft.cern.ch/lcg/releases/binutils/2.40-acaab/x86_64-el9/bin:$PATH
  fi

  # setup appropriate python version - coral needs python 3.9/ Don't setup python in alma9
  ##export PYTHON_LIB_PATH=${PATH_TO_LCG}Python/3.9.6-b0f98/x86_64-centos9-gcc11-opt/lib
  ##export PYTHON_INCLUDE_PATH=${PATH_TO_LCG}Python/3.9.6-b0f98/x86_64-centos9-gcc11-opt/include

  # protobuf
  export PROTOBUF_HEADERS=${PATH_TO_LCG}/protobuf2/2.5.0-47665/x86_64-el9-gcc13-opt/include
  export PROTOBUF_DIRECTORIES=${PATH_TO_LCG}/protobuf2/2.5.0-47665/x86_64-el9-gcc13-opt/lib
  export PROTOBUF_LIBS="-lprotobuf"

  # compiler and boost
  export CXX="g++"
  export CC="gcc"
  export BOOST_PATH=${PATH_TO_LCG}${LCG_version}/Boost/1.82.0/${GCC_version}/

  # xercesc
  export XERCES_DIRECTORIES=${PATH_TO_LCG}${LCG_version}/XercesC/3.2.4/${GCC_version}/lib
  export XERCES_HEADERS=${PATH_TO_LCG}${LCG_version}/XercesC/3.2.4/${GCC_version}/include

  # opcua toolkit
  export OPCUA_TOOLKIT_PATH=/winccoa/rpms/toolkits/uasdk-1.6.5-gcc11.3.0-with_OPCUA-2561-el9

  # commond libs and includes from tdaq
  export TDAQ_EXTERNALS_LIB_PATH=${PATH_TO_TDAQ_COMMON}installed/external/${GCC_version}/lib
  export TDAQ_EXTERNALS_INCLUDE_PATH=${PATH_TO_TDAQ_COMMON}installed/external/${GCC_version}/include

  # to use COOL and CORAL
  export COOL_LIB_PATH=${TDAQ_EXTERNALS_LIB_PATH}
  export COOL_INCLUDE_PATH=${TDAQ_EXTERNALS_INCLUDE_PATH}
  export CORAL_LIB_PATH=${TDAQ_EXTERNALS_LIB_PATH}
  export CORAL_INCLUDE_PATH=${TDAQ_EXTERNALS_INCLUDE_PATH}

  # specific versions of oracle, mysql, and frontier_client needed by CORAL
  export ORACLE_LIB_PATH=${PATH_TO_LCG}${LCG_version}/oracle/19.19.0.0.0/${GCC_version}/lib
  export ORACLE_INCLUDE_PATH=${PATH_TO_LCG}${LCG_version}/oracle/19.19.0.0.0/${GCC_version}/include
  export MYSQL_LIB_PATH=${PATH_TO_LCG}${LCG_version}/mysql/10.5.20/${GCC_version}/lib
  export MYSQL_INCLUDE_PATH=${PATH_TO_LCG}${LCG_version}/mysql/10.5.20/${GCC_version}/include
  export FRONTIER_LIB_PATH=${PATH_TO_LCG}${LCG_version}/frontier_client/2.10.2/${GCC_version}/lib
  export FRONTIER_INCLUDE_PATH=${PATH_TO_LCG}${LCG_version}/frontier_client/2.10.2/${GCC_version}/include

  ### update LD_LIBRARY_PATH with libraries needed at runtime
  export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/contrib/gcc/13/${GCC_version}/lib64/:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${COOL_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${BOOST_PATH}/lib:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${XERCES_DIRECTORIES}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${ORACLE_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${MYSQL_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${FRONTIER_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${PYTHON_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${TDAQ_EXTERNALS_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/prod/installed/external/x86_64-el9-gcc13-opt/lib:$LD_LIBRARY_PATH
  
  export LD_LIBRARY_PATH=${PROTOBUF_DIRECTORIES}:$LD_LIBRARY_PATH
  #export LD_LIBRARY_PATH=/det/lar/project/LTDB/OpcUaLarLtdbServer/protobuf/:$LD_LIBRARY_PATH
  #export LD_LIBRARY_PATH=${PATH_TO_LCG}/protobuf2/2.5.0-47665/x86_64-el9-gcc13-opt/lib/:$LD_LIBRARY_PATH

  # the last export is needed due to gcc version available by default at point 1
  if [[ ${HOSTNAME} == *"pc-lar-felix-ltdb"* ]]; then
    export LD_LIBRARY_PATH=/sw/atlas/tdaq/production/installed/external/${GCC_version}/lib:$LD_LIBRARY_PATH
  fi


# #added with timothee 26 janvier 2024
# export LD_LIBRARY_PATH=/usr/lib64/:$LD_LIBRARY_PATH