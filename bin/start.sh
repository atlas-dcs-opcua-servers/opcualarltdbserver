#!/bin/bash
source setup-gcc11.sh
SERVICE="OpcUaLarLtdbSer"
PROGRAM_NAME="./OpcUaLarLtdbServer"
OPTIONS=""
CONF_FILE_NEEDED=true

PID=`ps -e | sed -n /${SERVICE}/p | awk '{ print $1 }'`
now=$(date +"%y-%m-%d-%H-%M")

if [ $(hostname) = pc-lar-felix-ltdb-spare.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-00.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-01.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-02.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-03.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-04.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-05.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-06.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pc-lar-felix-ltdb-07.cern.ch ];then
  PROGRAM_NAME="./OpcUaLarLtdbServer"
  LOG="/data/logs/$SERVICE"
fi
if [ $(hostname) = pcatllar05.cern.ch ];then
  echo "Error wrong machine"
  exit 0;
fi


if [ -e $LOG/.last_pid ];then
  LAST_PID=`cat $LOG/.last_pid`
  TEST=`ps -e |grep -v grep| grep " $LAST_PID"`
fi
echo "Launching of $PROGRAM_NAME"

if [ $CONF_FILE_NEEDED = "true" ]; then
  if [ -z "$1" ];then
    echo "Missing Configuration , default one will be used"
    #OPTIONS="$OPTIONS configs/OpcUaLarLtdb-experts/$(hostname | awk -F '.' '{print $1}').xml"
    OPTIONS="$OPTIONS configs/OpcUaLarLtdb-default/$(hostname | awk -F '.' '{print $1}').xml"
  fi
    OPTIONS="$OPTIONS $1"
fi
if [ -e $LOG ];then
  echo "Log file found: $LOG"
else
  echo "ERR Log file not exist: $LOG"
    exit 1;
fi
if [ -z "$TEST" ];then
# NOT RUNNING WITH LAST PID
  if [ "${PID:-null}" = null ];then
   #NOT RUNNING AT ALL
      ###### START 
      set -m
      nohup $PROGRAM_NAME $OPTIONS > "$LOG/log-$now.log" &
      PID=$!
      echo "PID: $PID"
      echo "$PID" > $LOG/.last_pid
      echo "$PROGRAM_NAME $OPTIONS"
      echo "Logs created: $LOG/log-$now.log"
  else
    # RUNNING BUT PID!=LAST_PID
    echo "$PROGRAM_NAME already started"
    echo -e "\033[33mWARNING $PROGRAM_NAME is running without have been launched by this script\033[0m"
    echo "Running with PID: $PID"
  fi
else
# RUNNING WITH LAST PID
  echo "$PROGRAM_NAME already started"
  echo "Running with PID: $LAST_PID"
fi
