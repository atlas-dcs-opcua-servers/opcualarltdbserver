set(CUSTOM_SERVER_MODULES UaoClientForOpcUaSca)
set(EXECUTABLE OpcUaLarLtdbServer)
set(SERVER_INCLUDE_DIRECTORIES 
  /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/prod/installed/external/x86_64-el9-gcc13-opt/include  
  ${LTDBSW_HEADERS}
 	${COOL_INCLUDE_DIR}
	${CORAL_INCLUDE_DIR}
	${ORACLE_INCLUDE_DIR}
	${MYSQL_INCLUDE_DIR}
	${FRONTIER_INCLUDE_DIR}
	${PYTHON_HEADERS}
)
set(SERVER_LINK_LIBRARIES 
  libcactus_uhal_uhal.so 
  libcactus_uhal_log.so 
  ${LTDBSW_LIBS}
  ${COOL_LINK_LIBRARIES}
  ${CORAL_LINK_LIBRARIES}
)
#set(SERVER_LINK_LIBRARIES cactus_uhal_uhal cactus_uhal_log boost_program_options ${LTDBSW_LIBS})
set(SERVER_LINK_DIRECTORIES  
  /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/prod/installed/external/x86_64-el9-gcc13-opt/lib 
  ${LTDBSW_LIBDIRS}
	${COOL_LINK_DIR}
	${CORAL_LINK_DIR}
	${ORACLE_LINK_DIR}
	${MYSQL_LINK_DIR}
	${FRONTIER_LINK_DIR}
	${PYTHON_LIBDIRS}
)


