#ifndef COLOR_LOG_H_INCLUDED
#define COLOR_LOG_H_INCLUDED

class COLORLOG{
  public:
   enum Color{RED=31,GREEN=32,YELLOW=33,BLUE=34,MAGENTA=35,CYAN=36,RESET=0};
  static std::string color(std::string input, const Color colcode , bool bold=false);
};



#endif




