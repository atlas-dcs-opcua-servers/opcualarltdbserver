#ifndef STRESS_H_INCLUDED
#define STRESS_H_INCLUDED

#include <LogIt.h>
#include <iostream>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>
#include <string>

///stress fonction
UaStatus stress (string testNb, string ltdbId, string nbStress, UaClientSdk::UaSession* session);

UaStatus stressMTx(string ltdbId, int nbStress,UaClientSdk::UaSession* session);

UaStatus stressLOCx2(string ltdbId, int nbStress, UaClientSdk::UaSession* session);

UaStatus stressADC(string ltdbId, int nbStress, UaClientSdk::UaSession* session);

UaStatus stressADCTest2(string ltdbId, int nbStress,UaClientSdk::UaSession* session);


///fonction
UaStatus secureI2CWrite( UaoClientForOpcUaSca::I2cSlave reg , OpcUa_Byte value, std::string nodeId, int MaxRetry =5 , int flags =0 );

UaStatus switchTo10Bits (UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id);

UaStatus fakeRead (string ltdbId, string scaId, string locx2Id, string adcId, UaClientSdk::UaSession* session);

UaStatus mtx_configure (string ltdbId, string scaId, string mtxId, UaClientSdk::UaSession* session);

UaStatus mtx_checkConfig(string ltdbId, string scaId, string mtxId, int & result,UaClientSdk::UaSession* session);

UaStatus locx2_configure(string ltdbId, string scaId, string locxId, UaClientSdk::UaSession* session);

UaStatus locx2_checkConfig(string ltdbId, string scaId, string locxId, int & result,UaClientSdk::UaSession* session);

UaStatus adc_resetADC(string ltdbId, string scaId, string locxId, string adcId, UaClientSdk::UaSession* session);

UaStatus adc_writeADC (string ltdbId, string scaId, string locxId, string adcId, OpcUa_UInt64 value,UaClientSdk::UaSession* session);

UaStatus adc_readADC(string ltdbId, string scaId, string locxId, string adcId, OpcUa_UInt64 & value,UaClientSdk::UaSession* session);

UaStatus adc_writeAndCheckADC (string ltdbId, string scaId, string locx2Id, string adcId, OpcUa_UInt64 value,UaClientSdk::UaSession* session);




Log::LogComponentHandle logHandler;

enum
{
	I2C_IGNORE_ERROR = 1 << 0,
    I2C_NO_READBACK = 1 << 2,
};

#endif
