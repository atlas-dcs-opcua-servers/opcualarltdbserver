#include <iostream>
#include <fstream>
#include <cmath>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <LogIt.h>                  //Log librairy
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;

UaStatus valueFromCalibFiles(string filename,string nodeId_read, string nodeId_write,UaClientSdk::UaSession* session);
UaStatus writeAndCheckADC (string nodeId_read, string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session);
UaStatus readADC(string nodeId_read,OpcUa_UInt64 & value,UaClientSdk::UaSession* session);
UaStatus writeADC (string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session);
UaStatus writeValueAdc (string nodeId_read, 
            string nodeId_write,
            OpcUa_Byte numChannel,
            OpcUa_UInt16 MDAC1_C0,
            OpcUa_UInt16 MDAC2_C0,
            OpcUa_UInt16 MDAC3_C0,
            OpcUa_UInt16 MDAC4_C0,
            OpcUa_UInt16 MDAC1_C1,
            OpcUa_UInt16 MDAC2_C1,
            OpcUa_UInt16 MDAC3_C1,
            OpcUa_UInt16 MDAC4_C1,
            UaClientSdk::UaSession* session);
UaStatus DLtdbADCNevis::resetADC (std::string nodeId_write,std::string nodeId_read,UaClientSdk::UaSession* session,std::string baseNodeId,std::string locx2Id,std::string adcId);
UaStatus writeAndCheckADCWithoutInterLeave (string nodeId_read,string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session);
string formatNcaracters(string num, int size_format);
std::string tostr (int x);
std::vector<std::string> tokenize(std::string value,  std::string separator);
UaStatus secureSTF (std::string svalue1, float &output);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help"          , "Set values in files found in configFilePath to all ADCs")
    ("address"       , po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("configFilePath", po::value<string>()->default_value("/localdisk/OPC-PROJECTS/OpcUaLarLtdbServer/ltdb_calibration_files"),"File containing the values to set all ADCs in lardcs@pcatllarltdb")
    ("ltdbSN"        , po::value<string>()->default_value("1"), "ltdb serialNumber")
    ("ltdbId"        , po::value<string>()->default_value("1"), "ltdb identifier")
    ("scaId"         , po::value<string>()->default_value("1"), "sca identifier")
    ("locx2Id"       , po::value<string>()->default_value("1"), "locx2 identifier")
    ("adcId"         , po::value<string>()->default_value("1"), "adc identifier")
    ("adcNb"         , po::value<string>()->default_value("1"), "adc total number")

    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }

    
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    //LOG(Log::INF) << logID <<fileName;
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    LOG(Log::INF) << logID <<"-------------------";
    LOG(Log::INF) << logID <<" WRITE ADC  ";
    LOG(Log::INF) << logID <<"-------------------";

    string baseNodeId   = "ltdb_"+vm["ltdbId"].as<string>()+".scaFelix"+vm["scaId"].as<string>();
    string nodeId_read  = baseNodeId +".BusADC_LOCx2_" + vm["locx2Id"].as<string>() +".read_adc"  + vm["adcId"].as<string>();
    string nodeId_write = baseNodeId +".BusADC_LOCx2_" + vm["locx2Id"].as<string>() +".write_adc" + vm["adcId"].as<string>();
    //This path exists in lardcs@pcatllarltdb
    //std::string fileName= vm["configFilePath"].as<string>() + "/ltdb"+ formatNcaracters(vm["ltdbId"].as<string>(),3)+"/"+ "ADC_constants_" + vm["adcId"].as<string>() +".csv";
    std::string fileName= vm["configFilePath"].as<string>()+ "/ltdb"+ formatNcaracters(vm["ltdbSN"].as<string>(),3)+ "/ADC_constants_" + vm["adcNb"].as<string>() +".csv";
    LOG(Log::INF) << " Full path : " << fileName << endl;

    s = resetADC (nodeId_write,nodeId_read,session,baseNodeId,locx2Id,adcId)
    if (s.isGood()){
        LOG(Log::INF) << " The ADC has been successfully updated" << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : The ADC hasn't been reseted ";

    s = valueFromCalibFiles(fileName,nodeId_read,nodeId_write,session);
    if (s.isGood()){
        LOG(Log::INF) << " The ADC has been successfully updated" << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : The ADC hasn't been updated ";
    
    delete session;
    return 0;
}
UaStatus DLtdbADCNevis::fakeRead (std::string nodeId_write,std::string nodeId_read,UaClientSdk::UaSession* session){
	  OpcUa_UInt64 writeValue =0xA024000000000000;
	  OpcUa_UInt64 readValue;
	  UaStatus s =writeADC(nodeId_write,writeValue,session);
	  if (!s.isGood()){
		  return s;
	  }
	  s =readADC(nodeId_read,readValue,session);
	  if (!s.isGood()){
		  return s;
	  }
	  return OpcUa_Good;
  }

  UaStatus DLtdbADCNevis::switchTo10Bits (UaClientSdk::UaSession* session,std::string locx2Id,std::string adcId)
    {
  	  UaByteString bs;
  	  OpcUa_Byte oneByte;
  	  oneByte=0x20;
  	  try{

		  std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2Id + ".ctrl_adc" + adcId;
		  UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
		  //Not using secure I2C write because the register is write only
		  bs.setByteString(sizeof oneByte,&oneByte);
		  registerId.writeValue(bs);
  	  }
  		catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
			LOG(Log::ERR) << logID << logID <<"Caught(BadStatusCode):"<<e.what();
			return e.statusCode();}
  	 	catch(const std::exception& e){
			LOG(Log::ERR) << logID << logID <<"Caught:"<<e.what();
			return OpcUa_Bad;
  	  }
  	  return OpcUa_Good;
    }

 UaStatus DLtdbADCNevis::resetADC (std::string nodeId_write,std::string nodeId_read,UaClientSdk::UaSession* session,std::string baseNodeId,std::string locx2Id,std::string adcId)
	{
	//scaFelix1.gpio.RSTB_ADCL1A1
	  OpcUa_Boolean trueValue =1;
	  OpcUa_Boolean falseValue =0;
	  UaStatus s = OpcUa_Bad;
	  try{
		   std::string nodeId;
		  nodeId=baseNodeId + ".gpio.RSTB_ADCL"+locx2Id+"A"+adcId;
		  //LOG(Log::INF)<<"Reset ADC:"<<nodeId;
		  UaoClientForOpcUaSca::DigitalIO Adcl(session,UaNodeId(nodeId.c_str(),2));
		  //Reset
		  Adcl.writeValue(trueValue);
		  Adcl.writeValue(falseValue);
		  Adcl.writeValue(trueValue);
		  //Switch to 10 bits
		  s = switchTo10Bits(session,locx2Id,adcId);
		  if (!s.isGood()){return s;}
		  //fake read
		  s = fakeRead(nodeId_write,nodeId_read,session);
		  if (!s.isGood()){return s;}


	  }
	  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
		  if (e.statusCode()==OpcUa_BadResourceUnavailable){
			  LOG(Log::TRC) << logID  <<"Caught(BadStatusCode):"<<e.what();
		  }
		  else{
			  LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
		  }
	  	  return e.statusCode();
	    }
	  catch(const std::exception& e){
		  LOG(Log::ERR) << logID<<"Caught:"<<e.what();
		  return OpcUa_Bad;
	  }
	  return OpcUa_Good;

	}

UaStatus valueFromCalibFiles(string fileName, string nodeId_read, string nodeId_write, UaClientSdk::UaSession* session){
    //std::string fileName=getParent()->getParent()->getParent()->calibFilesPath() + "ADC_constants_" + std::to_string(totalAdcNum) +".csv";
    //std::string fileName= fileName + "ltdb"+ formatNcaracters(parameters[0],3)+"/"+ "ADC_constants_" + parameters[3] +".csv";

    LOG(Log::INF) << logID << "  ValueFromCalibFiles file:" <<fileName;

    OpcUa_UInt16 MDAC1_C0;
    OpcUa_UInt16 MDAC2_C0;
    OpcUa_UInt16 MDAC3_C0;
    OpcUa_UInt16 MDAC4_C0;
    OpcUa_UInt16 MDAC1_C1;
    OpcUa_UInt16 MDAC2_C1;
    OpcUa_UInt16 MDAC3_C1;
    OpcUa_UInt16 MDAC4_C1;

    std::string line;

    float ReadMDAC1_C0;
    float ReadMDAC2_C0;
    float ReadMDAC3_C0;
    float ReadMDAC4_C0;
    float ReadMDAC1_C1;
    float ReadMDAC2_C1;
    float ReadMDAC3_C1;
    float ReadMDAC4_C1;
    UaStatus s = OpcUa_Bad;
    std::ifstream file(fileName.c_str(), std::ios::in);
    int channel=0;
    if (file){
     while (getline(file, line) ){

       std::vector <std::string> tokens = tokenize(line,",");
       if(!tokens.size()) continue;
       if(tokens.size()!=8){
         LOG(Log::ERR)<< logID << "Error:Wrong formatting of the file :" << fileName << ": It must be 8 values";
         return OpcUa_BadInternalError;
       }
       channel++;
       if (channel >4){
         LOG(Log::ERR)<< logID << "Error: more than 4 channel found in file"<< fileName;
         return OpcUa_BadInternalError;
       }
       LOG(Log::TRC) << logID << "Reading the MDAQ for channel:"<< channel;
       s=secureSTF(tokens[0],ReadMDAC1_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[2],ReadMDAC2_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[4],ReadMDAC3_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[6],ReadMDAC4_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[1],ReadMDAC1_C1);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[3],ReadMDAC2_C1);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[5],ReadMDAC3_C1);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[7],ReadMDAC4_C1);
       if (!s.isGood()){return s;}

        LOG(Log::TRC)<< "ReadMDAC1_C0:"<<ReadMDAC1_C0;
        LOG(Log::TRC)<< "ReadMDAC2_C0:"<<ReadMDAC2_C0;
        LOG(Log::TRC)<< "ReadMDAC3_C0:"<<ReadMDAC3_C0;
        LOG(Log::TRC)<< "ReadMDAC4_C0:"<<ReadMDAC4_C0;
        LOG(Log::TRC)<< "ReadMDAC1_C1:"<<ReadMDAC1_C1;
        LOG(Log::TRC)<< "ReadMDAC2_C2:"<<ReadMDAC2_C1;
        LOG(Log::TRC)<< "ReadMDAC3_C3:"<<ReadMDAC3_C1;
        LOG(Log::TRC)<< "ReadMDAC4_C4:"<<ReadMDAC4_C1;

        ReadMDAC1_C0 = std::round(ReadMDAC1_C0);
        ReadMDAC2_C0 = std::round(ReadMDAC2_C0);
        ReadMDAC3_C0 = std::round(ReadMDAC3_C0);
        ReadMDAC4_C0 = std::round(ReadMDAC4_C0);
        ReadMDAC1_C1 = std::round(ReadMDAC1_C1);
        ReadMDAC2_C1 = std::round(ReadMDAC2_C1);
        ReadMDAC3_C1 = std::round(ReadMDAC3_C1);
        ReadMDAC4_C1 = std::round(ReadMDAC4_C1);

        LOG(Log::TRC)<< "ReadMDAC1_C0 rounded:"<<ReadMDAC1_C0;
        LOG(Log::TRC)<< "ReadMDAC2_C0 rounded:"<<ReadMDAC2_C0;
        LOG(Log::TRC)<< "ReadMDAC3_C0 rounded:"<<ReadMDAC3_C0;
        LOG(Log::TRC)<< "ReadMDAC4_C0 rounded:"<<ReadMDAC4_C0;
        LOG(Log::TRC)<< "ReadMDAC1_C1 rounded:"<<ReadMDAC1_C1;
        LOG(Log::TRC)<< "ReadMDAC2_C2 rounded:"<<ReadMDAC2_C1;
        LOG(Log::TRC)<< "ReadMDAC3_C3 rounded:"<<ReadMDAC3_C1;
        LOG(Log::TRC)<< "ReadMDAC4_C4 rounded:"<<ReadMDAC4_C1;

        MDAC1_C0 = (int) ReadMDAC1_C0;
        MDAC2_C0 = (int) ReadMDAC2_C0;
        MDAC3_C0 = (int) ReadMDAC3_C0;
        MDAC4_C0 = (int) ReadMDAC4_C0;
        MDAC1_C1 = (int) ReadMDAC1_C1;
        MDAC2_C1 = (int) ReadMDAC2_C1;
        MDAC3_C1 = (int) ReadMDAC3_C1;
        MDAC4_C1 = (int) ReadMDAC4_C1;

        LOG(Log::TRC)<< "MDAC1_C0:"<<MDAC1_C0 << "(0x" << std::hex << MDAC1_C0 << ")" ;
        LOG(Log::TRC)<< "MDAC2_C0:"<<MDAC2_C0<< "(0x" << std::hex << MDAC2_C0 << ")" ;;
        LOG(Log::TRC)<< "MDAC3_C0:"<<MDAC3_C0<< "(0x" << std::hex << MDAC3_C0 << ")" ;;
        LOG(Log::TRC)<< "MDAC4_C0:"<<MDAC4_C0<< "(0x" << std::hex << MDAC4_C0 << ")" ;;
        LOG(Log::TRC)<< "MDAC1_C1:"<<MDAC1_C1<< "(0x" << std::hex << MDAC1_C1 << ")" ;;
        LOG(Log::TRC)<< "MDAC2_C2:"<<MDAC2_C1<< "(0x" << std::hex << MDAC2_C1 << ")" ;;
        LOG(Log::TRC)<< "MDAC3_C3:"<<MDAC3_C1<< "(0x" << std::hex << MDAC3_C1 << ")" ;;
        LOG(Log::TRC)<< "MDAC4_C4:"<<MDAC4_C1<< "(0x" << std::hex << MDAC4_C1 << ")" ;;

        s=writeValueAdc(nodeId_read,nodeId_write,channel,MDAC1_C0,MDAC2_C0, MDAC3_C0, MDAC4_C0, MDAC1_C1,MDAC2_C1,MDAC3_C1, MDAC4_C1,session);
        if (!s.isGood()){return s;}
        LOG(Log::TRC)<< "Values read :" << MDAC1_C0<<"|"<<MDAC2_C0<<"|"<<MDAC3_C0<<"|"<<MDAC4_C0<<"|"<<MDAC1_C1<<"|" <<MDAC2_C1<<"|" << MDAC3_C1<<"|"<<MDAC4_C1;
      }
     if (channel !=4){
       LOG(Log::ERR)<< logID << "Error: less than 4 channel found in file" << fileName;
       return OpcUa_BadInternalError;
     }
      file.close();
      return OpcUa_Good;
    }
    else {
      LOG(Log::ERR) << logID  << " File not found:";
      return OpcUa_Bad;
    }
  }

  UaStatus writeValueAdc (string nodeId_read,
              string nodeId_write,
              OpcUa_Byte numChannel,
              OpcUa_UInt16 MDAC1_C0,
              OpcUa_UInt16 MDAC2_C0,
              OpcUa_UInt16 MDAC3_C0,
              OpcUa_UInt16 MDAC4_C0,
              OpcUa_UInt16 MDAC1_C1,
              OpcUa_UInt16 MDAC2_C1,
              OpcUa_UInt16 MDAC3_C1,
              OpcUa_UInt16 MDAC4_C1,
              UaClientSdk::UaSession* session)
  {
    UaStatus s = OpcUa_Bad;
    OpcUa_UInt16 mask30=0x000f;
    OpcUa_UInt16 mask70=0x00ff;
    OpcUa_UInt16 mask114=0x0ff0;
    OpcUa_UInt16 mask118=0x0f00;
    OpcUa_UInt64 writeRegister1;
    OpcUa_UInt64 writeRegister2;
    OpcUa_UInt64 writeRegisterConfig;
    OpcUa_UInt64 writeRegisterControlMDAC;
    switch(numChannel){
        case 1:
        writeRegister1     = 0xA034000000000000;
        writeRegister2     = 0xA035000000000000;
        writeRegisterControlMDAC= 0xA036000000000000;
        writeRegisterConfig = 0xA01A000000000000;
      break;
        case 2:
      writeRegister1     = 0xA054000000000000;
      writeRegister2     = 0xA055000000000000;
      writeRegisterControlMDAC= 0xA056000000000000;
      writeRegisterConfig = 0xA01A000000000000;
      break;
        case 3:
      writeRegister1     = 0x6034000000000000;
      writeRegister2     = 0x6035000000000000;
      writeRegisterControlMDAC= 0x6036000000000000;
      writeRegisterConfig = 0x601A000000000000;
      break;
        case 4:
      writeRegister1     = 0x6054000000000000;
      writeRegister2     = 0x6055000000000000;
      writeRegisterControlMDAC= 0x6056000000000000;
      writeRegisterConfig = 0x601A000000000000;
      break;
        default:
        LOG(Log::ERR) << logID <<"wrong channel number: "<<numChannel;
        return OpcUa_Bad;
    }
    writeRegisterControlMDAC+=0x80;
    writeRegister1+=((OpcUa_UInt64)MDAC1_C0)<<36;
    writeRegister1+=((OpcUa_UInt64)MDAC2_C0)<<24;
    writeRegister1+=((OpcUa_UInt64)MDAC3_C0)<<12;
    writeRegister1+=((OpcUa_UInt64)MDAC4_C0);
    writeRegister2+=((OpcUa_UInt64)MDAC1_C1)<<36;
    writeRegister2+=((OpcUa_UInt64)MDAC2_C1)<<24;
    writeRegister2+=((OpcUa_UInt64)MDAC3_C1)<<12;
    writeRegister2+=((OpcUa_UInt64)MDAC4_C1);

    s=writeAndCheckADC(nodeId_read,nodeId_write,writeRegister1,session);
    s=writeAndCheckADC(nodeId_read,nodeId_write,writeRegister2,session);
    s=writeAndCheckADCWithoutInterLeave(nodeId_read,nodeId_write,writeRegisterControlMDAC,session);
    if (!s.isGood()){return s;}
    s=writeAndCheckADCWithoutInterLeave(nodeId_read,nodeId_write,writeRegisterConfig,session);
    if (!s.isGood()){return s;}
    return OpcUa_Good;
  }

UaStatus writeAndCheckADC (string nodeId_read,string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session)
{
  int readError=0, writeError=0;
  int readErrorMAX=5, writeErrorMAX=5;
  LOG(Log::INF)<<"Write and Check data :" <<std::hex<< value;
  OpcUa_UInt64 addressMask = 0xfffe000000000000;
  OpcUa_UInt64 dataMask    = 0x0000ffffffffffff;
  OpcUa_UInt64 idRegMask = 0x0001000000000000;

  OpcUa_UInt64 readingMask1=0x0000000000000fff;
  OpcUa_UInt64 readingMask2=0x0000000000fff000;
  OpcUa_UInt64 readingMask3=0x0000000fff000000;
  OpcUa_UInt64 readingMask4=0x0000fff000000000;

  OpcUa_UInt64 readValue1;
  OpcUa_UInt64 readValue2;
  OpcUa_UInt64 reorderedValue;
  bool pairRegister = !((value & idRegMask)>0);
  OpcUa_UInt64 readingReg1, readingReg2;  
  UaStatus s = OpcUa_Bad;
  readingReg1 = (value & addressMask) - 0x0010000000000000;
  readingReg2 = readingReg1 + 0x0001000000000000;
  while(writeError<writeErrorMAX){
  s =writeADC(nodeId_write,value,session);
  if (!s.isGood()){return s;}
    readError=0;
    while(readError<readErrorMAX){
      s =writeADC(nodeId_write,readingReg1,session);
      if (!s.isGood()){return s;}
      s =readADC(nodeId_read,readValue1,session);
      if (!s.isGood()){return s;}
      s =writeADC(nodeId_write,readingReg2,session);
      if (!s.isGood()){return s;}
      s =readADC(nodeId_read,readValue2,session);
      if (!s.isGood()){return s;}

      if (pairRegister){
        reorderedValue= (readValue1 & readingMask4) + ((readValue1 & readingMask2)<<12) + ((readValue2 & readingMask4)>>24) + ((readValue2 & readingMask2)>>12) ;
      }
      else{
        reorderedValue= ((readValue1 & readingMask3)<<12) + ((readValue1 & readingMask1)<<24) + ((readValue2 & readingMask3)>>12) + ((readValue2 & readingMask1)) ;
      }
      if (reorderedValue==(value & dataMask)){
        return OpcUa_Good;
      }
      readError++;
      LOG(Log::WRN) << logID <<"Read Loop "<< readError <<"Read data("<<std::hex<< reorderedValue <<") don't correspond to written value:"<<value ;
    }
    writeError++;
    LOG(Log::WRN) << logID <<"Write Loop "<< writeError <<"Read data("<<std::hex<< reorderedValue <<") don't correspond to written value:"<<value ;
  }
  LOG(Log::ERR) << logID <<"Data don't correspond to written value:" ;
  return OpcUa_Bad;
}

  UaStatus writeAndCheckADCWithoutInterLeave (string nodeId_read,string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session){
      int readError=0, writeError=0;
      int readErrorMAX=5, writeErrorMAX=5;
      OpcUa_UInt64 addressMask = 0xffff000000000000;
      OpcUa_UInt64 dataMask    = 0x0000ffffffffffff;

      OpcUa_UInt64 readValue;
      OpcUa_UInt64 readingReg;
      UaStatus s = OpcUa_Bad;

      readingReg = (value & addressMask) - 0x0010000000000000;
      while(writeError<writeErrorMAX){
        s =writeADC(nodeId_write,value,session);
        if (!s.isGood()){return s;}
        readError=0;
        while(readError<readErrorMAX){
          s =writeADC(nodeId_write,readingReg,session);
          if (!s.isGood()){return s;}
          s =readADC(nodeId_read,readValue,session);
          if (!s.isGood()){return s;}

          if (readValue==(value & dataMask)){
            return OpcUa_Good;
          }
          readError++;
          LOG(Log::WRN) << logID <<"Read Loop "<< readError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
        }
        writeError++;
        LOG(Log::WRN) << logID <<"Write Loop "<< writeError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
      }
      LOG(Log::ERR) << logID <<"Data don't correspond to written value:" ;
      return OpcUa_Bad;
    }


UaStatus writeADC (string nodeId_write, OpcUa_UInt64 value,UaClientSdk::UaSession* session)
{   
  UaByteString bs;
  OpcUa_Byte tOneByte[8];
  try{
    for (int i=0;i<8;i++){
      tOneByte[7-i]=value & 255;
      value=value >> 8;
    }
   
    LOG(Log::INF) << logID << "writeADC : baseNodeId " + nodeId_write;  
    UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_write.c_str(),2));
    bs.setByteString(sizeof tOneByte,tOneByte);
    registerId.writeValue(bs);
  }
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  }   
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  } 
  return OpcUa_Good;
}

UaStatus readADC(string nodeId_read, OpcUa_UInt64 & value,UaClientSdk::UaSession* session)
  {
    UaByteString bs;
    value=0;
    try{
      UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_read.c_str(),2));
      UaByteString readBs(registerId.readValue());
      if (readBs.length()!=8){
        LOG(Log::ERR) << logID <<"Error reading register "+ nodeId_read;
        return OpcUa_Bad;
      }
      for (int i=0;i<8;i++){
        value = value <<8;
        LOG(Log::INF)<<"data n"<<i <<"="<<(int) readBs.data()[i];
        value+=(int) readBs.data()[i];
        LOG(Log::INF)<<"value ="<<std::hex<< value;
      }
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
      LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
      return e.statusCode();
    }
    catch(const std::exception& e){
      LOG(Log::ERR) << logID <<"Caught:"<<e.what();
      return OpcUa_Bad;
    }
    return OpcUa_Good;
  }

string formatNcaracters(string num, int size_format) {
    string s = num;
    while (s.size() < size_format) s = "0" + s;
    return s;
}

std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}

std::vector<std::string> tokenize(std::string value,  std::string separator){

  std::vector<std::string> tokens;
   std::string::size_type start = 0, end = 0;
   while ((end = value.find(separator, start)) != std::string::npos){
     std::string token = value.substr(start, end - start);
     if(token.size()) tokens.push_back(token);
     start = end + 1;
   }
   std::string lasttoken = value.substr(start);
   if(lasttoken.size()) tokens.push_back(lasttoken);

   return tokens;

}

  UaStatus secureSTF (std::string svalue1, float &output){

  std::string::size_type pos;
  try{
    output = std::stof(svalue1, &pos);
    if(pos != svalue1.size()){
      LOG(Log::ERR) <<  logID << "ERROR in string format on file";
      return OpcUa_BadInternalError;
    }
  }
  catch(const std::exception& err) {
    LOG(Log::ERR) <<  logID << "ERROR not able to convert";
    return OpcUa_BadInternalError;
  }
  return OpcUa_Good;
  }


