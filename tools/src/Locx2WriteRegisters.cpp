#include <iostream>
#include <sstream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus writeRegisterLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, int locx2_id, int reg_number, OpcUa_Byte reg_value, OpcUa_Boolean setDefVal);
UaStatus secureI2CWrite( UaoClientForOpcUaSca::I2cSlave reg , OpcUa_Byte value, std::string nodeId, int retry = 5);

std::string tostr (int x);

int main(int argc, const char *argv[])
{
  UaPlatformLayer::init();
  char const* warng=   "\n***************************************"
                       "\n@ WARNING! : The setDefaultValue flag @"
                       "\n@ is set true by default. If you want @"
                       "\n@ to set a different value you must   @"
                       "\n@ disable it.                         @"            
                       "\n@------------------------------------ @"
                       "\n@ Default values by register:         @"
                       "\n@     register0   => 0x14,            @"
                       "\n@     register1   => 0x34,            @"
                       "\n@     register2   => 0xE7,            @"
                       "\n@     register3   => 0x9C             @"
                       "\n***************************************";
  po::options_description desc("Allowed options");
  desc.add_options()
  ("help", "Allows writing of each specified Locx2 register")
  ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
  ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
  ("scaId", po::value<string>()->default_value("1"), "sca id")
  ("locx2Id",po::value<string>()->default_value("1"), "LOCx2 identifier")
  ("regNumber",po::value<string>()->default_value("0"),"register number [0 to 3] ")

  //("value",po::value<string>()->default_value("0x0"),"Value to set. Warning: Desable setDefaultValue flag, enabled by default")
  ("value",po::value<string>()->default_value("0"),"Value to set. ")
  ("setDefaultValue",po::value<string>()->default_value("true"),warng)
  ;                 
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
  }
  Log::initializeLogging(Log::INF);
  UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
  if (!session){
    LOG(Log::ERR)<<"Bad communication Error";
    return 0;
  }
  LOG(Log::INF) << logID <<"-----------------------";
  LOG(Log::INF) << logID <<"LOCX2 Write  Register  ";
  LOG(Log::INF) << logID <<"-----------------------";
  LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();

  OpcUa_Byte r_value = (OpcUa_Byte)strtol(vm["value"].as<string>().c_str(), NULL, 16);
  int locId= std::stoi(vm["locx2Id"].as<string>().c_str());
  int regNum= std::stoi(vm["regNumber"].as<string>().c_str());
  bool dv = (vm["setDefaultValue"].as<string>() == "true");
  LOG(Log::INF) << logID <<"locx2Id : " <<  locId;
  //LOG(Log::INF) << logID <<"regNumber : " << regNum;
  LOG(Log::INF) << logID <<"value_ : (0x" << std::hex << (int) r_value << ")";
  LOG(Log::INF) << logID <<"setDefaultValue : " << dv;  
  LOG(Log::INF) << logID <<"-----------------------";
  UaStatus s = OpcUa_Bad;
  s = writeRegisterLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),locId,regNum, r_value, dv);;
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";
  delete session;

  return 0;
}

UaStatus writeRegisterLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, int locx2_id, int reg_number, OpcUa_Byte reg_value, OpcUa_Boolean setDefVal)
{
  OpcUa_Byte baseAddress, busNumber;
  OpcUa_Byte defVal_reg[4]= {0x14,0x34,0xE7,0x9C};
  UaByteString bs; 
  try{

      //Set base address
      if(locx2_id==1 || locx2_id==3){
        baseAddress=0x4;
      }
      else if(locx2_id==2 || locx2_id==4){
        baseAddress=0x8;
      }
      //set bus number
      if(locx2_id ==1 || locx2_id==2){
        busNumber=5;
      }
      else if(locx2_id==3 || locx2_id == 4){
        busNumber=7;
      }
      //Set defaul value, if required
      if(setDefVal){
        reg_value = defVal_reg[reg_number];
      } 
      std::string baseNodeId="ltdb_"+ ltdb_id +".scaFelix"+ sca_id +".BusI2C_" + tostr(busNumber);
      std::string nodeId = baseNodeId+".register0" + tostr(baseAddress+reg_number);

      UaoClientForOpcUaSca::I2cSlave reg_locx2(session,UaNodeId(nodeId.c_str(),2));
      secureI2CWrite(reg_locx2,reg_value,"Unknown");
      //bs.setByteString(sizeof reg_value,&reg_value);
      //reg_locx2.writeValue(bs);
      //UaByteString register_value(reg_locx2.readValue());
      LOG(Log::INF) << logID << "writeRegisterLocx2 : Register_" << tostr(reg_number) + " writing value => (0x" << std::hex << (int) reg_value << ")";
      //LOG(Log::INF) << logID <<"The resetregister was succesfully updated";
  }  
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  return OpcUa_Good;    
}

std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}

UaStatus secureI2CWrite( UaoClientForOpcUaSca::I2cSlave reg , OpcUa_Byte value, std::string nodeId, int retry){
//Changing value of register 4

  UaByteString bs;
  OpcUa_Byte oneByte,readedValue;
  oneByte = value;
  bs.setByteString(sizeof oneByte,&oneByte);
  reg.writeValue(bs);

  //Read-back for error checking
  UaByteString readBs(reg.readValue());

  readedValue=readBs.data()[0];

  if (readedValue != value){
	  LOG(Log::ERR) << nodeId<<  ":The readed value (" << (int)readedValue << ") don't correspond to the writed(" << (int)value << ")";
	  return OpcUa_Bad;
  }
  return OpcUa_Good;
}



