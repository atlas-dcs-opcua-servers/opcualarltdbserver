#include <iostream>
#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus setDelayAllFiber (string ltdbId, string scaId, int locx2Id ,OpcUa_Byte delay,
      OpcUa_Byte clkphase,UaClientSdk::UaSession* session);
std::string tostr (int x);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "Set LOCX2 delay")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("locx2Id", po::value<string>()->default_value("1"), "locx2 id")
    ("delay",po::value<string>()->default_value("9"),"delay value: 9 to 26")
    ("clkphase",po::value<string>()->default_value("1"),"clkphase value")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
  return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    LOG(Log::INF) << logID <<"---------------------------";
    LOG(Log::INF) << logID <<" LOCX2 set delay";
    LOG(Log::INF) << logID <<"---------------------------";
  
    LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>() << endl;
    OpcUa_Byte delay = (OpcUa_Byte)strtol(vm["delay"].as<string>().c_str(), NULL, 16);
    OpcUa_Byte clkphase = (OpcUa_Byte)strtol(vm["clkphase"].as<string>().c_str(), NULL, 16);
    int locx2Id = stoi(vm["locx2Id"].as<string>().c_str());
    LOG(Log::INF) << logID <<"Locx2Id :  "<< locx2Id << ", delay : " << tostr(delay) << ", clkphase : " << tostr(clkphase) << endl;

    s = setDelayAllFiber(vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),locx2Id,delay,clkphase,session);
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

    delete session;

    return 0;
}

  UaStatus setDelayAllFiber (string ltdbId, string scaId, int locx2Id, OpcUa_Byte delay,
             OpcUa_Byte clkphase,UaClientSdk::UaSession* session)
  {
    OpcUa_Byte offset = 0;
    OpcUa_Byte adcType = 0;
    OpcUa_Byte busNumber;
    OpcUa_Byte baseAddress;
    OpcUa_Byte readingMask0a=192;
    UaByteString bs;
    OpcUa_Byte oneByte;
    OpcUa_Byte delayA =delay;
    OpcUa_Byte delayB = delay+offset;
    OpcUa_Byte delayC =delay;
    OpcUa_Byte delayD = delay+offset;

    try{
      //Set base address
      if(locx2Id==1 || locx2Id==3){
        baseAddress=0x4;
      }
      else if(locx2Id==2 || locx2Id==4){
        baseAddress=0x8;
      }
      //set bus number
      if(locx2Id ==1 || locx2Id==2){
        busNumber=5;
      }
      else if(locx2Id==3 || locx2Id == 4){
        busNumber=7;
      }

      string baseNodeId="ltdb_"+ltdbId+".scaFelix"+scaId+".BusI2C_" + tostr(busNumber);
      string nodeId = baseNodeId+".register0" + tostr(baseAddress);

      UaoClientForOpcUaSca::I2cSlave register1(session,UaNodeId(nodeId.c_str(),2));

      nodeId =baseNodeId+".register0" + tostr(baseAddress+1);
      UaoClientForOpcUaSca::I2cSlave register2(session,UaNodeId(nodeId.c_str(),2));

      nodeId =baseNodeId+".register0" + tostr(baseAddress+2);
      UaoClientForOpcUaSca::I2cSlave register3(session,UaNodeId(nodeId.c_str(),2));

      nodeId =baseNodeId+".register0" + tostr(baseAddress+3);
      UaoClientForOpcUaSca::I2cSlave register4(session,UaNodeId(nodeId.c_str(),2));

      //Register 08
      oneByte=0x14;
      bs.setByteString(sizeof oneByte,&oneByte);
      register1.writeValue(bs);

      //Register 09
      oneByte=(((delayA % 16) << 4) + ((clkphase & 3 )<<2) + (adcType & 3));
      bs.setByteString(sizeof oneByte,&oneByte);
      register2.writeValue(bs);

      //Register 0a
      oneByte=(delayB << 1) + (delayA >> 4) + ((delayC % 4 ) << 6);
      bs.setByteString(sizeof oneByte,&oneByte);
      register3.writeValue(bs);

      //Register 0b
      oneByte = (delayD << 3) + ( delayC >> 2 ) ;
      bs.setByteString(sizeof oneByte,&oneByte);
      register4.writeValue(bs);
      //LOG(Log::INF) << logID <<"Set delay done" << endl;

    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
      LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
      return e.statusCode();
    }
    catch(const std::exception& e){
      LOG(Log::ERR) << logID <<"Caught:"<<e.what();
      return OpcUa_Bad;
    }
    return OpcUa_Good;
  }

std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}
