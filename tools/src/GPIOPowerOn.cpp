#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <LogIt.h>                  //Log librairy
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus GpioPower (UaClientSdk::UaSession* session, string ltdbId, string scaFelix, OpcUa_Boolean value);
std::string tostr (int x);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "GPIO power On/Off")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("powerOn", po::value<string>()->default_value("true"), "true for powerOn, false for powerOff ")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }

    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }

    LOG(Log::INF) << logID <<"--------------------";
    LOG(Log::INF) << logID <<" GPIO  Power On/OFF ";
    LOG(Log::INF) << logID <<"--------------------";
    LOG(Log::INF) <<logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();
    bool pv = (vm["powerOn"].as<string>()=="true");
      
    s = GpioPower(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), pv);
    if (s.isGood()){
        if (pv) LOG(Log::INF) << " GPIO  Power is now ON" << endl;
        else LOG(Log::INF) << " GPIO  Power is now OFF" << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : GPIO Power was not changed";
    delete session;

    return 0;
}



UaStatus GpioPower (UaClientSdk::UaSession* session, string ltdbId, string scaFelix, OpcUa_Boolean value)
{
  //OpcUa_Boolean valueInv=!value;
  UaoClientForOpcUaSca::DigitalIO *Adcl;
  std::string baseNodeId="ltdb_"+ltdbId+".scaFelix"+scaFelix;
  try{
    std::string nodeId;
    for (int i=1;i<=5;i++){
        nodeId=baseNodeId + ".gpio.Ctrl"+tostr(i);
        Adcl= new UaoClientForOpcUaSca::DigitalIO (session,UaNodeId(nodeId.c_str(),2));
        Adcl->writeValue(value);
        if (value)  LOG(Log::INF) << logID <<"The GPIO : " << nodeId << " is On"<<endl;
        else LOG(Log::INF) << logID <<"The GPIO : " << nodeId << " is Off"<<endl;

        delete Adcl;
    }
  }
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  }
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
return OpcUa_Good;
}


std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}
