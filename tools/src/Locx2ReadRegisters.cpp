#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>
#include <string>

namespace po = boost::program_options;

UaStatus readLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);
std::string tostr (int x);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "LOCX2 registers status")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")

    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
  return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    
    int  delayA, delayB, delayC, delayD;
    int low_pass_filter,frecuency,band_selection,charge_pump; 
    int read1, read2,read3,read4;

    LOG(Log::INF) << logID <<"-------------------------";
    LOG(Log::INF) << logID <<" LOCX2s Registers status ";
    LOG(Log::INF) << logID <<"-------------------------";
    LOG(Log::INF) <<logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();
    LOG(Log::INF) << logID <<"-------------------------";
    s = readLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());

    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";
   
    delete session;

    return 0;
}

UaStatus readLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
  OpcUa_Byte baseAddress[2] = {0x04,0x08};
  int bus_number[2] = {5,7};
  int locx2_number=1;
  std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
  try{
    for (int j=0; j<2; j++){
      for (int b=0; b<2; b++){
        LOG(Log::INF) << logID <<"LOCX2_" << locx2_number;
        int  delayA, delayB, delayC, delayD;
        int low_pass_filter,frecuency,band_selection,charge_pump;
        int  readV[4];
        locx2_number ++;

        for (int reg=0; reg<4; reg++){
        {
          std::string nodeId =baseNodeId+".BusI2C_" + tostr(bus_number[j]) +".register0" + tostr(reg+baseAddress[b]);
          UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
          UaByteString read_val(registerId.readValue());
          readV[reg] = (int) read_val.data()[0];
          if(reg==0){
            //Low pass filter 3rd selection (R3)
            low_pass_filter=((readV[reg] & 192)>>6);
            LOG(Log::INF) << logID << "Low pass filter 3rd selection (R3) => (0x"<< std::hex << low_pass_filter << ")";
            //Frequency band selection.
            frecuency=((readV[reg] & 48)>>4);
            LOG(Log::INF) << logID << "Frequency band selection           => (0x"<< std::hex << frecuency << ")";
            //Band_selection
            band_selection=((readV[reg] & 12)>>2);
            LOG(Log::INF) << logID << "Low pass filter bandwidth selection=> (0x"<< std::hex << band_selection << ")";
            //Charge pump
            charge_pump=((readV[reg] & 3));
            LOG(Log::INF) << logID << "Charge pump current (μA)           => (0x"<< std::hex << charge_pump << ")";  
          }   
        } 
        //Reading delays
        delayA=(readV[1] >>4) + ((readV[2] & 1)<<4);
        LOG(Log::INF) << logID << "delayA                             => ("<< delayA   << ")";
        delayB=(readV[2] & 62)>>1;
        LOG(Log::INF) << logID << "delayB                             => ("<< delayB   << ")";
        delayC=((readV[2] & 192)>>6) + ((readV[3] & 7)<<2);
        LOG(Log::INF) << logID << "delayC                             => ("<< delayC   << ")";
        delayD=(readV[3] & 248)>>3;
        LOG(Log::INF) << logID << "delayD                             => ("<< delayD   << ")";
        }
      }
    }
  }
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  } 
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  return OpcUa_Good;    
}

std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}

