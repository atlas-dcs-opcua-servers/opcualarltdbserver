#include <iostream>
#include <colorLogs.h>
#include <sstream>

std::string COLORLOG::color(std::string input,const COLORLOG::Color colcode ,bool bold ){
  std::stringstream str;
  if (!bold){
  str << "\033[0;" << colcode << "m" << input << "\033[0m";
  }
  else{
  str << "\033[1;" << colcode << "m" << input << "\033[0m";
  }
  return str.str();


}


