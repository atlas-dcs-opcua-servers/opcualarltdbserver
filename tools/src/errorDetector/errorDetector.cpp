#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <SCA.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>
#include <errorDetector.h>
#include <colorLogs.h>

namespace po = boost::program_options;

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "Ltdb Error detector")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);


    std:string report="\n------\nReport\n------\n";
    std::string baseNodeId="ltdb_"+vm["ltdbId"].as<string>();
    
    Ltdb reportStruct;
    
    
    
    
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    
    //reportStruct.print();
    //---------------------------------------------------------------------------------
    // MTX #4 Issue
    // --------------------------------------------------------------------------------
    std::cout << "--------------------------------" << std::endl;
    std::cout << "Ltdb " << baseNodeId << std::endl;
    std::cout << "--------------------------------" << std::endl;
    
    OpcUa_Byte oneByte= 0x4;
    UaByteString bs;
    bs.setByteString(sizeof oneByte,&oneByte);
    oneByte= 0x19;
    UaByteString bs2;
    bs2.setByteString(sizeof oneByte,&oneByte);
    int failure,tries;
    bool success;

      //report += "MTx #4 Issue:\n";
      for ( int scaId =1 ; scaId <= 5 ; scaId ++){
        tries=0;
        failure=0;
        std::string scaNodeId = baseNodeId + ".scaFelix"+std::to_string(scaId);
        UaoClientForOpcUaSca::SCA SCA (session,UaNodeId (scaNodeId.c_str(),2));

        reportStruct.sca[scaId-1].id=(int)SCA.readId();
        //Register 1
        std::string nodeId =scaNodeId+".BusI2C_6.register08"; 
        UaoClientForOpcUaSca::I2cSlave reg(session,UaNodeId(nodeId.c_str(),2));
        do{
          try {
            tries++;
            reg.writeValue(bs);
          }catch(const std::exception& e){
            failure++;
          }
        }while( tries < 100 );
        
        reportStruct.sca[scaId-1].mtx[4-1].errorRate = failure;
        if (failure > 10){
          reportStruct.sca[scaId-1].mtx[4-1].errorI2C = true;
        }
        else{
          reportStruct.sca[scaId-1].mtx[4-1].errorI2C = false;

        }
          
      }
    
    //------------------------------------------------------------
    //Bit stuck 
    //------------------------------------------------------------
    /*std::string details;
    for ( int scaId =1 ; scaId <= 5 ; scaId ++){
        std::string scaNodeId = baseNodeId + ".scaFelix"+std::to_string(scaId);
        for (int mtxId =1 ; mtxId<=3 ; mtxId++){
           for ( int regId = 1 ; regId <= 4 ; regId ++) {
              std::string regNodeId = scaNodeId + mtxNodeId(mtxId,regId);
              UaoClientForOpcUaSca::I2cSlave reg(session,UaNodeId(regNodeId.c_str(),2));
              details=I2C_Bit_Stuck_8bits(reg);
              report += "Sca " + std::to_string(scaId) + " MTx " + std::to_string (mtxId) +
                    " Register " + std::to_string (regId) + ":" + details  + "\n";
           }
        }
    }

    LOG(Log::INF)<< report;
*/
    reportStruct.print();
    return 0;
}

std::string mtxNodeId(int mtxId, int reg){
   int busNumber, baseAddress;
   if(mtxId==1 || mtxId == 3){
      baseAddress=0x4;
   }
   else if(mtxId==2 || mtxId == 4){
      baseAddress=0x8;
   }
   if(mtxId==1 || mtxId == 2){
      busNumber=4;
   }
   else if(mtxId==3 || mtxId == 4){
      busNumber=6;
   }

   return ".BusI2C_" + tostr(busNumber) +".register0" + tostr(reg-1+baseAddress);


}
void I2C_Bit_Stuck_8bits(UaoClientForOpcUaSca::I2cSlave reg, int &report){
    /*report=0;
    UaByteString bs;
    OpcUa_Byte nullB=0;
    UaByteString nullBs;
    nullBs.setByteString(sizeof nullB,&nullB);
    for (int bitNb = 0 ; bitNb <=7 ; bitNb ++){
      OpcUa_Byte mask = 1 << bitNb;
      bs.setByteString(sizeof mask,&mask);

      //Writing 1 in the bit
      reg.writeValue(bs);
      UaByteString readOne(reg.readValue());
      if ( readOne.length() > 0 ) {
              readedValue = readBs.data()[0];
       }


    }

    OpcUa_Byte bNull=0x0;
    OpcUa_Byte bOne=255;
    UaByteString bsNull;
    bsNull.setByteString(sizeof bNull,&bNull);
    UaByteString bsOne;
    bsOne.setByteString(sizeof bOne, &bOne);
    
    //UaByteString readBs;
    OpcUa_Byte readedValue;
  
    std::stringstream stream;



      
      reg.writeValue(bsNull);
      //Reading
      UaByteString readBs(reg.readValue());
      if ( readBs.length() > 0 ) {
        readedValue = readBs.data()[0];
      }
      else {LOG(Log::INF) << "Read error";}
      
      if (readedValue != bNull){
         stream << "Bit stuck : readed " << std::hex << (int) readedValue + " Wanted " + (int) bNull ;
         return stream.str() ;
      }
      
      reg.writeValue(bsOne);
      UaByteString readBs2(reg.readValue());
      if ( readBs2.length() > 0 ) {
        readedValue=readBs2.data()[0];
      }
      if (readedValue != bOne){
         stream << "Bit stuck : readed " << std::hex << (int) readedValue + " Wanted " + (int) bOne ;
         return stream.str() ;
      }
       stream << " OK " ;
         return stream.str() ;
*/
}

void Ltdb::print(){
  for (int scaId = 1 ; scaId <= 5 ; scaId ++){
  
    std::cout << "SCA : " << std::to_string(scaId) << "(" << sca[scaId-1].id <<")" << std::endl;
    std::cout << "  MTx #4 err rate: " << sca[scaId-1].mtx[4-1].errorRate <<std::endl;
    if ( sca[scaId-1].mtx[4-1].errorI2C == true){
      std::cout << COLORLOG::color("  MTX #4 I2C Error",COLORLOG::RED,false) <<std::endl;
    }
    else{
      std::cout << COLORLOG::color("  MTX #4 I2C OK",COLORLOG::GREEN,false) <<std::endl;
    }
  }
}



std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}
