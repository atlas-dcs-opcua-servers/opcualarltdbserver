#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus resetMtrx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "MTRX Reset")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ;



    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    logID="Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_" + vm["scaId"].as<string>();
    s = resetMtrx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

    delete session;

    return 0;
}


UaStatus resetMtrx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
  LOG(Log::INF)<<"-----------------------";
  LOG(Log::INF)<<" MTRX Reset";
  LOG(Log::INF)<<"-----------------------";
  
  try{
    OpcUa_Boolean vtrue=1;
    OpcUa_Boolean vfalse=0;
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
    LOG(Log::INF)<<" Reset : ltdb_"+ltdb_id+".scaFelix"+sca_id + " ...";
    std::string nodeId=baseNodeId + ".gpio.MTRX-RST";
    UaoClientForOpcUaSca::DigitalIO mtrxio(session,UaNodeId(nodeId.c_str(),2));
    mtrxio.writeValue(vtrue);
    mtrxio.writeValue(vfalse);
    LOG(Log::INF)<<" Reset done.";

  }  
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  return OpcUa_Good;    
}
