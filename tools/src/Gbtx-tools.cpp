//PING
#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>
#include <boost/program_options.hpp>




//PING
namespace po = boost::program_options;
UaStatus resetGbtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);
UaStatus gbtxWrite(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, int reg, int value);
UaStatus gbtxRead(UaClientSdk::UaSession* session ,std::string ltdb_id , std::string sca_id, int reg,OpcUa_Byte &value);
UaStatus readClkdes (std::string ltdb_id , std::string sca_id,OpcUa_Byte numLocx2, OpcUa_UInt16 & value,UaClientSdk::UaSession* session);

//RESET
namespace po = boost::program_options;
UaStatus resetGbtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);


//MAIN
int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "Tools box for Gbtx")
        ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
        ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
        ("scaId", po::value<string>()->default_value("1"), "sca id")
        ("PING", "Ping GBTX 1 to 4")
        ("RESET", "Reset GBTX 1 to 4")
        ("reset", po::value<bool>()->default_value(false), "Is gbt resetted before reading")
        ("WRITE","Write in any register")
        ("numReg", po::value<int>()->default_value(1), "number of register")
        ("regValue", po::value<int>()->default_value(1), "value to put in the register")
        ;

    //COMMON
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
    }

    if (vm.count("WRITE")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        std::cout << "Connecting" << std::endl;
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
              
        int value[435];
        OpcUa_Byte tempValue;
        s=gbtxWrite(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), vm["numReg"].as<int>(), vm["regValue"].as<int>());
        std::cout << "succesfull write" << std::endl;
        for (int i =1 ; i <= 435; i++){
            s = gbtxRead(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), i ,tempValue);
            value[i-1]=tempValue;
        }
        if (s.isGood()){
            LOG(Log::INF) << " The GBTX read was successfully done" << endl;
        }
        else{
            LOG(Log::ERR)<<" ERROR : The GBTX reading was not successfully done";
        }
    }


    //PING
    if (vm.count("PING")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        std::cout << "Connecting" << std::endl;
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        if(vm["reset"].as<bool>()){
            LOG(Log::INF) << logID <<"-------------------------";
            LOG(Log::INF) << logID <<"  GBTX Reset";
            LOG(Log::INF) << logID <<"-------------------------";
            for (int gbtx =1 ; gbtx <= 5 ; gbtx++){
                s = resetGbtx(session, vm["ltdbId"].as<string>(), "1");
                if (s.isGood()){
                    LOG(Log::INF) << " The GBTX reset was successfully done" << endl;
                }
                else{
                    LOG(Log::ERR)<<" ERROR : The GBTX reset was not successfully done";
                }
            }
        }
        int value[435];
        OpcUa_Byte tempValue;
        for (int i =1 ; i <= 435; i++){
            s = gbtxRead(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), i ,tempValue);
            value[i-1]=tempValue;
        }
        if (s.isGood()){
            LOG(Log::INF) << " The GBTX read was successfully done" << endl;
        }
        else{
            LOG(Log::ERR)<<" ERROR : The GBTX writing was not successfully done";
        }
        std::cout << "------------------ Result ---------------" <<std::endl;

        for (int i=1 ;i<5; i++){
            OpcUa_UInt16 value;
            readClkdes (vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),i, value,session) ;
            std::cout << "Locx " << i << " delay: " << (int) value << std::endl;

        }
        for (int i =0 ; i < 435; i++){
            if(i%10==0) std::cout << std::endl;
            std::cout << std::hex << value[i] <<std::dec << " ";
        }
        std::cout << std::endl;




        delete session;

        return 0;
    }

    //RESET
    if (vm.count("RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) << logID <<"  GBTX Reset";
        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) << logID <<"Ltdb_"<< vm["ltdbId"].as<string>() << " : Sca_ "<< vm["scaId"].as<string>();
        s = resetGbtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());

        if (s.isGood()){
            LOG(Log::INF) << " The GBTX reset was successfully done" << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : The GBTX reset was not successfully done";
        delete session;

        return 0;
    }
}



//PING
UaStatus gbtxRead(UaClientSdk::UaSession* session ,std::string ltdb_id , std::string sca_id, int reg,OpcUa_Byte &value){
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix1";
    std::string readCtrlNodeId=baseNodeId + ".GBTxbus.GBTx"+sca_id;
    std::string read=baseNodeId + ".GBTxbus.GBTx"+sca_id;
    UaByteString *readBs;
    value=0;
    UaByteString bs;
    OpcUa_Byte tOneByte[2];
    tOneByte[0]= ( reg & 0xFF);
    tOneByte[1]= ( reg >> 8 );
    //tOneByte[1]= ( reg & 0xFF);

    try{
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(readCtrlNodeId.c_str(),2));
        bs.setByteString(sizeof tOneByte,tOneByte);
        registerId.writeSlave(bs);
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    }
    //std::cout << "Gbtx read" << std::endl;
    try{
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(read.c_str(),2));
        readBs=new UaByteString(registerId.readValue());
        if (readBs->length()==0){
            LOG(Log::ERR)<<"Error reading the gbtx.register";
            delete readBs;
            return OpcUa_Bad;
        }
        for (int i=0;i< readBs->length();i++){
            value = value << 8;
            value+=(int) readBs->data()[i];
        }
        //std::cout << "Reading Done" << std::endl;
        std::cout<<"Register"<< reg <<": Readed value "<< (int)value <<std::endl;
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Reading Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    return OpcUa_Good;
}
UaStatus readClkdes (std::string ltdb_id , std::string sca_id,OpcUa_Byte numLocx2, OpcUa_UInt16 & value,UaClientSdk::UaSession* session){
    OpcUa_UInt16 coarseBitsMask=0x1f0;
    OpcUa_UInt16 fineBitsMask=0xf;
    OpcUa_Byte readingMaskCoarse=0x1f;
    OpcUa_Byte readingMaskFine1=0x0f;
    OpcUa_Byte readingMaskFine2=0xf0;
    OpcUa_Byte oneByte,readedValue;
    UaByteString bs;
    UaByteString *readBs;
    try{
        switch(numLocx2){
            case 1:
                gbtxRead(session,ltdb_id,sca_id,8,readedValue);
                value=((readedValue & readingMaskCoarse) << 4);
                gbtxRead(session,ltdb_id,sca_id,4,readedValue);
                value+=(readedValue & readingMaskFine1);
                break;

            case 2:
                gbtxRead(session,ltdb_id,sca_id,10,readedValue);
                value=((readedValue & readingMaskCoarse) << 4);
                gbtxRead(session,ltdb_id,sca_id,5,readedValue);
                value+=(readedValue & readingMaskFine1);


                break;
            case 3:
                gbtxRead(session,ltdb_id,sca_id,13,readedValue);
                value=((readedValue & readingMaskCoarse) << 4);
                gbtxRead(session,ltdb_id,sca_id,6,readedValue);
                value+=((readedValue & readingMaskFine2)>>4);

                break;
            case 4:
                gbtxRead(session,ltdb_id,sca_id,15,readedValue);
                value=((readedValue & readingMaskCoarse) << 4);
                gbtxRead(session,ltdb_id,sca_id,7,readedValue);
                value+=((readedValue & readingMaskFine2)>>4);
                break;
            default:

                break;
        }

        return OpcUa_Good;

    }
    catch(const std::exception& e){
        LOG(Log::ERR)<<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
}



UaStatus gbtxWrite(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, int reg, int value){
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix1";
	std::string readCtrlNodeId=baseNodeId + ".GBTxbus.GBTx"+sca_id;
	std::string read=baseNodeId + ".GBTxbus.GBTx"+sca_id;

    UaByteString bs;
    OpcUa_Byte tOneByte[3];
    tOneByte[2]= value;
    tOneByte[0]= ( reg & 0xFF);
    tOneByte[1]= ( reg >> 8 );

    std::cout << "Gbtx write" << std::endl;
    try{
            UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(readCtrlNodeId.c_str(),2));
            bs.setByteString(sizeof tOneByte,tOneByte);
            registerId.writeSlave(bs);
        }
        catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    return OpcUa_Good;
}




/*UaStatus gbtxWrite(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,int reg,int &value){
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix1";
    std::string readCtrlNodeId=baseNodeId + ".GBTxbus.GBTx"+sca_id; 
    std::string read=baseNodeId + ".GBTxbus.GBTx"+sca_id; 
    UaByteString *readBs;
    value=0;

    UaByteString bs;
    OpcUa_Byte tOneByte[2];
    tOneByte[1]= ( reg >> 8 );
    tOneByte[0]= ( reg & 0xFF);
    std::cout << "Gbtx reading of reg"<< reg << "  Adress:" <<std::hex << (int) tOneByte[0] << (int) tOneByte[1]<< std::dec << std::endl;
    try{
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(readCtrlNodeId.c_str(),2));
        bs.setByteString(sizeof tOneByte,tOneByte);
        registerId.writeSlave(bs);
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    }
    //std::cout << "Gbtx read" << std::endl;
    try{
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(read.c_str(),2));
        readBs=new UaByteString(registerId.readValue());
        if (readBs->length()==0){
            LOG(Log::ERR)<<"Error reading the gbtx.register";
            delete readBs;
            return OpcUa_Bad;
        }                                                                  
        for (int i=0;i< readBs->length();i++){
            value = value <<8;
            value+=(int) readBs->data()[i];
        }
        //std::cout << "Reading Done" << std::endl;
        std::cout<<"Register"<< reg <<": Readed value "<<std::hex<< value<<std::dec <<std::endl;
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Reading Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    return OpcUa_Good;
}*/

//RESET
UaStatus resetGbtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
    try{

        OpcUa_Boolean vfalse=0;
        OpcUa_Boolean vtrue=1;
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.GBTxRST";
        UaoClientForOpcUaSca::DigitalIO gbtx_r(session,UaNodeId(nodeId.c_str(),2));
        gbtx_r.writeValue(vtrue);
        gbtx_r.writeValue(vfalse);

        LOG(Log::INF) << logID <<"Reset done.";
    }  
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }

    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}
