#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <LogIt.h>                  //Log librairy
#include <uaplatformlayer.h>
#include <DigitalIO.h>
#include <fstream>
#include <cmath>

///DECLARATIONS
//READ
namespace po = boost::program_options;
UaStatus readADC_r(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,string locx2Id, string adcId);
std::string tostr (int x);

//RESET
namespace po = boost::program_options;

UaStatus resetADC(UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id);
UaStatus fakeRead (UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id);
UaStatus switchTo10Bits (UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id);
UaStatus readADC_r(OpcUa_UInt64 & value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id);
UaStatus writeADC_r (OpcUa_UInt64 value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id);

//SELECT MODE
static const OpcUa_Byte RAW_MODE     = 2 ;
static const OpcUa_Byte PATTERN_MODE = 1;
static const OpcUa_Byte DATA_MODE    = 0;

namespace po = boost::program_options;
UaStatus selectAdcModeForAdc (int locx2Id, string nodeId_read, string nodeId_write, int numChannel,OpcUa_UInt64 mode,UaClientSdk::UaSession* session);
UaStatus selectAdcMode (string nodeId_read, string nodeId_write,int  numChannel, OpcUa_Byte numMode,UaClientSdk::UaSession* session);
UaStatus readADC_sm (string nodeId_read, OpcUa_UInt64 &value, UaClientSdk::UaSession* session);
UaStatus writeADC_sm (string nodeId_write, OpcUa_UInt64 value,UaClientSdk::UaSession* session);
UaStatus writeAndCheckADCWithoutInterLeave_sm (string nodeId_read, string nodeId_write, OpcUa_UInt64 value,UaClientSdk::UaSession* session);
std::string tostr (int x);

//WRITE AND CHECK
namespace po = boost::program_options;
UaStatus valueFromCalibFiles(string filename,string nodeId_read, string nodeId_write,UaClientSdk::UaSession* session);
UaStatus writeAndCheckADC (string nodeId_read, string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session);
UaStatus readADC(string nodeId_read,OpcUa_UInt64 & value,UaClientSdk::UaSession* session);
UaStatus writeADC (string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session);
UaStatus writeValueAdc (string nodeId_read, 
        string nodeId_write,
        OpcUa_Byte numChannel,
        OpcUa_UInt16 MDAC1_C0,
        OpcUa_UInt16 MDAC2_C0,
        OpcUa_UInt16 MDAC3_C0,
        OpcUa_UInt16 MDAC4_C0,
        OpcUa_UInt16 MDAC1_C1,
        OpcUa_UInt16 MDAC2_C1,
        OpcUa_UInt16 MDAC3_C1,
        OpcUa_UInt16 MDAC4_C1,
        UaClientSdk::UaSession* session);
UaStatus writeAndCheckADCWithoutInterLeave (string nodeId_read,string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session);
string formatNcaracters(string num, int size_format);
std::string tostr (int x);
std::vector<std::string> tokenize(std::string value,  std::string separator);
UaStatus secureSTF (std::string svalue1, float &output);


///MAIN
int main(int argc, const char *argv[])
{
    //COMMON
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "Tool boxe for ADC registers")
        ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
        ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
        ("scaId", po::value<string>()->default_value("1"), "sca id")
        ("locx2Id", po::value<string>()->default_value("1"), "locx2 id")
        ("adcId", po::value<string>()->default_value("1"), "adc id from locx2 (from 1 to 4)")
        ("READ", "ADC status of registers")
        ("RESET", "ADC reset")
        ("SELECT_MODE", "Select Adc mode")
        ("numChannel", po::value<string>()->default_value("1"), "channel number, 1 to 4")
        ("numMode", po::value<string>()->default_value("0"), "Modes : data =>0, pattern =>1 or raw =>2")
        ("WRITEandCHECK", "Set values in files found in configFilePath to all ADCs")
        ("configFilePath", po::value<string>()->default_value("/localdisk/OPC-PROJECTS/OpcUaLarLtdbServer/ltdb_calibration_files"),"File containing the values to set all ADCs in lardcs@pcatllarltdb")
        ;


    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }

    if ( (vm["adcId"].as<string>() != "1") || (vm["adcId"].as<string>() != "2") ||(vm["adcId"].as<string>() != "3") ||(vm["adcId"].as<string>() != "4"))
    {
      cout << "Be carefull, the adc Id must be between 1 and 4" << endl;
      return 1;
    }
    //READ
    if (vm.count("READ")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }

        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) << logID <<" ADC  Status Registers";
        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) <<logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();

        s = readADC_r(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), vm["locx2Id"].as<string>(), vm["adcId"].as<string>());
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }


    //RESET
    if (vm.count("RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        logID="Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>() + ": Locx2_ " + vm["locx2Id"].as<string>() + ": ADC_ " + vm["adcId"].as<string>();
        std::string baseNodeId="ltdb_"+vm["ltdbId"].as<string>()+".scaFelix"+vm["scaId"].as<string>();

        //std::string nodeId=vm["locx2Id"].as<string>()+"A"+vm["adcId"].as<string>();

        s = resetADC(session,baseNodeId,vm["locx2Id"].as<string>(),vm["adcId"].as<string>());
        if (s.isGood()){
            LOG(Log::INF) << "Reset successfully done" << endl;              
        }


        delete session;

        return 0;
    }


    //SELECT MODE
    if (vm.count("SELECT_MODE")) {
		UaStatus s = OpcUa_Bad;
		Log::initializeLogging(Log::INF);
		UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
		if (!session){
			LOG(Log::ERR)<<"Bad communication Error";
			return 0;
		}

		LOG(Log::INF) << logID <<"--------------------";
		LOG(Log::INF) << logID <<" ADC Select mode    ";
		LOG(Log::INF) << logID <<"--------------------";

		int numChannel = stoi(vm["numChannel"].as<string>().c_str());
		OpcUa_Byte numMode = (OpcUa_Byte)strtol(vm["numMode"].as<string>().c_str(), NULL, 16);
		int locx2Id = stoi(vm["locx2Id"].as<string>().c_str());

		string baseNodeId="ltdb_"+ vm["ltdbId"].as<string>() +".scaFelix"+  vm["scaId"].as<string>();
		string nodeId_read =baseNodeId +".BusADC_LOCx2_" +  vm["locx2Id"].as<string>() +".read_adc"+ vm["adcId"].as<string>() ;
		string nodeId_write =baseNodeId +".BusADC_LOCx2_" +  vm["locx2Id"].as<string>() +".write_adc"+ vm["adcId"].as<string>() ;

		LOG(Log::INF) <<logID << "NodeId read : " <<nodeId_read;
		LOG(Log::INF) <<logID << "NodeId write : " <<nodeId_write;
		LOG(Log::INF) <<logID << "numChannel_" + vm["numChannel"].as<string>() + " : numMode_"+ vm["numMode"].as<string>();

		s = selectAdcMode(nodeId_read,nodeId_write,numChannel,numMode,session);
    
		//Luz
		//s = selectAdcModeForAdc (locx2Id,nodeId_read,nodeId_write,numChannel,numMode,session); 
		if (s.isGood()){
			LOG(Log::INF) << " Mode has been successfully updated" << endl;
		}
		else
			LOG(Log::ERR)<<" The ADC mode hasn't been updated ";
		delete session;

		return 0;
	}


    //WRITE AND CHECK
    if (vm.count("WRITEandCHECK")) {
        UaStatus s = OpcUa_Bad;
		Log::initializeLogging(Log::INF);
		//LOG(Log::INF) << logID <<fileName;
		UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
		if (!session){
		  LOG(Log::ERR)<<"Bad communication Error";
		  return 0;
		}
		LOG(Log::INF) << logID <<"-------------------";
		LOG(Log::INF) << logID <<" WRITE ADC  ";
		LOG(Log::INF) << logID <<"-------------------";

		string baseNodeId="ltdb_"+vm["ltdbId"].as<string>()+".scaFelix"+vm["scaId"].as<string>();
		string nodeId_read =baseNodeId +".BusADC_LOCx2_" + vm["locx2Id"].as<string>() +".read_adc" + vm["adcId"].as<string>();
		string nodeId_write =baseNodeId +".BusADC_LOCx2_" + vm["locx2Id"].as<string>() +".write_adc" + vm["adcId"].as<string>();
		//This path exists in lardcs@pcatllarltdb
		//std::string fileName= vm["configFilePath"].as<string>() + "/ltdb"+ formatNcaracters(vm["ltdbId"].as<string>(),3)+"/"+ "ADC_constants_" + vm["adcId"].as<string>() +".csv";
		std::string fileName= vm["configFilePath"].as<string>()+ "/ltdb"+ formatNcaracters(vm["ltdbId"].as<string>(),3)+ "/ADC_constants_" + vm["adcId"].as<string>() +".csv";
		LOG(Log::INF) << " Full path : " << fileName << endl;

		s = valueFromCalibFiles(fileName,nodeId_read,nodeId_write,session);
		if (s.isGood()){
			LOG(Log::INF) << " The ADC has been successfully updated" << endl;
		}
		else
			LOG(Log::ERR)<<" ERROR : The ADC hasn't been updated ";
		
		delete session;
		return 0;
    }
}


//READ
UaStatus readADC_r(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, string locx2Id, string adcId)
{ 
    UaByteString bs;
    OpcUa_UInt64 value=0;
    try{
        std::string baseNodeId="ltdb_"+ ltdb_id +".scaFelix"+ sca_id;
        std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2Id +".read_adc"+ adcId ;
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
        UaByteString readBs(registerId.readValue());
        LOG(Log::INF)<<"READ ADC"<< endl;
        if (readBs.length()!=8){ 
            LOG(Log::ERR) << logID <<"Error reading register"+ sca_id +".BusADC_LOCx2_" + locx2Id +".read_adc" + adcId;
            return OpcUa_Bad;
        }   
        for (int i=0;i<8;i++){
            LOG(Log::INF)<<"data n"<<i <<"="<<(int) readBs.data()[i];
            value = value <<8;
            value+=(int) readBs.data()[i];
            LOG(Log::INF)<<"value ="<<std::hex<< value;
        }
        // LOG(Log::ERR) << logID <<"Register "+sca_id +".BusADC_LOCx2_" + locx2Id +".read_adc" + adcId;
        // LOG(Log::ERR) << logID <<"Value" << "  => (0x"<< std::hex << (int)readBs.data()[0] << ")";
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }   
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}

std::string tostr (int x)
{
    std::stringstream str;
    str <<std::hex<< x;
    return str.str();
}



//RESET
UaStatus resetADC(UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id)
{
    try{

        OpcUa_Boolean true_value=1;
        OpcUa_Boolean false_value =0;
        UaStatus s = OpcUa_Bad;
        //std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.RSTB_ADCL"+locx2_id+"A"+adc_id;
        UaoClientForOpcUaSca::DigitalIO adc_d(session,UaNodeId(nodeId.c_str(),2));

        adc_d.writeValue(true_value);
        adc_d.writeValue(false_value);
        adc_d.writeValue(true_value);
        s = switchTo10Bits(session,baseNodeId,locx2_id,adc_id);
        if (!s.isGood()){return s;}
        s = fakeRead(session,baseNodeId,locx2_id,adc_id);
        if (!s.isGood()){return s;}

    }  
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        if (e.statusCode()==OpcUa_BadResourceUnavailable){
            LOG(Log::TRC) << logID <<"Caught(BadStatusCode):"<<e.what();
        }
        else{
            LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        }
        return e.statusCode();
    }
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}

UaStatus fakeRead (UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id){
    OpcUa_UInt64 writeValue =0xA024000000000000;
    OpcUa_UInt64 readValue;
    UaStatus s =writeADC_r(writeValue,session,baseNodeId,locx2_id,adc_id);
    if (!s.isGood()){
        return s;
    }
    s =readADC_r(readValue,session,baseNodeId,locx2_id,adc_id);
    if (!s.isGood()){
        return s;
    }
    LOG(Log::INF) << "fakeRead OK"<<endl;
    return OpcUa_Good;
}
UaStatus switchTo10Bits (UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id){
    UaByteString bs;
    OpcUa_Byte oneByte;
    oneByte=0x20;
    try{
        //std::string baseNodeId="ltdb_"+ltdb_id +".scaFelix"+ sca_id;
        std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2_id +".ctrl_adc" + adc_id;
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
        bs.setByteString(sizeof oneByte,&oneByte);
        registerId.writeValue(bs);
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    LOG(Log::INF) << "switchTo10Bits OK"<<endl;
    return OpcUa_Good;
}
UaStatus readADC_r(OpcUa_UInt64 & value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id){
    UaByteString bs;
    value=0;
    try{
        //std::string baseNodeId="ltdb_"+ltdb_id +".scaFelix"+ sca_id;
        std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2_id +".read_adc" + adc_id;
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
        UaByteString readBs(registerId.readValue());
        if (readBs.length()!=8){
            LOG(Log::ERR) << logID <<"Error reading register"+ nodeId;
            return OpcUa_Bad;
        }
        for (int i=0;i<8;i++){
            value = value <<8;
            //LOG(Log::INF)<<"Data : ("<< i << "=" << (int) readBs.data()[i] << ")" << endl;
            value+=(int) readBs.data()[i];
            //LOG(Log::INF)<<"Value : ("<<std::hex<< value<< ")" << endl;
        } 
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    LOG(Log::INF) << "ReadADC OK"<<endl;
    return OpcUa_Good;
}

UaStatus writeADC_r (OpcUa_UInt64 value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id){
    UaByteString bs;
    OpcUa_Byte tOneByte[8];
    try{
        for (int i=0;i<8;i++){
            tOneByte[7-i]=value & 255;
            value=value >> 8;
        }
        //std::string baseNodeId="ltdb_"+ltdb_id +".scaFelix"+ sca_id;
        std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2_id +".write_adc"+ adc_id;
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
        bs.setByteString(sizeof tOneByte,tOneByte);
        registerId.writeValue(bs);
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    LOG(Log::INF) << "writeADC OK"<<endl;
    return OpcUa_Good;
}



//SELECT MODE
UaStatus selectAdcModeForAdc (int locx2Id, string nodeId_read, string nodeId_write, int numChannel,OpcUa_UInt64 mode,UaClientSdk::UaSession* session){
      int triedtimes;
      OpcUa_UInt64 mask =1;
      //int i=(std::stoi(locx2Id)-1)*16;
      int i=(locx2Id-1)*16;
      UaStatus s = OpcUa_Bad;
      //for (DLtdbADCNevis* adc : ltdbadcneviss()){
        //for(int channel = 1 ; channel <= 4 ; channel ++){
          if ((mode & (mask << i)) != 0){
            //LOG(Log::INF) << logID << "selectAdcModeForAdc: channel: " << (64 * (std::stoi(getParent()->id()) - 1)) + i + 1 << ": PATTERN_MODE" ;
            triedtimes=0;
             do{
               triedtimes++;
               //s=selectAdcMode(nodeId_read,nodeId_write,channel,DLtdbADCNevis::PATTERN_MODE,session);
               s = selectAdcMode(nodeId_read,nodeId_write,numChannel,mode,session); 
             }while(!s.isGood() && triedtimes < 5);
              if (triedtimes>1){
                LOG(Log::WRN) << logID << "Number of try :"<< triedtimes;
              }
              if (!s.isGood()){return s;}

          }
          else {
            //LOG(Log::INF) << logID << "selectAdcModeForAdc: channel: " << (64 * (std::stoi(getParent()->id()) - 1)) + i + 1 << ": DATA_MODE" ;
            triedtimes=0;
             do{
               triedtimes++;
               s=selectAdcMode(nodeId_read,nodeId_write,numChannel,DATA_MODE,session);
             }while(!s.isGood() && triedtimes < 5);
              if (triedtimes>1){
                LOG(Log::WRN) << logID << "Number of try :"<< triedtimes;
              }
              if (!s.isGood()){return s;}
          }
          i++;

      //}
      return OpcUa_Good;
}

UaStatus selectAdcMode (string nodeId_read, string nodeId_write, int numChannel, OpcUa_Byte numMode,UaClientSdk::UaSession* session)
{
    UaStatus s = OpcUa_Bad;
    OpcUa_UInt64 writeRegisterControlMDAC;
    switch(numChannel){
      case 1:
      writeRegisterControlMDAC= 0xA036000000000000;
      break;
      case 2:
      writeRegisterControlMDAC= 0xA056000000000000;
      break;
      case 3:
      writeRegisterControlMDAC= 0x6036000000000000;
      break;
      case 4:
      writeRegisterControlMDAC= 0x6056000000000000;
      break;
      default:
      LOG(Log::ERR) << logID << "wrong channel number: "<<numChannel;
      return OpcUa_Bad;
    }
    switch(numMode){
      case RAW_MODE:
        LOG(Log::INF) << "Setting up RAW_MODE" << endl;
        // ajoute 0 au registre de controle donc on ne fait rien
      break;
      case PATTERN_MODE:
        LOG(Log::INF) << "Setting up PATTERN_MODE" << endl;
        writeRegisterControlMDAC +=0x20;
      break;
      case DATA_MODE:
        LOG(Log::INF) << "Setting up DATA_MODE" << endl;
        writeRegisterControlMDAC +=0x80;
      break;
      /*case 3:
 *           writeRegisterControlMDAC+=0x40;
 *                   break;*/
      default:
      LOG(Log::ERR) << logID <<"wrong Mode: "<<numMode;
      return OpcUa_Bad;
    }
    //s=writeAndCheckADCWithoutInterLeave(nodeId_read,nodeId_write,writeRegisterControlMDAC,session);
    s=writeADC_sm(nodeId_write,writeRegisterControlMDAC,session);
    if (!s.isGood()){return s;}
    return OpcUa_Good;
  }



UaStatus writeAndCheckADCWithoutInterLeave_sm (string nodeId_read, string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session){
      int readError=0, writeError=0;
      int readErrorMAX=5, writeErrorMAX=5;
      LOG(Log::INF)<<"Write and Check data :" <<std::hex<< value;
      OpcUa_UInt64 addressMask = 0xffff000000000000;
      OpcUa_UInt64 dataMask    = 0x0000ffffffffffff;

      OpcUa_UInt64 readValue;
      OpcUa_UInt64 readingReg;
      UaStatus s = OpcUa_Bad;

      readingReg = (value & addressMask) - 0x0010000000000000;
      while(writeError<writeErrorMAX){
        s =writeADC_sm(nodeId_write,value,session);
        if (!s.isGood()){return s;}
        readError=0;
        while(readError<readErrorMAX){
          //Reading reg 1
          s =writeADC_sm(nodeId_write,readingReg,session);
          if (!s.isGood()){return s;}
          s =readADC_sm(nodeId_read,readValue,session);
          LOG(Log::INF)<<"Data read:" <<std::hex<< readValue;
          if (!s.isGood()){return s;}

          if (readValue==(value & dataMask)){
            LOG(Log::WRN) << logID <<"Reading data corresponds to written value"<<endl;
            return OpcUa_Good;
          }
          readError++;
          LOG(Log::WRN) << logID <<"Read Loop "<< readError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
        }
        writeError++;
        LOG(Log::WRN) << logID <<"Write Loop "<< writeError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
      }
      LOG(Log::ERR) << logID <<"Data don't correspond to written value:" ;
      return OpcUa_Bad;
    }
 
UaStatus readADC_sm(string nodeId_read, OpcUa_UInt64 &value, UaClientSdk::UaSession* session)
  {
    UaByteString bs;
    value=0;
    try{
      UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_read.c_str(),2));
      UaByteString readBs(registerId.readValue());
      if (readBs.length()!=8){
        LOG(Log::ERR) << logID <<"Error reading register"+ nodeId_read;
        return OpcUa_Bad;
      }
      for (int i=0;i<8;i++){
        value = value <<8;
        value+=(int) readBs.data()[i];
      }
      LOG(Log::INF) << logID <<"Register "+ nodeId_read;
      //LOG(Log::INF) << logID <<"Value" << "  => (0x"<< std::hex << (int)readBs.data()[0] << ")";
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
      LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
      return e.statusCode();
    }
    catch(const std::exception& e){
      LOG(Log::ERR) << logID <<"Caught:"<<e.what();
      return OpcUa_Bad;
    }
    return OpcUa_Good;
  }

  UaStatus writeADC_sm (string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session)
  {   
    UaByteString bs;
    OpcUa_Byte tOneByte[8];
    try{
        for (int i=0;i<8;i++){
            tOneByte[7-i]=value & 255;
            LOG(Log::INF)<<"tOneByte[7-i] ="<<std::hex << (int)tOneByte[7-i];
            value=value >> 8;
        }
        LOG(Log::INF)<< "writing in node: "<< nodeId_write;
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_write.c_str(),2));
        bs.setByteString(sizeof tOneByte,tOneByte);
        //LOG(Log::INF) << logID <<"Value to write:" << "  => (0x"<< std::hex << (int)bs.data()[0] << ")";
        registerId.writeValue(bs);

    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }   
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    } 
      return OpcUa_Good;
  }
  


//WRITE AND CHECK
UaStatus valueFromCalibFiles(string fileName, string nodeId_read, string nodeId_write, UaClientSdk::UaSession* session){
    //std::string fileName=getParent()->getParent()->getParent()->calibFilesPath() + "ADC_constants_" + std::to_string(totalAdcNum) +".csv";
    //std::string fileName= fileName + "ltdb"+ formatNcaracters(parameters[0],3)+"/"+ "ADC_constants_" + parameters[3] +".csv";

    LOG(Log::INF) << logID << "  ValueFromCalibFiles file:" <<fileName;

    OpcUa_UInt16 MDAC1_C0;
    OpcUa_UInt16 MDAC2_C0;
    OpcUa_UInt16 MDAC3_C0;
    OpcUa_UInt16 MDAC4_C0;
    OpcUa_UInt16 MDAC1_C1;
    OpcUa_UInt16 MDAC2_C1;
    OpcUa_UInt16 MDAC3_C1;
    OpcUa_UInt16 MDAC4_C1;

    std::string line;

    float ReadMDAC1_C0;
    float ReadMDAC2_C0;
    float ReadMDAC3_C0;
    float ReadMDAC4_C0;
    float ReadMDAC1_C1;
    float ReadMDAC2_C1;
    float ReadMDAC3_C1;
    float ReadMDAC4_C1;
    UaStatus s = OpcUa_Bad;
    std::ifstream file(fileName.c_str(), std::ios::in);
    int channel=0;
    if (file){
     while (getline(file, line) ){

       std::vector <std::string> tokens = tokenize(line,",");
       if(!tokens.size()) continue;
       if(tokens.size()!=8){
         LOG(Log::ERR)<< logID << "Error:Wrong formatting of the file :" << fileName << ": It must be 8 values";
         return OpcUa_BadInternalError;
       }
       channel++;
       if (channel >4){
         LOG(Log::ERR)<< logID << "Error: more than 4 channel found in file"<< fileName;
         return OpcUa_BadInternalError;
       }
       LOG(Log::TRC) << logID << "Reading the MDAQ for channel:"<< channel;
       s=secureSTF(tokens[0],ReadMDAC1_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[2],ReadMDAC2_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[4],ReadMDAC3_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[6],ReadMDAC4_C0);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[1],ReadMDAC1_C1);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[3],ReadMDAC2_C1);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[5],ReadMDAC3_C1);
       if (!s.isGood()){return s;}
       s=secureSTF(tokens[7],ReadMDAC4_C1);
       if (!s.isGood()){return s;}

        LOG(Log::TRC)<< "ReadMDAC1_C0:"<<ReadMDAC1_C0;
        LOG(Log::TRC)<< "ReadMDAC2_C0:"<<ReadMDAC2_C0;
        LOG(Log::TRC)<< "ReadMDAC3_C0:"<<ReadMDAC3_C0;
        LOG(Log::TRC)<< "ReadMDAC4_C0:"<<ReadMDAC4_C0;
        LOG(Log::TRC)<< "ReadMDAC1_C1:"<<ReadMDAC1_C1;
        LOG(Log::TRC)<< "ReadMDAC2_C2:"<<ReadMDAC2_C1;
        LOG(Log::TRC)<< "ReadMDAC3_C3:"<<ReadMDAC3_C1;
        LOG(Log::TRC)<< "ReadMDAC4_C4:"<<ReadMDAC4_C1;

        ReadMDAC1_C0 = std::round(ReadMDAC1_C0);
        ReadMDAC2_C0 = std::round(ReadMDAC2_C0);
        ReadMDAC3_C0 = std::round(ReadMDAC3_C0);
        ReadMDAC4_C0 = std::round(ReadMDAC4_C0);
        ReadMDAC1_C1 = std::round(ReadMDAC1_C1);
        ReadMDAC2_C1 = std::round(ReadMDAC2_C1);
        ReadMDAC3_C1 = std::round(ReadMDAC3_C1);
        ReadMDAC4_C1 = std::round(ReadMDAC4_C1);

        LOG(Log::TRC)<< "ReadMDAC1_C0 rounded:"<<ReadMDAC1_C0;
        LOG(Log::TRC)<< "ReadMDAC2_C0 rounded:"<<ReadMDAC2_C0;
        LOG(Log::TRC)<< "ReadMDAC3_C0 rounded:"<<ReadMDAC3_C0;
        LOG(Log::TRC)<< "ReadMDAC4_C0 rounded:"<<ReadMDAC4_C0;
        LOG(Log::TRC)<< "ReadMDAC1_C1 rounded:"<<ReadMDAC1_C1;
        LOG(Log::TRC)<< "ReadMDAC2_C2 rounded:"<<ReadMDAC2_C1;
        LOG(Log::TRC)<< "ReadMDAC3_C3 rounded:"<<ReadMDAC3_C1;
        LOG(Log::TRC)<< "ReadMDAC4_C4 rounded:"<<ReadMDAC4_C1;

        MDAC1_C0 = (int) ReadMDAC1_C0;
        MDAC2_C0 = (int) ReadMDAC2_C0;
        MDAC3_C0 = (int) ReadMDAC3_C0;
        MDAC4_C0 = (int) ReadMDAC4_C0;
        MDAC1_C1 = (int) ReadMDAC1_C1;
        MDAC2_C1 = (int) ReadMDAC2_C1;
        MDAC3_C1 = (int) ReadMDAC3_C1;
        MDAC4_C1 = (int) ReadMDAC4_C1;

        LOG(Log::TRC)<< "MDAC1_C0:"<<MDAC1_C0 << "(0x" << std::hex << MDAC1_C0 << ")" ;
        LOG(Log::TRC)<< "MDAC2_C0:"<<MDAC2_C0<< "(0x" << std::hex << MDAC2_C0 << ")" ;;
        LOG(Log::TRC)<< "MDAC3_C0:"<<MDAC3_C0<< "(0x" << std::hex << MDAC3_C0 << ")" ;;
        LOG(Log::TRC)<< "MDAC4_C0:"<<MDAC4_C0<< "(0x" << std::hex << MDAC4_C0 << ")" ;;
        LOG(Log::TRC)<< "MDAC1_C1:"<<MDAC1_C1<< "(0x" << std::hex << MDAC1_C1 << ")" ;;
        LOG(Log::TRC)<< "MDAC2_C2:"<<MDAC2_C1<< "(0x" << std::hex << MDAC2_C1 << ")" ;;
        LOG(Log::TRC)<< "MDAC3_C3:"<<MDAC3_C1<< "(0x" << std::hex << MDAC3_C1 << ")" ;;
        LOG(Log::TRC)<< "MDAC4_C4:"<<MDAC4_C1<< "(0x" << std::hex << MDAC4_C1 << ")" ;;

        s=writeValueAdc(nodeId_read,nodeId_write,channel,MDAC1_C0,MDAC2_C0, MDAC3_C0, MDAC4_C0, MDAC1_C1,MDAC2_C1,MDAC3_C1, MDAC4_C1,session);
        if (!s.isGood()){return s;}
        LOG(Log::TRC)<< "Values read :" << MDAC1_C0<<"|"<<MDAC2_C0<<"|"<<MDAC3_C0<<"|"<<MDAC4_C0<<"|"<<MDAC1_C1<<"|" <<MDAC2_C1<<"|" << MDAC3_C1<<"|"<<MDAC4_C1;
      }
     if (channel !=4){
       LOG(Log::ERR)<< logID << "Error: less than 4 channel found in file" << fileName;
       return OpcUa_BadInternalError;
     }
      file.close();
      return OpcUa_Good;
    }
    else {
      LOG(Log::ERR) << logID  << " File not found:";
      return OpcUa_Bad;
    }
  }

  UaStatus writeValueAdc (string nodeId_read,
              string nodeId_write,
              OpcUa_Byte numChannel,
              OpcUa_UInt16 MDAC1_C0,
              OpcUa_UInt16 MDAC2_C0,
              OpcUa_UInt16 MDAC3_C0,
              OpcUa_UInt16 MDAC4_C0,
              OpcUa_UInt16 MDAC1_C1,
              OpcUa_UInt16 MDAC2_C1,
              OpcUa_UInt16 MDAC3_C1,
              OpcUa_UInt16 MDAC4_C1,
              UaClientSdk::UaSession* session)
  {
    UaStatus s = OpcUa_Bad;
    OpcUa_UInt16 mask30=0x000f;
    OpcUa_UInt16 mask70=0x00ff;
    OpcUa_UInt16 mask114=0x0ff0;
    OpcUa_UInt16 mask118=0x0f00;
    OpcUa_UInt64 writeRegister1;
    OpcUa_UInt64 writeRegister2;
    OpcUa_UInt64 writeRegisterConfig;
    OpcUa_UInt64 writeRegisterControlMDAC;
    switch(numChannel){
        case 1:
        writeRegister1     = 0xA034000000000000;
        writeRegister2     = 0xA035000000000000;
        writeRegisterControlMDAC= 0xA036000000000000;
        writeRegisterConfig = 0xA01A000000000000;
      break;
        case 2:
      writeRegister1     = 0xA054000000000000;
      writeRegister2     = 0xA055000000000000;
      writeRegisterControlMDAC= 0xA056000000000000;
      writeRegisterConfig = 0xA01A000000000000;
      break;
        case 3:
      writeRegister1     = 0x6034000000000000;
      writeRegister2     = 0x6035000000000000;
      writeRegisterControlMDAC= 0x6036000000000000;
      writeRegisterConfig = 0x601A000000000000;
      break;
        case 4:
      writeRegister1     = 0x6054000000000000;
      writeRegister2     = 0x6055000000000000;
      writeRegisterControlMDAC= 0x6056000000000000;
      writeRegisterConfig = 0x601A000000000000;
      break;
        default:
        LOG(Log::ERR) << logID <<"wrong channel number: "<<numChannel;
        return OpcUa_Bad;
    }
    writeRegisterControlMDAC+=0x80;
    writeRegister1+=((OpcUa_UInt64)MDAC1_C0)<<36;
    writeRegister1+=((OpcUa_UInt64)MDAC2_C0)<<24;
    writeRegister1+=((OpcUa_UInt64)MDAC3_C0)<<12;
    writeRegister1+=((OpcUa_UInt64)MDAC4_C0);
    writeRegister2+=((OpcUa_UInt64)MDAC1_C1)<<36;
    writeRegister2+=((OpcUa_UInt64)MDAC2_C1)<<24;
    writeRegister2+=((OpcUa_UInt64)MDAC3_C1)<<12;
    writeRegister2+=((OpcUa_UInt64)MDAC4_C1);

    s=writeAndCheckADC(nodeId_read,nodeId_write,writeRegister1,session);
    s=writeAndCheckADC(nodeId_read,nodeId_write,writeRegister2,session);
    s=writeAndCheckADCWithoutInterLeave(nodeId_read,nodeId_write,writeRegisterControlMDAC,session);
    if (!s.isGood()){return s;}
    s=writeAndCheckADCWithoutInterLeave(nodeId_read,nodeId_write,writeRegisterConfig,session);
    if (!s.isGood()){return s;}
    return OpcUa_Good;
  }

UaStatus writeAndCheckADC (string nodeId_read,string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session)
{
  int readError=0, writeError=0;
  int readErrorMAX=5, writeErrorMAX=5;
  LOG(Log::INF)<<"Write and Check data :" <<std::hex<< value;
  OpcUa_UInt64 addressMask = 0xfffe000000000000;
  OpcUa_UInt64 dataMask    = 0x0000ffffffffffff;
  OpcUa_UInt64 idRegMask = 0x0001000000000000;

  OpcUa_UInt64 readingMask1=0x0000000000000fff;
  OpcUa_UInt64 readingMask2=0x0000000000fff000;
  OpcUa_UInt64 readingMask3=0x0000000fff000000;
  OpcUa_UInt64 readingMask4=0x0000fff000000000;

  OpcUa_UInt64 readValue1;
  OpcUa_UInt64 readValue2;
  OpcUa_UInt64 reorderedValue;
  bool pairRegister = !((value & idRegMask)>0);
  OpcUa_UInt64 readingReg1, readingReg2;  
  UaStatus s = OpcUa_Bad;
  readingReg1 = (value & addressMask) - 0x0010000000000000;
  readingReg2 = readingReg1 + 0x0001000000000000;
  while(writeError<writeErrorMAX){
  s =writeADC(nodeId_write,value,session);
  if (!s.isGood()){return s;}
    readError=0;
    while(readError<readErrorMAX){
      s =writeADC(nodeId_write,readingReg1,session);
      if (!s.isGood()){return s;}
      s =readADC(nodeId_read,readValue1,session);
      if (!s.isGood()){return s;}
      s =writeADC(nodeId_write,readingReg2,session);
      if (!s.isGood()){return s;}
      s =readADC(nodeId_read,readValue2,session);
      if (!s.isGood()){return s;}

      if (pairRegister){
        reorderedValue= (readValue1 & readingMask4) + ((readValue1 & readingMask2)<<12) + ((readValue2 & readingMask4)>>24) + ((readValue2 & readingMask2)>>12) ;
      }
      else{
        reorderedValue= ((readValue1 & readingMask3)<<12) + ((readValue1 & readingMask1)<<24) + ((readValue2 & readingMask3)>>12) + ((readValue2 & readingMask1)) ;
      }
      if (reorderedValue==(value & dataMask)){
        return OpcUa_Good;
      }
      readError++;
      LOG(Log::WRN) << logID <<"Read Loop "<< readError <<"Read data("<<std::hex<< reorderedValue <<") don't correspond to written value:"<<value ;
    }
    writeError++;
    LOG(Log::WRN) << logID <<"Write Loop "<< writeError <<"Read data("<<std::hex<< reorderedValue <<") don't correspond to written value:"<<value ;
  }
  LOG(Log::ERR) << logID <<"Data don't correspond to written value:" ;
  return OpcUa_Bad;
}

  UaStatus writeAndCheckADCWithoutInterLeave (string nodeId_read,string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session){
      int readError=0, writeError=0;
      int readErrorMAX=5, writeErrorMAX=5;
      OpcUa_UInt64 addressMask = 0xffff000000000000;
      OpcUa_UInt64 dataMask    = 0x0000ffffffffffff;

      OpcUa_UInt64 readValue;
      OpcUa_UInt64 readingReg;
      UaStatus s = OpcUa_Bad;

      readingReg = (value & addressMask) - 0x0010000000000000;
      while(writeError<writeErrorMAX){
        s =writeADC(nodeId_write,value,session);
        if (!s.isGood()){return s;}
        readError=0;
        while(readError<readErrorMAX){
          s =writeADC(nodeId_write,readingReg,session);
          if (!s.isGood()){return s;}
          s =readADC(nodeId_read,readValue,session);
          if (!s.isGood()){return s;}

          if (readValue==(value & dataMask)){
            return OpcUa_Good;
          }
          readError++;
          LOG(Log::WRN) << logID <<"Read Loop "<< readError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
        }
        writeError++;
        LOG(Log::WRN) << logID <<"Write Loop "<< writeError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
      }
      LOG(Log::ERR) << logID <<"Data don't correspond to written value:" ;
      return OpcUa_Bad;
    }


UaStatus writeADC (string nodeId_write, OpcUa_UInt64 value,UaClientSdk::UaSession* session)
{   
  UaByteString bs;
  OpcUa_Byte tOneByte[8];
  try{
    for (int i=0;i<8;i++){
      tOneByte[7-i]=value & 255;
      value=value >> 8;
    }
   
    LOG(Log::INF) << logID << "writeADC : baseNodeId " + nodeId_write;  
    UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_write.c_str(),2));
    bs.setByteString(sizeof tOneByte,tOneByte);
    registerId.writeValue(bs);
  }
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  }   
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  } 
  return OpcUa_Good;
}

UaStatus readADC(string nodeId_read, OpcUa_UInt64 & value,UaClientSdk::UaSession* session)
  {
    UaByteString bs;
    value=0;
    try{
      UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_read.c_str(),2));
      UaByteString readBs(registerId.readValue());
      if (readBs.length()!=8){
        LOG(Log::ERR) << logID <<"Error reading register "+ nodeId_read;
        return OpcUa_Bad;
      }
      for (int i=0;i<8;i++){
        value = value <<8;
        LOG(Log::INF)<<"data n"<<i <<"="<<(int) readBs.data()[i];
        value+=(int) readBs.data()[i];
        LOG(Log::INF)<<"value ="<<std::hex<< value;
      }
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
      LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
      return e.statusCode();
    }
    catch(const std::exception& e){
      LOG(Log::ERR) << logID <<"Caught:"<<e.what();
      return OpcUa_Bad;
    }
    return OpcUa_Good;
  }

string formatNcaracters(string num, int size_format) {
    string s = num;
    while (s.size() < size_format) s = "0" + s;
    return s;
}


std::vector<std::string> tokenize(std::string value,  std::string separator){

  std::vector<std::string> tokens;
   std::string::size_type start = 0, end = 0;
   while ((end = value.find(separator, start)) != std::string::npos){
     std::string token = value.substr(start, end - start);
     if(token.size()) tokens.push_back(token);
     start = end + 1;
   }
   std::string lasttoken = value.substr(start);
   if(lasttoken.size()) tokens.push_back(lasttoken);

   return tokens;

}

  UaStatus secureSTF (std::string svalue1, float &output){

  std::string::size_type pos;
  try{
    output = std::stof(svalue1, &pos);
    if(pos != svalue1.size()){
      LOG(Log::ERR) <<  logID << "ERROR in string format on file";
      return OpcUa_BadInternalError;
    }
  }
  catch(const std::exception& err) {
    LOG(Log::ERR) <<  logID << "ERROR not able to convert";
    return OpcUa_BadInternalError;
  }
  return OpcUa_Good;
  }
