#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;

//declaration des fonctions
UaStatus resetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);
UaStatus readMtx(UaClientSdk::UaSession* session,string ltdb_id, string sca_id, string mtx_id, string reg_id, int *readed_value);
UaStatus writeMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, string mtxId, string reg_id, OpcUa_Byte value);
std::string tostr (int x);


//main
int main(int argc, const char *argv[])
{
	//boost's programm option
	UaPlatformLayer::init();
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "Mtx Stability Test")
		("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
		("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
		("scaId", po::value<string>()->default_value("1"), "sca id")
		("mtxId", po::value<string>()->default_value("1"), "mtx id")
		("regId", po::value<string>()->default_value("1"), "register id")
		//("value", po::value<string>()->default_value("1"), "value to write on the mtx's register")
		("nbTests", po::value<int>()->default_value(1), "number of test to do")
		("tsleep", po::value<int>()->default_value(0), "time of sleep between a reset and a read, in microsecond")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	
	//help
	if (vm.count("help")) {
		cout << desc << "\n";
		return 1;
	}
	
	//init opcua session, and log
	UaStatus s = OpcUa_Bad;
	Log::initializeLogging(Log::INF);
	UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
	if (!session){
		LOG(Log::ERR)<<"Bad communication Error";
		return 0;
	}
	
	//variable
	int i = 0;
	int nbErrReset = 0;
	int nbErrWrite = 0;
	int nbErrOpc = 0;
	int nbResetLoop = 0;
	int nbWriteLoop = 0;
	
	int nb_test = vm["nbTests"].as<int>();
	int tsleep = vm["tsleep"].as<int>();
	
	OpcUa_Byte value_reset = 0;
	if(vm["regId"].as<string>() == "0" || vm["regId"].as<string>() == "2")
	{
		value_reset = 4;
	}
	if(vm["regId"].as<string>() == "1" || vm["regId"].as<string>() == "3")
	{
		value_reset = 25;
	}
	OpcUa_Byte value = value_reset;
	//OpcUa_Byte value = vm["value"].as<int>();
	
	
	//test loop
	for(i = 0; i < nb_test; i ++)
	{
		if(i%200==0)
		{
			cout << "	- loop " << i << " / " << nb_test << endl;
		}
		//reset
		s = resetMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
		if (!s.isGood()){nbErrOpc ++; continue;}
		
		//optionnal sleep
		if(tsleep != 0){usleep(tsleep);}
		
		//read after reset
		int le_truc_lu;
		s = readMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), vm["mtxId"].as<string>(), vm["regId"].as<string>(), &le_truc_lu);
		if (!s.isGood()){nbErrOpc ++; continue;}
		if(le_truc_lu != value_reset)
		{
			nbErrReset ++;
			LOG(Log::ERR)<<"Error after a reset";
		}
		nbResetLoop ++;
		
		//write
		s = writeMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>() , vm["mtxId"].as<string>(), vm["regId"].as<string>(), value);
		if (!s.isGood()){nbErrOpc ++; continue;}
		
		//read after write
		s = readMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), vm["mtxId"].as<string>(), vm["regId"].as<string>(), &le_truc_lu);
		if (!s.isGood()){nbErrOpc ++; continue;}
		if(le_truc_lu != value)
		{
			nbErrWrite ++;
			LOG(Log::ERR)<<"Error after a write";
		}
		nbWriteLoop ++;
		
	}
	
	cout << endl;
	cout << "        |---------------Result---------------|" << endl;
	cout << "        |    -nb reset error : "<< nbErrReset << endl;
	cout << "        |    -nb write error : "<< nbErrWrite << endl;
	cout << "        |    -nb opcua error : "<< nbErrOpc << endl;
	cout << "        |    -nb reset loop finished : "<< nbResetLoop << endl;
	cout << "        |    -nb write loop finished : "<< nbResetLoop << endl;
	cout << "        |------------------------------------|" << endl;
	cout << endl;
	
	delete session;
}


UaStatus resetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
	try
	{
		OpcUa_Boolean vtrue=1;
		OpcUa_Boolean vfalse=0;
		std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
		std::string nodeId=baseNodeId + ".gpio.MTX-RST";
		UaoClientForOpcUaSca::DigitalIO mtrxio(session,UaNodeId(nodeId.c_str(),2));
		mtrxio.writeValue(vtrue);
		mtrxio.writeValue(vfalse);
		//LOG(Log::INF) << logID <<"resetMtx : Reset done";

	}
	catch(const std::exception& e)
	{
		LOG(Log::ERR) << logID <<"Caught:"<<e.what();
		return OpcUa_Bad;
	}
	return OpcUa_Good;    
}

UaStatus readMtx(UaClientSdk::UaSession* session,string ltdb_id, string sca_id, string mtx_id, string reg_id, int *readed_value)
{
	OpcUa_Byte baseAddress[2] = {0x04,0x08};
	int bus_number[2] = {4,6};
	std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
	int mtx_number=1;
	try
	{  
		//for (int bus_n=0; bus_n<2; bus_n++){
		//for (int b_add=0; b_add<2; b_add++){
		int bus_n=0;
		int b_add=0;
		if(mtx_id == "1")
		{
			bus_n=0;
			b_add=0;
			mtx_number=1;
		}
		else if (mtx_id == "2")
		{
			bus_n=0;
			b_add=1;
			mtx_number=2;
		}
		else if (mtx_id == "3")
		{
			bus_n=1;
			b_add=0;
			mtx_number=3;
		}
		else if (mtx_id == "4")
		{
			bus_n=1;
			b_add=1;
			mtx_number=4;
		}
		else
		{
			bus_n=0;
			b_add=0;
		}
		//LOG(Log::INF) << logID <<"MTX_" << mtx_number;
		mtx_number ++;
		int r = stoi(reg_id);
		std::string nodeId =baseNodeId+".BusI2C_" + tostr(bus_number[bus_n]) +".register0" + tostr(r+baseAddress[b_add]);
		UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
		UaByteString readBs(registerId.readValue());
		//LOG(Log::INF) << logID << "Register_" << r << "  => (0x"<< std::hex << (int)readBs.data()[0] << ")";
		*readed_value = (int)readBs.data()[0];


	}
	catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
	{
		LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
		return e.statusCode();
	}
	catch(const std::exception& e)
	{
		LOG(Log::ERR) << logID <<"Caught:"<<e.what();
		return OpcUa_Bad;
	}
	return OpcUa_Good;
}

UaStatus writeMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, string mtx_id, string reg_id, OpcUa_Byte value)
{ 
      OpcUa_Byte busNumber;
      OpcUa_Byte baseAddress;

    if(mtx_id=="1" || mtx_id == "3"){
      baseAddress=0x4;
    }
    else if(mtx_id=="2" || mtx_id == "4"){
      baseAddress=0x8;
    }
    if(mtx_id=="1" || mtx_id == "2"){
      busNumber=4;
    }
    else if(mtx_id=="3" || mtx_id == "4"){
      busNumber=6;
    }

    try{
        UaStatus s=OpcUa_Bad;
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId =baseNodeId + ".BusI2C_" + tostr(busNumber) +".register0";
        std::string logID="";
    
        OpcUa_Byte  writeValue4,writeValue5;
        //Creation of the I2C nodes
        //LOG(Log::INF) << logID <<"Status register4";

        
        int baseAddressIncrement = stoi(reg_id);
        UaByteString bs;
        bs.setByteString(sizeof value, &value);
        
        std::string regId=nodeId+tostr(baseAddress+baseAddressIncrement);
        if(baseAddressIncrement == 0)
        {
			UaoClientForOpcUaSca::I2cSlave reg0(session,UaNodeId(regId.c_str(),2));
			reg0.writeValue(bs);
			
			UaByteString register0_value(reg0.readValue());
			//LOG(Log::INF) << logID << "writeMtrx : Register0 value => (0x"<< std::hex << (int)*register0_value.data() << ")";
		}
		else if(baseAddressIncrement == 1)
        {
			UaoClientForOpcUaSca::I2cSlave reg1(session,UaNodeId(regId.c_str(),2));
			reg1.writeValue(bs);
			
			UaByteString register1_value(reg1.readValue());
			//LOG(Log::INF) << logID << "writeMtrx : Register1 value => (0x"<< std::hex << (int)*register1_value.data() << ")";
		}
		else if(baseAddressIncrement == 2)
        {
			UaoClientForOpcUaSca::I2cSlave reg2(session,UaNodeId(regId.c_str(),2));
			reg2.writeValue(bs);
			
			UaByteString register2_value(reg2.readValue());
			//LOG(Log::INF) << logID << "writeMtrx : Register2 value => (0x"<< std::hex << (int)*register2_value.data() << ")";
		}
		else if(baseAddressIncrement == 3)
        {
			UaoClientForOpcUaSca::I2cSlave reg3(session,UaNodeId(regId.c_str(),2));
			reg3.writeValue(bs);
			
			UaByteString register3_value(reg3.readValue());
			//LOG(Log::INF) << logID << "writeMtrx : Register3 value => (0x"<< std::hex << (int)*register3_value.data() << ")";
		}

    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
    {
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}


std::string tostr (int x)
{
	std::stringstream str;
	str <<std::hex<< x;
	return str.str();
}

