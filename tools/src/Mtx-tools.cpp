#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>

//READ
namespace po = boost::program_options;
UaStatus readMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);
std::string tostr (int x);

//READ-RESET
UaStatus readResetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);

//RESET
UaStatus resetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);

//WRITE-RESET
UaStatus writeResetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reset_value);

//WRITE
UaStatus writeMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, string mtxId ,OpcUa_Byte value0, OpcUa_Byte value1, OpcUa_Byte value2, OpcUa_Byte value3);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "Tool box for Mtx")
        //READ
        ("READ", "Mtx Read")
        //WRITE
        ("WRITE", "Mtx write")
        ("mtxId", po::value<string>()->default_value("1"), "mtx id")
        ("reg0", po::value<int>()->default_value(4), "ltdb id")
        ("reg1", po::value<int>()->default_value(25), "ltdb id")
        ("reg2", po::value<int>()->default_value(4), "ltdb id")
        ("reg3", po::value<int>()->default_value(25), "ltdb id")
        

        ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
        ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
        ("scaId", po::value<string>()->default_value("1"), "sca id")
        //READ-RESET
        ("READ-RESET", "Status of MTX reset register")
        //RESET
        ("RESET", "MTX Reset")
        //WRITE-RESET
        ("WRITE-RESET", "MTX write reset register")
        ("resetValue",po::value<string>()->default_value("false"),"reset value")
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }

//WRITE
    if (vm.count("WRITE")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }

        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) << logID <<" Mtx Write Registers";
        LOG(Log::INF) << logID <<"-------------------------";
        int reg0=vm["reg0"].as<int>();
        int reg1=vm["reg1"].as<int>();
        int reg2=vm["reg2"].as<int>();
        int reg3=vm["reg3"].as<int>();
        s = writeMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>() , vm["mtxId"].as<string>(), reg0,reg1,reg2,reg3);
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }
    //READ
    if (vm.count("READ")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }

        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) << logID <<" Mtx status Registers";
        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) <<logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();

        s = readMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }

    //READ-RESET
    if (vm.count("READ-RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"------------------------------------";
        LOG(Log::INF) << logID <<" MTX  Status Reset Register";
        LOG(Log::INF) << logID <<"------------------------------------";

        LOG(Log::INF) << logID <<" Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>();
        s = readResetMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }

    //RESET
    if (vm.count("RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"------------";
        LOG(Log::INF) << logID <<" MTX Reset ";
        LOG(Log::INF) << logID <<"------------";

        LOG(Log::INF) << logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_" + vm["scaId"].as<string>();
        s = resetMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }

    //WRITE-RESET
    if (vm.count("WRITE-RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) << logID <<" MTX write Reset register ";
        LOG(Log::INF) << logID <<"-------------------------";

        LOG(Log::INF) << logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_" + vm["scaId"].as<string>();
        bool rv = (vm["resetValue"].as<string>()=="true");
        LOG(Log::INF) << logID << "restValue_" << rv ;
        s = writeResetMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),rv);
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";
        delete session;

        return 0;
    }
}


//READ
UaStatus readMtx(UaClientSdk::UaSession* session,string ltdb_id, string sca_id)
{
    OpcUa_Byte baseAddress[2] = {0x04,0x08};
    int bus_number[2] = {4,6};
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
    int mtx_number=1;
    try
    {  
        for (int bus_n=0; bus_n<2; bus_n++){
            for (int b_add=0; b_add<2; b_add++){
                LOG(Log::INF) << logID <<"MTX_" << mtx_number;
                mtx_number ++;
                for (int r=0; r<4; r++){
                    { 
                        std::string nodeId =baseNodeId+".BusI2C_" + tostr(bus_number[bus_n]) +".register0" + tostr(r+baseAddress[b_add]);
                        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
                        UaByteString readBs(registerId.readValue());
                        /*if (readBs.length()!=1)
                          {
                          return OpcUa_Bad;
                          }*/
                        LOG(Log::INF) << logID << "Register_" << r << "  => (0x"<< std::hex << (int)readBs.data()[0] << ")";
                    }
                } 
            }
        }
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
    {
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}

std::string tostr (int x)
{
    std::stringstream str;
    str <<std::hex<< x;
    return str.str();
}

//READ-RESET
UaStatus readResetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
    try{
        UaByteString readBs;
        OpcUa_Boolean vfalse=0;
        OpcUa_Boolean vtrue=1;
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.MTX-RST";
        UaoClientForOpcUaSca::DigitalIO Mtx_1to4(session,UaNodeId(nodeId.c_str(),2));

        OpcUa_Boolean Mtx_reset_value(Mtx_1to4.readValue());
        LOG(Log::INF) << logID << " readResetMtx : Reset_1_to_4 registre value (" << (bool)Mtx_reset_value << ")";
    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}

//RESET
UaStatus resetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
    try{
        OpcUa_Boolean vtrue=1;
        OpcUa_Boolean vfalse=0;
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.MTX-RST";
        UaoClientForOpcUaSca::DigitalIO mtrxio(session,UaNodeId(nodeId.c_str(),2));
        mtrxio.writeValue(vtrue);
        mtrxio.writeValue(vfalse);
        LOG(Log::INF) << logID <<"resetMtx : Reset done";

    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}

//WRITE-RESET
UaStatus writeResetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reset_value)
{
    try{
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.MTX-RST";
        UaoClientForOpcUaSca::DigitalIO mtrxio(session,UaNodeId(nodeId.c_str(),2));
        mtrxio.writeValue(reset_value);
        if (reset_value)
            LOG(Log::INF) << logID <<"writeResetMtx : Reset is true";
        else 
            LOG(Log::INF) << logID <<"writeResetMtx : Reset is false";
    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}
//WRITE
UaStatus writeMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, string mtxId ,OpcUa_Byte value0, OpcUa_Byte value1, OpcUa_Byte value2, OpcUa_Byte value3)
{ 
      OpcUa_Byte busNumber;
      OpcUa_Byte baseAddress;

    if(mtxId=="1" || mtxId == "3"){
      baseAddress=0x4;
    }
    else if(mtxId=="2" || mtxId == "4"){
      baseAddress=0x8;
    }
    if(mtxId=="1" || mtxId == "2"){
      busNumber=4;
    }
    else if(mtxId=="3" || mtxId == "4"){
      busNumber=6;
    }

    try{
        UaStatus s=OpcUa_Bad;
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId =baseNodeId + ".BusI2C_" + tostr(busNumber) +".register0";
        std::string logID="";

        LOG(Log::INF) << logID <<"---------------------------------";
        LOG(Log::INF) << logID <<" Set MTX registers";
        LOG(Log::INF) << logID <<"---------------------------------";
        LOG(Log::INF) << logID << nodeId ;
    
        OpcUa_Byte  writeValue4,writeValue5;
        //Creation of the I2C nodes
        //LOG(Log::INF) << logID <<"Status register4";

        std::string regId=nodeId+tostr(baseAddress);
        UaoClientForOpcUaSca::I2cSlave reg0(session,UaNodeId(regId.c_str(),2));
        regId=nodeId+tostr(baseAddress+1);
        UaoClientForOpcUaSca::I2cSlave reg1(session,UaNodeId(regId.c_str(),2));
        regId=nodeId+tostr(baseAddress+2);
        UaoClientForOpcUaSca::I2cSlave reg2(session,UaNodeId(regId.c_str(),2));
        regId=nodeId+tostr(baseAddress+3);
        UaoClientForOpcUaSca::I2cSlave reg3(session,UaNodeId(regId.c_str(),2));

        UaByteString bs;
        bs.setByteString(sizeof value0, &value0);
        reg0.writeValue(bs);

        UaByteString bs1;
        bs1.setByteString(sizeof value1, &value1);
        reg1.writeValue(bs1);

        UaByteString bs2;
        bs2.setByteString(sizeof value2, &value2);
        reg2.writeValue(bs2);

        UaByteString bs3;
        bs3.setByteString(sizeof value3, &value3);
        reg3.writeValue(bs3);

        UaByteString register0_value(reg0.readValue());
        UaByteString register1_value(reg1.readValue());
        UaByteString register2_value(reg2.readValue());
        UaByteString register3_value(reg3.readValue());

        LOG(Log::INF) << logID << "writeMtrx : Register0 value => (0x"<< std::hex << (int)*register0_value.data() << ")";
        LOG(Log::INF) << logID << "writeMtrx : Register1 value => (0x"<< std::hex << (int)*register1_value.data() << ")";
        LOG(Log::INF) << logID << "writeMtrx : Register2 value => (0x"<< std::hex << (int)*register2_value.data() << ")";
        LOG(Log::INF) << logID << "writeMtrx : Register3 value => (0x"<< std::hex << (int)*register3_value.data() << ")";

    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
    {
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}

