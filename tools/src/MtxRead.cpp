#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus readMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);
std::string tostr (int x);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "Mtx Read")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }

    LOG(Log::INF) << logID <<"-------------------------";
    LOG(Log::INF) << logID <<" Mtx status Registers";
    LOG(Log::INF) << logID <<"-------------------------";
    LOG(Log::INF) <<logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();

    s = readMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

    delete session;

    return 0;
}


UaStatus readMtx(UaClientSdk::UaSession* session,string ltdb_id, string sca_id)
{
    OpcUa_Byte baseAddress[2] = {0x04,0x08};
    int bus_number[2] = {4,6};
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
    int mtx_number=1;
    try
    {  
        for (int bus_n=0; bus_n<2; bus_n++){
          for (int b_add=0; b_add<2; b_add++){
            LOG(Log::INF) << logID <<"MTX_" << mtx_number;
            mtx_number ++;
            for (int r=0; r<4; r++){
            { 
              std::string nodeId =baseNodeId+".BusI2C_" + tostr(bus_number[bus_n]) +".register0" + tostr(r+baseAddress[b_add]);
              UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
              UaByteString readBs(registerId.readValue());
              /*if (readBs.length()!=1)
              {
                return OpcUa_Bad;
              }*/
              LOG(Log::INF) << logID << "Register_" << r << "  => (0x"<< std::hex << (int)readBs.data()[0] << ")";
            }
          } 
        }
      }
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
    {
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}

std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}
