//READ-REGISTER
#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>
#include <string>

//READ-RESET

//RESET

//SET-DELAY

//WRITE

//WRITE-REGISTER
#include <sstream>

//WRITE-RESET


//READ-REGISTER
namespace po = boost::program_options;

UaStatus readLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);
std::string tostr (int x);

//READ-RESET
UaStatus readLocx2_reset(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);

//RESET
UaStatus resetLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);

//SET-DELAY
UaStatus setDelayAllFiber (string ltdbId, string scaId, int locx2Id ,OpcUa_Byte delay, OpcUa_Byte clkphase,UaClientSdk::UaSession* session);

//WRITE
UaStatus writeLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reseat_value);

//WRITE-REGISTER
UaStatus writeRegisterLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, int locx2_id, int reg_number, OpcUa_Byte reg_value, OpcUa_Boolean setDefVal);
UaStatus secureI2CWrite( UaoClientForOpcUaSca::I2cSlave reg , OpcUa_Byte value, std::string nodeId, int retry = 5);

//WRITE-RESET
UaStatus writeResetLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reseat_value);


int main(int argc, const char *argv[])
{
    //READ-REGISTER
    UaPlatformLayer::init();
    char const* warng=  "\n***************************************"
        "\n@ WARNING! : The setDefaultValue flag @"
        "\n@ is set true by default. If you want @"
        "\n@ to set a different value you must   @"
        "\n@ disable it.                         @"            
        "\n@------------------------------------ @"
        "\n@ Default values by register:         @"
        "\n@     register0   => 0x14,            @"
        "\n@     register1   => 0x34,            @"
        "\n@     register2   => 0xE7,            @"
        "\n@     register3   => 0x9C             @"
        "\n***************************************";
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "Tools box for LOCX2")
        ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
        ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
        ("scaId", po::value<string>()->default_value("1"), "sca id")
        ("READ-REGISTER", "LOCX2 registers status")
        //READ-RESET
        ("READ-RESET", "Status of reset register")
        //RESET
        ("RESET", "Reset of Locx2s 1 to 4")
        //SET-DELAY
        ("SET-DELAY", "Set LOCX2 delay")
        ("locx2Id", po::value<string>()->default_value("1"), "locx2 id")
        ("delay",po::value<string>()->default_value("9"),"delay value: 9 to 26")
        ("clkphase",po::value<string>()->default_value("1"),"clkphase value")
        //WRITE
        ("WRITE", "Reset of Locx2s 1 to 4")
        ("restValue",po::value<string>()->default_value("false"),"reset value")
        //WRITE-REGISTER
        ("WRITE-REGISTER", "Allows writing of each specified Locx2 register")
        ("regNumber",po::value<string>()->default_value("0"),"register number [0 to 3] ")
        //("value",po::value<string>()->default_value("0x0"),"Value to set. Warning: Desable setDefaultValue flag, enabled by default")
        ("value",po::value<string>()->default_value("0"),"Value to set. ")
        ("setDefaultValue",po::value<string>()->default_value("true"),warng)
        //WRITE-RESET
        ("WRITE-RESET", "Modifify the value of reset reguster")
        ("resetValue",po::value<string>()->default_value("false"),"reset value")

        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }

    //READ-REGISTER
    if (vm.count("READ-REGISTER")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }

        int  delayA, delayB, delayC, delayD;
        int low_pass_filter,frecuency,band_selection,charge_pump; 
        int read1, read2,read3,read4;

        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) << logID <<" LOCX2s Registers status ";
        LOG(Log::INF) << logID <<"-------------------------";
        LOG(Log::INF) <<logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();
        LOG(Log::INF) << logID <<"-------------------------";
        s = readLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());

        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }

    //READ-RESET
    if (vm.count("READ-RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"------------------------------------";
        LOG(Log::INF) << logID <<" LOCX2  Status Reset Register";
        LOG(Log::INF) << logID <<"------------------------------------";

        LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>();
        s = readLocx2_reset(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;
        return 0;
    }

    //RESET
    if (vm.count("RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"--------------";
        LOG(Log::INF) << logID <<" LOCX2  Reset";
        LOG(Log::INF) << logID <<"--------------";

        LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>();
        s = resetLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());

        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }

    //SET-DELAY
    if (vm.count("SET-DELAY")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"---------------------------";
        LOG(Log::INF) << logID <<" LOCX2 set delay";
        LOG(Log::INF) << logID <<"---------------------------";

        LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>() << endl;
        OpcUa_Byte delay = (OpcUa_Byte)strtol(vm["delay"].as<string>().c_str(), NULL, 16);
        OpcUa_Byte clkphase = (OpcUa_Byte)strtol(vm["clkphase"].as<string>().c_str(), NULL, 16);
        int locx2Id = stoi(vm["locx2Id"].as<string>().c_str());
        LOG(Log::INF) << logID <<"Locx2Id :  "<< locx2Id << ", delay : " << tostr(delay) << ", clkphase : " << tostr(clkphase) << endl;

        s = setDelayAllFiber(vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),locx2Id,delay,clkphase,session);
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }

    //WRITE
    if (vm.count("WRITE")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str()); //You need to have a valid OpcUaendpoints
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"---------------------------";
        LOG(Log::INF) << logID <<"LOCX2 Write Reset Register";
        LOG(Log::INF) << logID <<"---------------------------";

        LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>();
        OpcUa_Boolean r_value;
        s = writeLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), OpcUa_Boolean(vm["restValue"].as<bool>()));
        delete session;

        return 0;
    }

    //WRITE-REGISTER
    if (vm.count("WRITE-REGISTER")) {
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"-----------------------";
        LOG(Log::INF) << logID <<"LOCX2 Write  Register  ";
        LOG(Log::INF) << logID <<"-----------------------";
        LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();

        OpcUa_Byte r_value = (OpcUa_Byte)strtol(vm["value"].as<string>().c_str(), NULL, 16);
        int locId= std::stoi(vm["locx2Id"].as<string>().c_str());
        int regNum= std::stoi(vm["regNumber"].as<string>().c_str());
        bool dv = (vm["setDefaultValue"].as<string>() == "true");
        LOG(Log::INF) << logID <<"locx2Id : " <<  locId;
        //LOG(Log::INF) << logID <<"regNumber : " << regNum;
        LOG(Log::INF) << logID <<"value_ : (0x" << std::hex << (int) r_value << ")";
        LOG(Log::INF) << logID <<"setDefaultValue : " << dv;  
        LOG(Log::INF) << logID <<"-----------------------";
        UaStatus s = OpcUa_Bad;
        s = writeRegisterLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),locId,regNum, r_value, dv);;
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";
        delete session;

        return 0;
    }

    //WRITE-RESET
    if (vm.count("WRITE-RESET")) {
        UaStatus s = OpcUa_Bad;
        Log::initializeLogging(Log::INF);
        UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
        if (!session){
            LOG(Log::ERR)<<"Bad communication Error";
            return 0;
        }
        LOG(Log::INF) << logID <<"---------------------------";
        LOG(Log::INF) << logID <<"LOCX2 Write Reset Register";
        LOG(Log::INF) << logID <<"---------------------------";

        LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>();
        bool rv = (vm["resetValue"].as<string>()=="true");
        //LOG(Log::INF) << logID <<"Value of resert received from vm " << rv;

        s = writeResetLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),rv);
        if (s.isGood()){
            LOG(Log::INF) << " Operation successfully done " << endl;
        }
        else
            LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

        delete session;

        return 0;
    }
}


//READ-REGISTER
UaStatus readLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
    OpcUa_Byte baseAddress[2] = {0x04,0x08};
    int bus_number[2] = {5,7};
    int locx2_number=1;
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
    try{
      for (int j=0; j<2; j++){
        for (int b=0; b<2; b++){
          LOG(Log::INF) << logID <<"LOCX2_" << locx2_number;
          int  delayA, delayB, delayC, delayD;
          int low_pass_filter,frecuency,band_selection,charge_pump;
          int  readV[4];
          locx2_number ++;

          for (int reg=0; reg<4; reg++){ 
            std::string nodeId =baseNodeId+".BusI2C_" + tostr(bus_number[j]) +".register0" + tostr(reg+baseAddress[b]);
            UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
            UaByteString read_val(registerId.readValue());
            readV[reg] = (int) read_val.data()[0];
            if(reg==0){
                //Low pass filter 3rd selection (R3)
                low_pass_filter=((readV[reg] & 192)>>6);
                LOG(Log::INF) << logID << "Low pass filter 3rd selection (R3) => (0x"<< std::hex << low_pass_filter << ")";
                //Frequency band selection.
                frecuency=((readV[reg] & 48)>>4);
                LOG(Log::INF) << logID << "Frequency band selection           => (0x"<< std::hex << frecuency << ")";
                //Band_selection
                band_selection=((readV[reg] & 12)>>2);
                LOG(Log::INF) << logID << "Low pass filter bandwidth selection=> (0x"<< std::hex << band_selection << ")";
                //Charge pump
                charge_pump=((readV[reg] & 3));
                LOG(Log::INF) << logID << "Charge pump current (μA)           => (0x"<< std::hex << charge_pump << ")";  
            }   
          } 
          //Reading delays
          delayA=(readV[1] >>4) + ((readV[2] & 1)<<4);
          LOG(Log::INF) << logID << "delayA                             => ("<< delayA   << ")";
          delayB=(readV[2] & 62)>>1;
          LOG(Log::INF) << logID << "delayB                             => ("<< delayB   << ")";
          delayC=((readV[2] & 192)>>6) + ((readV[3] & 7)<<2);
          LOG(Log::INF) << logID << "delayC                             => ("<< delayC   << ")";
          delayD=(readV[3] & 248)>>3;
          LOG(Log::INF) << logID << "delayD                             => ("<< delayD   << ")";
        }
      }
    }
    
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    } 
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}

std::string tostr (int x)
{
    std::stringstream str;
    str <<std::hex<< x;
    return str.str();
}


//READ-RESET
UaStatus readLocx2_reset(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
    try{
        UaByteString readBs;
        OpcUa_Boolean vfalse=0;
        OpcUa_Boolean vtrue=1;
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.LOCx2-RST";
        UaoClientForOpcUaSca::DigitalIO Locx2_1to4(session,UaNodeId(nodeId.c_str(),2));

        OpcUa_Boolean Locx2_reset_value(Locx2_1to4.readValue());
        LOG(Log::INF) << logID << "readLocx2 : Reset_1_to_4 registre value (" << (bool)Locx2_reset_value << ")";
    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}


//RESET
UaStatus resetLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{
    try{

        OpcUa_Boolean vfalse=0;
        OpcUa_Boolean vtrue=1;
        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.LOCx2-RST";
        UaoClientForOpcUaSca::DigitalIO Locx2_1to4(session,UaNodeId(nodeId.c_str(),2));

        Locx2_1to4.writeValue(vtrue);
        Locx2_1to4.writeValue(vfalse);
        //LOG(Log::INF) << logID <<"Reset done";

    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}

//SET-DELAY
UaStatus setDelayAllFiber (string ltdbId, string scaId, int locx2Id, OpcUa_Byte delay,
        OpcUa_Byte clkphase,UaClientSdk::UaSession* session)
{
    OpcUa_Byte offset = 0;
    OpcUa_Byte adcType = 0;
    OpcUa_Byte busNumber;
    OpcUa_Byte baseAddress;
    OpcUa_Byte readingMask0a=192;
    UaByteString bs;
    OpcUa_Byte oneByte;
    OpcUa_Byte delayA =delay;
    OpcUa_Byte delayB = delay+offset;
    OpcUa_Byte delayC =delay;
    OpcUa_Byte delayD = delay+offset;

    try{
        //Set base address
        if(locx2Id==1 || locx2Id==3){
            baseAddress=0x4;
        }
        else if(locx2Id==2 || locx2Id==4){
            baseAddress=0x8;
        }
        //set bus number
        if(locx2Id ==1 || locx2Id==2){
            busNumber=5;
        }
        else if(locx2Id==3 || locx2Id == 4){
            busNumber=7;
        }

        string baseNodeId="ltdb_"+ltdbId+".scaFelix"+scaId+".BusI2C_" + tostr(busNumber);
        string nodeId = baseNodeId+".register0" + tostr(baseAddress);

        UaoClientForOpcUaSca::I2cSlave register1(session,UaNodeId(nodeId.c_str(),2));

        nodeId =baseNodeId+".register0" + tostr(baseAddress+1);
        UaoClientForOpcUaSca::I2cSlave register2(session,UaNodeId(nodeId.c_str(),2));

        nodeId =baseNodeId+".register0" + tostr(baseAddress+2);
        UaoClientForOpcUaSca::I2cSlave register3(session,UaNodeId(nodeId.c_str(),2));

        nodeId =baseNodeId+".register0" + tostr(baseAddress+3);
        UaoClientForOpcUaSca::I2cSlave register4(session,UaNodeId(nodeId.c_str(),2));

        //Register 08
        oneByte=0x14;
        bs.setByteString(sizeof oneByte,&oneByte);
        register1.writeValue(bs);

        //Register 09
        oneByte=(((delayA % 16) << 4) + ((clkphase & 3 )<<2) + (adcType & 3));
        bs.setByteString(sizeof oneByte,&oneByte);
        register2.writeValue(bs);

        //Register 0a
        oneByte=(delayB << 1) + (delayA >> 4) + ((delayC % 4 ) << 6);
        bs.setByteString(sizeof oneByte,&oneByte);
        register3.writeValue(bs);

        //Register 0b
        oneByte = (delayD << 3) + ( delayC >> 2 ) ;
        bs.setByteString(sizeof oneByte,&oneByte);
        register4.writeValue(bs);
        //LOG(Log::INF) << logID <<"Set delay done" << endl;

    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}


//WRITE
UaStatus writeLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reseat_value)
{
    try{

        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.LOCx2-RST";
        UaoClientForOpcUaSca::DigitalIO Locx2_1to4(session,UaNodeId(nodeId.c_str(),2));

        Locx2_1to4.writeValue(reseat_value);
        LOG(Log::INF) << logID <<"The resetregister was succesfully actualised";

    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}


//WRITE-REGISTER
UaStatus writeRegisterLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, int locx2_id, int reg_number, OpcUa_Byte reg_value, OpcUa_Boolean setDefVal)
{
    OpcUa_Byte baseAddress, busNumber;
    OpcUa_Byte defVal_reg[4]= {0x14,0x34,0xE7,0x9C};
    UaByteString bs; 
    try{

        //Set base address
        if(locx2_id==1 || locx2_id==3){
            baseAddress=0x4;
        }
        else if(locx2_id==2 || locx2_id==4){
            baseAddress=0x8;
        }
        //set bus number
        if(locx2_id ==1 || locx2_id==2){
            busNumber=5;
        }
        else if(locx2_id==3 || locx2_id == 4){
            busNumber=7;
        }
        //Set defaul value, if required
        if(setDefVal){
            reg_value = defVal_reg[reg_number];
        } 
        std::string baseNodeId="ltdb_"+ ltdb_id +".scaFelix"+ sca_id +".BusI2C_" + tostr(busNumber);
        std::string nodeId = baseNodeId+".register0" + tostr(baseAddress+reg_number);

        UaoClientForOpcUaSca::I2cSlave reg_locx2(session,UaNodeId(nodeId.c_str(),2));
        secureI2CWrite(reg_locx2,reg_value,"Unknown");
        //bs.setByteString(sizeof reg_value,&reg_value);
        //reg_locx2.writeValue(bs);
        //UaByteString register_value(reg_locx2.readValue());
        LOG(Log::INF) << logID << "writeRegisterLocx2 : Register_" << tostr(reg_number) + " writing value => (0x" << std::hex << (int) reg_value << ")";
        //LOG(Log::INF) << logID <<"The resetregister was succesfully updated";
    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}


UaStatus secureI2CWrite( UaoClientForOpcUaSca::I2cSlave reg , OpcUa_Byte value, std::string nodeId, int retry){
    //Changing value of register 4

    UaByteString bs;
    OpcUa_Byte oneByte,readedValue;
    oneByte = value;
    bs.setByteString(sizeof oneByte,&oneByte);
    reg.writeValue(bs);

    //Read-back for error checking
    UaByteString readBs(reg.readValue());

    readedValue=readBs.data()[0];

    if (readedValue != value){
        LOG(Log::ERR) << nodeId<<  ":The readed value (" << (int)readedValue << ") don't correspond to the writed(" << (int)value << ")";
        return OpcUa_Bad;
    }
    return OpcUa_Good;
}


//WRITE-RESET
UaStatus writeResetLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reseat_value)
{
    try{

        std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
        std::string nodeId=baseNodeId + ".gpio.LOCx2-RST";
        UaoClientForOpcUaSca::DigitalIO Locx2_1to4(session,UaNodeId(nodeId.c_str(),2));
        //LOG(Log::INF) << logID <<"Value of resert received in function " << reseat_value;
        Locx2_1to4.writeValue(reseat_value);
        //UaByteString register_value(Locx2_1to4.readValue());
        LOG(Log::INF) << logID << "writeResetLocx2 : Written value in the reset register  => (0x"<< std::hex << reseat_value << ")";

    }  
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;    
}
