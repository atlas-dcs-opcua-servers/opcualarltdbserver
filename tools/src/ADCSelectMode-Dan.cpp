#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <LogIt.h>                  //Log librairy
#include <uaplatformlayer.h>
#include <DigitalIO.h>

static const OpcUa_Byte RAW_MODE     = 2 ;
static const OpcUa_Byte PATTERN_MODE = 1;
static const OpcUa_Byte DATA_MODE    = 0;

namespace po = boost::program_options;
UaStatus selectAdcModeForAdc (int locx2Id, string nodeId_read, string nodeId_write, int numChannel,OpcUa_UInt64 mode,UaClientSdk::UaSession* session);
UaStatus selectAdcMode (string nodeId_read, string nodeId_write,int  numChannel, OpcUa_Byte numMode,UaClientSdk::UaSession* session);
UaStatus readADC (string nodeId_read, OpcUa_UInt64 &value, UaClientSdk::UaSession* session);
UaStatus writeADC (string nodeId_write, OpcUa_UInt64 value,UaClientSdk::UaSession* session);
UaStatus writeAndCheckADCWithoutInterLeave (string nodeId_read, string nodeId_write, OpcUa_UInt64 value,UaClientSdk::UaSession* session);
std::string tostr (int x);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "Select Adc mode")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("locx2Id", po::value<string>()->default_value("1"), "locx2 id")
    ("adcId", po::value<string>()->default_value("1"), "adc id")
    ("numChannel", po::value<string>()->default_value("1"), "channel number, 1 to 4")
    ("numMode", po::value<string>()->default_value("0"), "Modes : data =>0, pattern =>1 or raw =>2")
    ;

    //It's necessqry to verify if the licx2Id and adcId start at 0 or one.

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }

    LOG(Log::INF) << logID <<"--------------------";
    LOG(Log::INF) << logID <<" ADC Select mode    ";
    LOG(Log::INF) << logID <<"--------------------";

    int numChannel = stoi(vm["numChannel"].as<string>().c_str());
    OpcUa_Byte numMode = (OpcUa_Byte)strtol(vm["numMode"].as<string>().c_str(), NULL, 16);
    int locx2Id = stoi(vm["locx2Id"].as<string>().c_str());

    string baseNodeId="ltdb_"+ vm["ltdbId"].as<string>() +".scaFelix"+  vm["scaId"].as<string>();
    string nodeId_read =baseNodeId +".BusADC_LOCx2_" +  vm["locx2Id"].as<string>() +".read_adc"+ vm["adcId"].as<string>() ;
    string nodeId_write =baseNodeId +".BusADC_LOCx2_" +  vm["locx2Id"].as<string>() +".write_adc"+ vm["adcId"].as<string>() ;

    LOG(Log::INF) <<logID << "NodeId read : " <<nodeId_read;
    LOG(Log::INF) <<logID << "NodeId write : " <<nodeId_write;
    LOG(Log::INF) <<logID << "numChannel_" + vm["numChannel"].as<string>() + " : numMode_"+ vm["numMode"].as<string>();

    s = selectAdcMode(nodeId_read,nodeId_write,numChannel,numMode,session);
    
    //Luz
    //s = selectAdcModeForAdc (locx2Id,nodeId_read,nodeId_write,numChannel,numMode,session); 
    if (s.isGood()){
        LOG(Log::INF) << " Mode has been successfully updated" << endl;
    }
    else
        LOG(Log::ERR)<<" The ADC mode hasn't been updated ";
    delete session;

    return 0;
}

UaStatus selectAdcModeForAdc (int locx2Id, string nodeId_read, string nodeId_write, int numChannel,OpcUa_UInt64 mode,UaClientSdk::UaSession* session){
      int triedtimes;
      OpcUa_UInt64 mask =1;
      //int i=(std::stoi(locx2Id)-1)*16;
      int i=(locx2Id-1)*16;
      UaStatus s = OpcUa_Bad;
      //for (DLtdbADCNevis* adc : ltdbadcneviss()){
        //for(int channel = 1 ; channel <= 4 ; channel ++){
          if ((mode & (mask << i)) != 0){
            //LOG(Log::INF) << logID << "selectAdcModeForAdc: channel: " << (64 * (std::stoi(getParent()->id()) - 1)) + i + 1 << ": PATTERN_MODE" ;
            triedtimes=0;
             do{
               triedtimes++;
               //s=selectAdcMode(nodeId_read,nodeId_write,channel,DLtdbADCNevis::PATTERN_MODE,session);
               s = selectAdcMode(nodeId_read,nodeId_write,numChannel,mode,session); 
             }while(!s.isGood() && triedtimes < 5);
              if (triedtimes>1){
                LOG(Log::WRN) << logID << "Number of try :"<< triedtimes;
              }
              if (!s.isGood()){return s;}

          }
          else {
            //LOG(Log::INF) << logID << "selectAdcModeForAdc: channel: " << (64 * (std::stoi(getParent()->id()) - 1)) + i + 1 << ": DATA_MODE" ;
            triedtimes=0;
             do{
               triedtimes++;
               s=selectAdcMode(nodeId_read,nodeId_write,numChannel,DATA_MODE,session);
             }while(!s.isGood() && triedtimes < 5);
              if (triedtimes>1){
                LOG(Log::WRN) << logID << "Number of try :"<< triedtimes;
              }
              if (!s.isGood()){return s;}
          }
          i++;

      //}
      return OpcUa_Good;
}

UaStatus selectAdcMode (string nodeId_read, string nodeId_write, int numChannel, OpcUa_Byte numMode,UaClientSdk::UaSession* session)
{
    UaStatus s = OpcUa_Bad;
    OpcUa_UInt64 writeRegisterControlMDAC;
    switch(numChannel){
      case 1:
      writeRegisterControlMDAC= 0xA036000000000000;
      break;
      case 2:
      writeRegisterControlMDAC= 0xA056000000000000;
      break;
      case 3:
      writeRegisterControlMDAC= 0x6036000000000000;
      break;
      case 4:
      writeRegisterControlMDAC= 0x6056000000000000;
      break;
      default:
      LOG(Log::ERR) << logID << "wrong channel number: "<<numChannel;
      return OpcUa_Bad;
    }
    switch(numMode){
      case RAW_MODE:
        LOG(Log::INF) << "Setting up RAW_MODE" << endl;
        // ajoute 0 au registre de controle donc on ne fait rien
      break;
      case PATTERN_MODE:
        LOG(Log::INF) << "Setting up PATTERN_MODE" << endl;
        writeRegisterControlMDAC +=0x20;
      break;
      case DATA_MODE:
        LOG(Log::INF) << "Setting up DATA_MODE" << endl;
        writeRegisterControlMDAC +=0x80;
      break;
      /*case 3:
 *           writeRegisterControlMDAC+=0x40;
 *                   break;*/
      default:
      LOG(Log::ERR) << logID <<"wrong Mode: "<<numMode;
      return OpcUa_Bad;
    }
    //s=writeAndCheckADCWithoutInterLeave(nodeId_read,nodeId_write,writeRegisterControlMDAC,session);
    s=writeADC(nodeId_write,writeRegisterControlMDAC,session);
    if (!s.isGood()){return s;}
    return OpcUa_Good;
  }



UaStatus writeAndCheckADCWithoutInterLeave (string nodeId_read, string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session){
      int readError=0, writeError=0;
      int readErrorMAX=5, writeErrorMAX=5;
      LOG(Log::INF)<<"Write and Check data :" <<std::hex<< value;
      OpcUa_UInt64 addressMask = 0xffff000000000000;
      OpcUa_UInt64 dataMask    = 0x0000ffffffffffff;

      OpcUa_UInt64 readValue;
      OpcUa_UInt64 readingReg;
      UaStatus s = OpcUa_Bad;

      readingReg = (value & addressMask) - 0x0010000000000000;
      while(writeError<writeErrorMAX){
        s =writeADC(nodeId_write,value,session);
        if (!s.isGood()){return s;}
        readError=0;
        while(readError<readErrorMAX){
          //Reading reg 1
          s =writeADC(nodeId_write,readingReg,session);
          if (!s.isGood()){return s;}
          s =readADC(nodeId_read,readValue,session);
          LOG(Log::INF)<<"Data read:" <<std::hex<< readValue;
          if (!s.isGood()){return s;}

          if (readValue==(value & dataMask)){
            LOG(Log::WRN) << logID <<"Reading data corresponds to written value"<<endl;
            return OpcUa_Good;
          }
          readError++;
          LOG(Log::WRN) << logID <<"Read Loop "<< readError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
        }
        writeError++;
        LOG(Log::WRN) << logID <<"Write Loop "<< writeError <<"Read data("<<std::hex<< readValue <<") don't correspond to written value:"<<value ;
      }
      LOG(Log::ERR) << logID <<"Data don't correspond to written value:" ;
      return OpcUa_Bad;
    }
 
UaStatus readADC(string nodeId_read, OpcUa_UInt64 &value, UaClientSdk::UaSession* session)
  {
    UaByteString bs;
    value=0;
    try{
      UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_read.c_str(),2));
      UaByteString readBs(registerId.readValue());
      if (readBs.length()!=8){
        LOG(Log::ERR) << logID <<"Error reading register"+ nodeId_read;
        return OpcUa_Bad;
      }
      for (int i=0;i<8;i++){
        value = value <<8;
        value+=(int) readBs.data()[i];
      }
      LOG(Log::INF) << logID <<"Register "+ nodeId_read;
      //LOG(Log::INF) << logID <<"Value" << "  => (0x"<< std::hex << (int)readBs.data()[0] << ")";
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
      LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
      return e.statusCode();
    }
    catch(const std::exception& e){
      LOG(Log::ERR) << logID <<"Caught:"<<e.what();
      return OpcUa_Bad;
    }
    return OpcUa_Good;
  }

  UaStatus writeADC (string nodeId_write,OpcUa_UInt64 value,UaClientSdk::UaSession* session)
  {   
    UaByteString bs;
    OpcUa_Byte tOneByte[8];
    try{
        for (int i=0;i<8;i++){
            tOneByte[7-i]=value & 255;
            LOG(Log::INF)<<"tOneByte[7-i] ="<<std::hex << (int)tOneByte[7-i];
            value=value >> 8;
        }
        LOG(Log::INF)<< "writing in node: "<< nodeId_write;
        UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId_write.c_str(),2));
        bs.setByteString(sizeof tOneByte,tOneByte);
        //LOG(Log::INF) << logID <<"Value to write:" << "  => (0x"<< std::hex << (int)bs.data()[0] << ")";
        registerId.writeValue(bs);

    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
        LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
        return e.statusCode();
    }   
    catch(const std::exception& e){
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    } 
      return OpcUa_Good;
  }

std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}
