#include <iostream>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus writeResetLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reseat_value);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "Modifify the value of reset reguster")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("resetValue",po::value<string>()->default_value("false"),"reset value")

    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
  return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    LOG(Log::INF) << logID <<"---------------------------";
    LOG(Log::INF) << logID <<"LOCX2 Write Reset Register";
    LOG(Log::INF) << logID <<"---------------------------";
  
    LOG(Log::INF) << logID <<"Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>();
    bool rv = (vm["resetValue"].as<string>()=="true");
    //LOG(Log::INF) << logID <<"Value of resert received from vm " << rv;

    s = writeResetLocx2(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),rv);
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

    delete session;

    return 0;
}

UaStatus writeResetLocx2(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reseat_value)
{
  try{

    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
    std::string nodeId=baseNodeId + ".gpio.LOCx2-RST";
    UaoClientForOpcUaSca::DigitalIO Locx2_1to4(session,UaNodeId(nodeId.c_str(),2));
    //LOG(Log::INF) << logID <<"Value of resert received in function " << reseat_value;
    Locx2_1to4.writeValue(reseat_value);
    //UaByteString register_value(Locx2_1to4.readValue());
    LOG(Log::INF) << logID << "writeResetLocx2 : Written value in the reset register  => (0x"<< std::hex << reseat_value << ")";

  }  
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  return OpcUa_Good;    
}
