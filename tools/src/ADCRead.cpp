#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <LogIt.h>                  //Log librairy
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus readADC(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,string locx2Id, string adcId);
std::string tostr (int x);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "ADC status of registers")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("locx2Id", po::value<string>()->default_value("1"), "locx2 id")
    ("adcId", po::value<string>()->default_value("1"), "adc id")
    ;

    //It's necessqry to verify if the locx2Id and adcId start at 0 or one.

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }

    LOG(Log::INF) << logID <<"-------------------------";
    LOG(Log::INF) << logID <<" ADC  Status Registers";
    LOG(Log::INF) << logID <<"-------------------------";
    LOG(Log::INF) <<logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_"+ vm["scaId"].as<string>();

    s = readADC(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(), vm["locx2Id"].as<string>(), vm["adcId"].as<string>());
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

    delete session;

    return 0;
}


UaStatus readADC(UaClientSdk::UaSession* session, string ltdb_id, string sca_id, string locx2Id, string adcId)
  { 
    UaByteString bs;
    OpcUa_UInt64 value=0;
    try{
      std::string baseNodeId="ltdb_"+ ltdb_id +".scaFelix"+ sca_id;
      std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2Id +".read_adc"+ adcId ;
      UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
      UaByteString readBs(registerId.readValue());
      LOG(Log::INF)<<"READ ADC"<< endl;
      if (readBs.length()!=8){ 
        LOG(Log::ERR) << logID <<"Error reading register"+ sca_id +".BusADC_LOCx2_" + locx2Id +".read_adc" + adcId;
        return OpcUa_Bad;
      }   
      for (int i=0;i<8;i++){
        LOG(Log::INF)<<"data n"<<i <<"="<<(int) readBs.data()[i];
        value = value <<8;
        value+=(int) readBs.data()[i];
        LOG(Log::INF)<<"value ="<<std::hex<< value;
      }
     // LOG(Log::ERR) << logID <<"Register "+sca_id +".BusADC_LOCx2_" + locx2Id +".read_adc" + adcId;
     // LOG(Log::ERR) << logID <<"Value" << "  => (0x"<< std::hex << (int)readBs.data()[0] << ")";
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
      LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
      return e.statusCode();
    }   
    catch(const std::exception& e){
      LOG(Log::ERR) << logID <<"Caught:"<<e.what();
      return OpcUa_Bad;
    }
    return OpcUa_Good;
  }

std::string tostr (int x)
{
  std::stringstream str;
  str <<std::hex<< x;
  return str.str();
}
