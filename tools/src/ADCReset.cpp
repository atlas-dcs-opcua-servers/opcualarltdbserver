#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;

UaStatus resetADC(UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id);
UaStatus fakeRead (UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id);
UaStatus switchTo10Bits (UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id);
UaStatus readADC(OpcUa_UInt64 & value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id);
UaStatus writeADC (OpcUa_UInt64 value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id);



int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "ADC Reset")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("locx2Id", po::value<string>()->default_value("1"), "LOCx2 id")
    ("adcId", po::value<string>()->default_value("1"), "ADC id")

    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        cout << desc << "\n";
  return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    logID="Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>() + ": Locx2_ " + vm["locx2Id"].as<string>() + ": ADC_ " + vm["adcId"].as<string>();
    std::string baseNodeId="ltdb_"+vm["ltdbId"].as<string>()+".scaFelix"+vm["scaId"].as<string>();

    //std::string nodeId=vm["locx2Id"].as<string>()+"A"+vm["adcId"].as<string>();

    s = resetADC(session,baseNodeId,vm["locx2Id"].as<string>(),vm["adcId"].as<string>());
    if (s.isGood()){
        LOG(Log::INF) << "Reset successfully done" << endl;              
    }


    delete session;

    return 0;
}
UaStatus resetADC(UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id)
{
  try{

    OpcUa_Boolean true_value=1;
    OpcUa_Boolean false_value =0;
    UaStatus s = OpcUa_Bad;
    //std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
    std::string nodeId=baseNodeId + ".gpio.RSTB_ADCL"+locx2_id+"A"+adc_id;
    UaoClientForOpcUaSca::DigitalIO adc_d(session,UaNodeId(nodeId.c_str(),2));

    adc_d.writeValue(true_value);
    adc_d.writeValue(false_value);
    adc_d.writeValue(true_value);
    s = switchTo10Bits(session,baseNodeId,locx2_id,adc_id);
    if (!s.isGood()){return s;}
    s = fakeRead(session,baseNodeId,locx2_id,adc_id);
    if (!s.isGood()){return s;}

  }  
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    if (e.statusCode()==OpcUa_BadResourceUnavailable){
        LOG(Log::TRC) << logID <<"Caught(BadStatusCode):"<<e.what();
    }
    else{
      LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    }
      return e.statusCode();
    }
    catch(const std::exception& e){
      LOG(Log::ERR) << logID <<"Caught:"<<e.what();
      return OpcUa_Bad;
    }
    return OpcUa_Good;
}

UaStatus fakeRead (UaClientSdk::UaSession* session,string baseNodeId, string locx2_id, string adc_id){
  OpcUa_UInt64 writeValue =0xA024000000000000;
  OpcUa_UInt64 readValue;
  UaStatus s =writeADC(writeValue,session,baseNodeId,locx2_id,adc_id);
  if (!s.isGood()){
    return s;
  }
  s =readADC(readValue,session,baseNodeId,locx2_id,adc_id);
  if (!s.isGood()){
    return s;
  }
  LOG(Log::INF) << "fakeRead OK"<<endl;
  return OpcUa_Good;
}
UaStatus switchTo10Bits (UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id){
  UaByteString bs;
  OpcUa_Byte oneByte;
  oneByte=0x20;
  try{
      //std::string baseNodeId="ltdb_"+ltdb_id +".scaFelix"+ sca_id;
      std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2_id +".ctrl_adc" + adc_id;
      UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
      bs.setByteString(sizeof oneByte,&oneByte);
      registerId.writeValue(bs);
  }
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  }
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  LOG(Log::INF) << "switchTo10Bits OK"<<endl;
  return OpcUa_Good;
}
UaStatus readADC(OpcUa_UInt64 & value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id){
  UaByteString bs;
  value=0;
  try{
      //std::string baseNodeId="ltdb_"+ltdb_id +".scaFelix"+ sca_id;
      std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2_id +".read_adc" + adc_id;
      UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
      UaByteString readBs(registerId.readValue());
      if (readBs.length()!=8){
        LOG(Log::ERR) << logID <<"Error reading register"+ nodeId;
        return OpcUa_Bad;
      }
      for (int i=0;i<8;i++){
        value = value <<8;
        //LOG(Log::INF)<<"Data : ("<< i << "=" << (int) readBs.data()[i] << ")" << endl;
        value+=(int) readBs.data()[i];
        //LOG(Log::INF)<<"Value : ("<<std::hex<< value<< ")" << endl;
      } 
  }
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  }
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  LOG(Log::INF) << "ReadADC OK"<<endl;
  return OpcUa_Good;
}

UaStatus writeADC (OpcUa_UInt64 value,UaClientSdk::UaSession* session, string baseNodeId, string locx2_id, string adc_id){
  UaByteString bs;
  OpcUa_Byte tOneByte[8];
  try{
    for (int i=0;i<8;i++){
      tOneByte[7-i]=value & 255;
      value=value >> 8;
    }
    //std::string baseNodeId="ltdb_"+ltdb_id +".scaFelix"+ sca_id;
    std::string nodeId =baseNodeId +".BusADC_LOCx2_" + locx2_id +".write_adc"+ adc_id;
    UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
    bs.setByteString(sizeof tOneByte,tOneByte);
    registerId.writeValue(bs);
  }
  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e){
    LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  }
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  LOG(Log::INF) << "writeADC OK"<<endl;
  return OpcUa_Good;
}


