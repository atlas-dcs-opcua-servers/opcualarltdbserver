#include <iostream>
//#include <DLtdbMtrx.h>
//#include <ASLtdbMtrx.h>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>               
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>


namespace po = boost::program_options;
//using namespace boost;
UaStatus readMtrx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options"); 
    desc.add_options()
    ("help", "Status of MTRX registers")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    

    if (vm.count("help")) {
        cout << desc << "\n";
	return 1;
    }

    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    s = readMtrx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>());
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

    delete session;

    return 0;
}

UaStatus readMtrx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id)
{ 
  try{
      UaStatus s=OpcUa_Bad;
      OpcUa_Byte ltdb_register4_value;
    
      string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
      std::string logID="";

   	  LOG(Log::INF) << logID <<"----------------------------------";
   	  LOG(Log::INF) << logID <<" MTRX status registers";
   	  LOG(Log::INF) << logID <<"----------------------------------";
      logID="Ltdb_" + ltdb_id + " : Sca_ "+ sca_id;
	    //UaByteString register4_value, register5_value;
      LOG(Log::INF) << logID << baseNodeId ;
      OpcUa_Byte readedValue, writeValue;
	    //Creation of the I2C nodes
	    //LOG(Log::INF) << logID <<"Status register4";

      string nodeId=baseNodeId + ".MTRx.register04";
	    UaoClientForOpcUaSca::I2cSlave register04(session,UaNodeId(nodeId.c_str(),2));

      //LOG(Log::INF) << logID <<"Status register5";
	    nodeId=baseNodeId + ".MTRx.register05";
      UaoClientForOpcUaSca::I2cSlave register05(session,UaNodeId(nodeId.c_str(),2));

	    UaByteString register4_value(register04.readValue());
      UaByteString register5_value(register05.readValue());


	    LOG(Log::INF) << logID << "mtrx_read : Register4 value => (0x"<< std::hex << (int)*register4_value.data() << ")";
	    LOG(Log::INF) << logID << "mtrx_read : Register5 value => (0x"<< std::hex << (int)*register5_value.data() << ")";

    }
        catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
    {
    	LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
	    return e.statusCode();
    }
    catch(const std::exception& e){
	    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
	    return OpcUa_Bad;
    }
    return OpcUa_Good;
}
