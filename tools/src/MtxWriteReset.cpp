#include <iostream>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <common_tools.h>
#include <LogIt.h>                  //Log librairy
#include <uaplatformlayer.h>
#include <DigitalIO.h>


namespace po = boost::program_options;
UaStatus writeResetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reset_value);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "MTX write reset register")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("resetValue",po::value<string>()->default_value("false"),"reset value")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    LOG(Log::INF) << logID <<"-------------------------";
    LOG(Log::INF) << logID <<" MTX write Reset register ";
    LOG(Log::INF) << logID <<"-------------------------";

    LOG(Log::INF) << logID << "Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_" + vm["scaId"].as<string>();
    bool rv = (vm["resetValue"].as<string>()=="true");
    LOG(Log::INF) << logID << "restValue_" << rv ;
    s = writeResetMtx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),rv);
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";
    delete session;

    return 0;
}

UaStatus writeResetMtx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,OpcUa_Boolean reset_value)
{
  try{
    std::string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
    std::string nodeId=baseNodeId + ".gpio.MTX-RST";
    UaoClientForOpcUaSca::DigitalIO mtrxio(session,UaNodeId(nodeId.c_str(),2));
    mtrxio.writeValue(reset_value);
    if (reset_value)
        LOG(Log::INF) << logID <<"writeResetMtx : Reset is true";
    else 
        LOG(Log::INF) << logID <<"writeResetMtx : Reset is false";
  }  
  catch(const std::exception& e){
    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  return OpcUa_Good;    
}
