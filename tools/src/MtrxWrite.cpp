#include <iostream>
//#include <DLtdbMtrx.h>
//#include <ASLtdbMtrx.h>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <I2cSlave.h>               
#include <ClientSessionFactory.h>   //Creation of connection
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <LogIt.h>                  //Log librairy
#include <common_tools.h>
#include <uaplatformlayer.h>


namespace po = boost::program_options;
//using namespace boost;
UaStatus writeMtrx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,  OpcUa_Byte value4, OpcUa_Byte value5);

int main(int argc, const char *argv[])
{
    UaPlatformLayer::init();
    po::options_description desc("Allowed options"); 
    desc.add_options()
    ("help", "Allows the modification of the 4th and 5th MTrx registers")
    ("address", po::value<string>()->default_value("opc.tcp://localhost:48020"),"Address connection")
    ("ltdbId", po::value<string>()->default_value("1"), "ltdb id")
    ("scaId", po::value<string>()->default_value("1"), "sca id")
    ("register4Value", po::value<string>()->default_value("0x5"), "Value to set to register4")
    ("register5Value", po::value<string>()->default_value("0x19"), "Value to set to register5")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")) {
        cout << desc << "\n";
	return 1;
    }

    UaStatus s = OpcUa_Bad;
    Log::initializeLogging(Log::INF);
    UaClientSdk::UaSession *session=ClientSessionFactory::connect(vm["address"].as<string>().c_str());
    if (!session){
      LOG(Log::ERR)<<"Bad communication Error";
      return 0;
    }
    logID="Ltdb_" + vm["ltdbId"].as<string>() + " : Sca_ "+ vm["scaId"].as<string>() + ": ";
    OpcUa_Byte value4 = (OpcUa_Byte)strtol(vm["register4Value"].as<string>().c_str(), NULL, 16);
    OpcUa_Byte value5 = (OpcUa_Byte)strtol(vm["register5Value"].as<string>().c_str(), NULL, 16);

    LOG(Log::INF) << logID <<"Reg5 : Writing " <<vm["register5Value"].as<string>();

    s = writeMtrx(session, vm["ltdbId"].as<string>(), vm["scaId"].as<string>(),value4,value5);
    if (s.isGood()){
        LOG(Log::INF) << " Operation successfully done " << endl;
    }
    else
        LOG(Log::ERR)<<" ERROR : Operation was not successfully done";

    delete session;

    return 0;
}

UaStatus writeMtrx(UaClientSdk::UaSession* session, string ltdb_id, string sca_id,  OpcUa_Byte value4, OpcUa_Byte value5)
{ 
  try{
      UaStatus s=OpcUa_Bad;
      string baseNodeId="ltdb_"+ltdb_id+".scaFelix"+sca_id;
      std::string logID="";

   	  LOG(Log::INF) << logID <<"---------------------------------";
   	  LOG(Log::INF) << logID <<" Set MTRX registers";
   	  LOG(Log::INF) << logID <<"---------------------------------";
      LOG(Log::INF) << logID << baseNodeId ;
      OpcUa_Byte  writeValue4,writeValue5;
	    //Creation of the I2C nodes
	    //LOG(Log::INF) << logID <<"Status register4";

      string nodeId=baseNodeId + ".MTRx.register04";
	    UaoClientForOpcUaSca::I2cSlave register04(session,UaNodeId(nodeId.c_str(),2));

      //LOG(Log::INF) << logID <<"Status register5";
	    nodeId=baseNodeId + ".MTRx.register05";
      UaoClientForOpcUaSca::I2cSlave register05(session,UaNodeId(nodeId.c_str(),2));

      UaByteString bs;
      writeValue4 =  value4;
      bs.setByteString(sizeof writeValue4, &writeValue4);      
      register04.writeValue(bs);

      UaByteString bs2;
      writeValue5 =  value5;

      bs2.setByteString(sizeof writeValue5, &writeValue5);      
      register05.writeValue(bs2);

	    UaByteString register4_value(register04.readValue());
      UaByteString register5_value(register05.readValue());

	    LOG(Log::INF) << logID << "writeMtrx : Register4 value => (0x"<< std::hex << (int)*register4_value.data() << ")";
	    LOG(Log::INF) << logID << "writeMtrx : Register5 value => (0x"<< std::hex << (int)*register5_value.data() << ")";

    }
        catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
    {
    	LOG(Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
	    return e.statusCode();
    }
    catch(const std::exception& e){
	    LOG(Log::ERR) << logID <<"Caught:"<<e.what();
	    return OpcUa_Bad;
    }
    return OpcUa_Good;
}
