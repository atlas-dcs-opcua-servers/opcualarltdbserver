#rm -Rf build
mkdir build
mkdir build/old_tools
cd build
git clone https://github.com/quasar-team/open62541-compat.git -b pmaster 
cd open62541-compat
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DSTANDALONE_BUILD=ON -DOPEN62541-COMPAT_BUILD_CONFIG_FILE=boost_standard_install_cc7.cmake -DSKIP_TESTS=ON ../
make
echo "open62541-compat finished. Not sure if it was successful ;-) "
cd ../../
cmake -DCMAKE_BUILD_TYPE=Debug ../ 
make

mv mtx_writeReset gbtx-ping mtx_readReset mtx_read mtrx_reset locx2_setDelay locx2_writeReset mtx_reset mtrx_read mtrx_write adc_reset adc_selectMode adc_selectMode_Dan adc_read gbtx_reset locx2_readReset locx2_readRegisters adc_writeAndCheck locx2_writeRegister locx2_reset old_tools
