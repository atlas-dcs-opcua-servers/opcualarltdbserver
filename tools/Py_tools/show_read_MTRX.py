import argparse
from prettytable import PrettyTable
from colorama import Fore, Style, init
from LTDB_tools import read_mtrx

# Initialise colorama
init(autoreset=True)

def read_and_show_mtrx(ip, ltdb_id, sca_id):
    results = read_mtrx(ip, ltdb_id, sca_id)
    table = PrettyTable()
    table.field_names = [Fore.BLUE + "Register Address" + Style.RESET_ALL, Fore.BLUE + "Result" + Style.RESET_ALL]
    
    for address, value in results:
        table.add_row([address, value])
    
    print(f"{Fore.BLUE}MTRX_{sca_id} Results:{Style.RESET_ALL}\n{table}")

def main():
    parser = argparse.ArgumentParser(description='Read I2C Matrix Data via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='SCA ID', required=True)
    
    args = parser.parse_args()
    read_and_show_mtrx(args.ip, args.ltdb_id, args.sca_id)

if __name__ == "__main__":
    main()
