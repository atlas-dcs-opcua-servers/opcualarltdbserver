import sys
import argparse
from prettytable import PrettyTable
from colorama import init, Fore
sys.path.append('/localdisk/test_sofian')
from LTDB_tools import read_mtx

def read_mtx_data(ip, ltdb_id, sca_id):
    init(autoreset=True)
    results = read_mtx(ip, ltdb_id, sca_id)

    for bus_n, bus_results in results.items():
        table = PrettyTable([Fore.BLUE + "MTX Number" + Fore.RESET, 
                             Fore.BLUE + "Register Address" + Fore.RESET, 
                             Fore.BLUE + "Result" + Fore.RESET])
        prev_mtx_number = None  

        for register_address, read_value, mtx_number in bus_results:
            if prev_mtx_number is not None and mtx_number != prev_mtx_number:
                table.add_row(['-' * 12, '-' * 17, '-' * 8])  

            formatted_mtx_number = f"{mtx_number}"
            formatted_register_address = f"{register_address}"
            result_value = read_value

            table.add_row([formatted_mtx_number, formatted_register_address, result_value])
            prev_mtx_number = mtx_number  

        print(f"Results for BusI2C_{bus_n}:\n{table}\n")

def main():
    parser = argparse.ArgumentParser(description='Read I2C MTX Data via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='SCA ID', required=True)
    args = parser.parse_args()

    read_mtx_data(args.ip, args.ltdb_id, args.sca_id)

if __name__ == "__main__":
    main()

