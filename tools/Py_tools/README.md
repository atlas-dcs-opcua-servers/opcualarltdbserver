
+-------------------------+--------+


# Lar LTDB Control Software Tools

This document explains how to use the tools allowing LTDB experts to check the read, write, and reset options of LTDB card components. All of these tools are available in a git repository at the following address: [Link to GitLab repository](https://gitlab.cern.ch/atlas-dcs-opcua-servers/opcualarltdbserver/-/tree/soso/tools/Py_tools).

## Deployment on P1

It is important to note that these tools still need to be deployed on P1, and for that, IP address modifications will be necessary at the time of deployment. Currently, the project runs at EMF on this IP address: `opc.tcp://pc-emf-felix-fe-01:48020`.

## List of Available Components

The tools allow checking the following components:

╔════════════════════════════════════════════════════════╗
║═══════════ Lar LTDB Control Software Tools ════════════║
╚════════════════════════════════════════════════════════╝

         Main Menu:   
                    
Check SCA Status *
MTX Components *
MTRX Components *
GBTX Components *
LOCX2 Components *
ADC Components *
Exit *


## Usage

To use the tools, you need to connect to the machine `pcatllaralmaemf`, then navigate to the directory `/localdisk/test_sofian`. This directory is a working directory that includes all the tools as illustrated below:

Example of launching `menu.py`:

[smahuan@pcatllaralmaemf test_sofian]$ python menu.py
╔════════════════════════════════════════════════════════╗
║═══════════ Lar LTDB Control Software Tools ════════════║
╚══════
****************************************
*              Main Menu:              *
****************************************
* 1. Check SCA Status                 *
* 2. MTX Components                   *
* 3. MTRX Components                  *
* 4. GBTX Components                  *
* 5. LOCX2 Components                 *
* 6. ADC Components                   *
* 7. Exit                             *

We will check the status of SCAs, here are the results:
[smahuan@pcatllaralmaemf test_sofian]$ python menu.py 
╔════════════════════════════════════════════════════════╗
║═══════════ Lar LTDB Control Software Tools ════════════║
╚════════════════════════════════════════════════════════╝
****************************************
*              Main Menu:              *
****************************************
* 1. Check SCA Status                 *
* 2. MTX Components                   *
* 3. MTRX Components                  *
* 4. GBTX Components                  *
* 5. LOCX2 Components                 *
* 6. ADC Components                   *
* 7. Exit                             *
****************************************
Enter your choice: 1
Enter an LTDB number: 1
Enter an SCA number in the range 1-5, or press Enter to check all SCAs: 
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
+-------------------------+--------+
|          Device         | Status |
+-------------------------+--------+
| ns=2;s=ltdb_1.scaFelix1 | online |
| ns=2;s=ltdb_1.scaFelix2 | online |
| ns=2;s=ltdb_1.scaFelix3 | online |
| ns=2;s=ltdb_1.scaFelix4 | online |
| ns=2;s=ltdb_1.scaFelix5 | online |
+-------------------------+--------+

To run the script outside the menu and see the available options, you can use the following command:

[smahuan@pcatllaralmaemf test_sofian]$ python show_online_SCA.py 
usage: show_online_SCA.py [-h] --ltdb_id {1,2,3,4,5} [--sca_id [{1,2,3,4,5}]] [--table]
show_online_SCA.py: error: the following arguments are required: --ltdb_id

 [smahuan@pcatllaralmaemf test_sofian]$ python show_online_SCA.py 
usage: show_online_SCA.py [-h] --ltdb_id {1,2,3,4,5} [--sca_id [{1,2,3,4,5}]] [--table]
show_online_SCA.py: error: the following arguments are required: --ltdb_id
[smahuan@pcatllaralmaemf test_sofian]$ python show_online_SCA.py --h
usage: show_online_SCA.py [-h] --ltdb_id {1,2,3,4,5} [--sca_id [{1,2,3,4,5}]] [--table]

Check the status of specified SCA instances.

optional arguments:
  -h, --help            show this help message and exit
  --ltdb_id {1,2,3,4,5}
                        Enter an LTDB number in the range 1-5.
  --sca_id [{1,2,3,4,5}]
                        Enter an SCA number in the range 1-5. If not specified, all SCAs are checked.
  --table               Display the table. By default, only the line is displayed.
[smahuan@pcatllaralmaemf test_sofian]$ python show_online_SCA.py --table  -- ltdb_id 1 
usage: show_online_SCA.py [-h] --ltdb_id {1,2,3,4,5} [--sca_id [{1,2,3,4,5}]] [--table]
show_online_SCA.py: error: the following arguments are required: --ltdb_id
[smahuan@pcatllaralmaemf test_sofian]$ python show_online_SCA.py  --ltdb_id 1 --table  
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
Requested session timeout to be 3600000ms, got 30000ms instead
+-------------------------+--------+
|          Device         | Status |
+-------------------------+--------+
| ns=2;s=ltdb_1.scaFelix1 | online |
| ns=2;s=ltdb_1.scaFelix2 | online |
| ns=2;s=ltdb_1.scaFelix3 | online |
| ns=2;s=ltdb_1.scaFelix4 | online |
| ns=2;s=ltdb_1.scaFelix5 | online |
+-------------------------+--------+





