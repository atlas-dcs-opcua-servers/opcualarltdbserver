from opcua import Client
from opcua import ua

class I2cSlave:
    def __init__(self, ip, identifierString):
        print(identifierString)
        self.identifierString = identifierString
        self.client = Client(ip)
        self.client.connect()
        self.client.load_type_definitions()

    def disconnect(self):
        self.client.disconnect()

    def readValue(self):
        node = self.client.get_node(self.identifierString + ".value")
        try:
            ret = node.get_data_value()
            return ret.StatusCode, ret.Value.Value, ret.SourceTimestamp
        except Exception as e:
            print("Error occurred while setting the value:", e)
            return ua.StatusCode(ua.StatusCodes.Bad), None, None

    def writeValue(self, value):
        node = self.client.get_node(self.identifierString + ".value")
        try:
            node.set_value(value)
        except Exception as e:
            print("Error occurred while setting the value:", e)
            return ua.StatusCode(ua.StatusCodes.Bad)

    def readSlave(self, numberOfBytes):
        base_node = self.client.get_node(self.identifierString)
        method_node = self.client.get_node(self.identifierString + ".readSlave")
        try:
            out_reply = base_node.call_method(method_node, ua.Variant(numberOfBytes, ua.VariantType.Byte))
            return ua.StatusCode(ua.StatusCodes.Good), out_reply
        except Exception as e:
            print("Error occurred while setting the value:", e)
            return ua.StatusCode(ua.StatusCodes.Bad), None

    def writeSlave(self, payload):
        base_node = self.client.get_node(self.identifierString)
        method_node = self.client.get_node(self.identifierString + ".writeSlave")
        try:
            base_node.call_method(method_node, ua.Variant(payload, ua.VariantType.UaByteString))
            return ua.StatusCode(ua.StatusCodes.Good)
        except Exception as e:
            print("Error occurred while setting the value:", e)
            return ua.StatusCode(ua.StatusCodes.Bad)
