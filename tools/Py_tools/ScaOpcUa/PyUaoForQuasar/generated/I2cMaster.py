from opcua import Client
from opcua import ua

class I2cMaster:
  def __init__(self,ip,identifierString):
    self.identifierString = identifierString
    self.client = Client(ip)
    self.client.connect()
    self.client.load_type_definitions()

  def disconnect(self):
    self.client.disconnect()

  def readDiagnostics(self):
    node = self.client.get_node(self.identifierString + ".diagnostics")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None
