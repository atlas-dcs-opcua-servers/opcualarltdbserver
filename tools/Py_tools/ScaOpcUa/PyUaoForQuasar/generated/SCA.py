from opcua import Client
from opcua import ua

class SCA:
  def __init__(self,ip,identifierString):
    self.identifierString = identifierString
    self.client = Client(ip)
    self.client.connect()
    self.client.load_type_definitions()

  def disconnect(self):
    self.client.disconnect()

  def readAddress(self):
    node = self.client.get_node(self.identifierString + ".address")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readOnline(self):
    node = self.client.get_node(self.identifierString + ".online")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readId(self):
    node = self.client.get_node(self.identifierString + ".id")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readNumberRequests(self):
    node = self.client.get_node(self.identifierString + ".numberRequests")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readNumberReplies(self):
    node = self.client.get_node(self.identifierString + ".numberReplies")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readLastReplySecondsAgo(self):
    node = self.client.get_node(self.identifierString + ".lastReplySecondsAgo")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readRequestRate(self):
    node = self.client.get_node(self.identifierString + ".requestRate")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readNumberLostReplies(self):
    node = self.client.get_node(self.identifierString + ".numberLostReplies")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readLostRepliesRate(self):
    node = self.client.get_node(self.identifierString + ".lostRepliesRate")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None


  def readPingRttUs(self):
    node = self.client.get_node(self.identifierString + ".pingRttUs")
    try:
      ret  = node.get_data_value()
      return ret.StatusCode,ret.Value.Value,ret.SourceTimestamp
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None,None



  def reset(self):
    base_node   = self.client.get_node(self.identifierString)
    method_node = self.client.get_node(self.identifierString + ".reset")
    try:
        base_node.call_method(method_node)
        return ua.StatusCode(ua.StatusCodes.Good)
    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad)


  def ping(self):
    base_node   = self.client.get_node(self.identifierString)
    method_node = self.client.get_node(self.identifierString + ".ping")
    try:
        out_pong   =        base_node.call_method(method_node)
        return ua.StatusCode(ua.StatusCodes.Good),out_pong

    except Exception as e:
        # Handle any exceptions that occur during the value setting process
        print("Error occurred while setting the value:", e)
        return ua.StatusCode(ua.StatusCodes.Bad),None

