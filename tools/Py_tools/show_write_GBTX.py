import argparse
from colorama import init, Fore, Style
import sys
from prettytable import PrettyTable
sys.path.append('/localdisk/test_sofian')
from LTDB_tools import write_gbtx 

def write_gbtx_values(ip, ltdb_id, sca_id, values):
    init(autoreset=True)
    table = PrettyTable(["Value", "Type"])
    
    converted_values = []
    for value in values:
        # Déterminer le type de la valeur (hexadécimal ou décimal)
        if value.startswith('0x') or value.startswith('0X'):
            converted_value = int(value, 16) 
            table.add_row([value, "Hexadecimal"])
        else:
            converted_value = int(value)  
            table.add_row([value, "Decimal"])
        
        converted_values.append(converted_value)

  
    print(table)
    
    # Appeler la fonction write_gbtx avec les valeurs converties
    try:
        write_gbtx(ip, ltdb_id, sca_id, *converted_values)
        print(f"{Fore.GREEN}Data written successfully to GBTX.{Style.RESET_ALL}")
    except Exception as e:
        print(f"{Fore.RED}Failed to write data to GBTX: {e}{Style.RESET_ALL}")

def main():
    parser = argparse.ArgumentParser(description='Write data to I2C GBTX via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='SCA ID', required=True)
    parser.add_argument('--value0', help='Value 0 (hexadecimal or decimal)', required=True)
    parser.add_argument('--value1', help='Value 1 (hexadecimal or decimal)', required=True)
    parser.add_argument('--value2', help='Value 2 (hexadecimal or decimal)', required=True)
    parser.add_argument('--value3', help='Value 3 (hexadecimal or decimal)', required=True)
    parser.add_argument('--value4', help='Value 4 (hexadecimal or decimal)', required=True)

    args = parser.parse_args()

    values = [args.value0, args.value1, args.value2, args.value3, args.value4]
    write_gbtx_values(args.ip, args.ltdb_id, args.sca_id, values)

if __name__ == "__main__":
    main()
