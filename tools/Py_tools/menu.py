import sys
from colorama import Fore, Style, init

init(autoreset=True)

sys.path.append('/localdisk/test_sofian')
sys.path.append('/localdisk/test_sofian/ScaOpcUa/PyUaoForQuasar/generated')

from LTDB_tools import check_online_status
from show_online_SCA import check_sca_status
from show_read_MTX import read_mtx_data
from show_write_MTX import write_mtx_data
from reset_MTX import reset_mtx_data
from show_read_MTRX import read_and_show_mtrx
from show_write_MTRX import write_mtrx_values
from reset_MTRX import reset_mtrx_data
from show_read_GBTX import read_gbtx
from show_write_GBTX import write_gbtx_values
from reset_GBTX import reset_gbtx_data
from show_read_LOCx2 import perform_read_locx2
from show_write_LOCX2 import perform_write_locx2
from reset_LOCX2 import perform_reset_locx2
from show_read_ADC import perform_read_adc
from show_write_ADC import perform_write_adc
from reset_ADC import perform_reset_adc

def display_header():
    border_top = "╔" + "═"*56 + "╗"  
    border_bottom = "╚" + "═"*56 + "╝"  
    title = " Lar LTDB Control Software Tools "
    print(border_top)
    print("║" + title.center(56, '═') + "║")  
    print(border_bottom)

def display_menu():
    display_header()
    border = "*" * 40  
    print(border)
    print("*" + "Main Menu:".center(38) + "*")
    print(border)
    print("* 1. Check SCA Status".ljust(38) + "*")
    print("* 2. MTX Components".ljust(38) + "*")
    print("* 3. MTRX Components".ljust(38) + "*")
    print("* 4. GBTX Components".ljust(38) + "*")
    print("* 5. LOCX2 Components".ljust(38) + "*")
    print("* 6. ADC Components".ljust(38) + "*")
    print("* 7. Exit".ljust(38) + "*")
    print(border)

def display_mtx_menu():
    border = "=" * 48
    print(border)
    print("|" + " MTX Components Menu ".center(46) + "|")
    print(border)
    print("| 1. Read MTX".ljust(46) + "|")
    print("| 2. Write MTX".ljust(46) + "|")
    print("| 3. Reset MTX".ljust(46) + "|")
    print("| 4. Return to Main Menu".ljust(46) + "|")
    print(border + "\n")

def display_mtrx_menu():
    border = "=" * 48
    print(border)
    print("|" + " MTRX Components Menu ".center(46) + "|")
    print(border)
    print("| 1. Read MTRX".ljust(46) + "|")
    print("| 2. Write MTRX".ljust(46) + "|")
    print("| 3. Reset MTRX".ljust(46) + "|")
    print("| 4. Return to Main Menu".ljust(46) + "|")
    print(border + "\n")

def display_gbtx_menu():
    border = "=" * 48
    print(border)
    print("|" + " GBTX Components Menu ".center(46) + "|")
    print(border)
    print("| 1. Read GBTX".ljust(46) + "|")
    print("| 2. Write GBTX".ljust(46) + "|")
    print("| 3. Reset GBTX".ljust(46) + "|")
    print("| 4. Return to Main Menu".ljust(46) + "|")
    print(border + "\n")

def display_locx2_menu():
    border = "=" * 48
    print(border)
    print("|" + " LOCX2 Components Menu ".center(46) + "|")
    print(border)
    print("| 1. Read LOCX2".ljust(46) + "|")
    print("| 2. Write LOCX2".ljust(46) + "|")
    print("| 3. Reset LOCX2".ljust(46) + "|")
    print("| 4. Return to Main Menu".ljust(46) + "|")
    print(border + "\n")

def display_adc_menu():
    border = "=" * 48
    print(border)
    print("|" + " ADC Components Menu ".center(46) + "|")
    print(border)
    print("| 1. Read ADC".ljust(46) + "|")
    print("| 2. Write ADC".ljust(46) + "|")
    print("| 3. Reset ADC".ljust(46) + "|")
    print("| 4. Return to Main Menu".ljust(46) + "|")
    print(border + "\n")

def handle_choice(choice, ip):
    if choice == '1':
        ltdb_id = input("Enter an LTDB number: ")
        sca_id_input = input("Enter an SCA number in the range 1-5, or press Enter to check all SCAs: ")
        if sca_id_input:
            try:
                sca_id = int(sca_id_input)
                assert 1 <= sca_id <= 5
            except (ValueError, AssertionError):
                print(f"{Fore.RED}Invalid SCA number. Please enter a number in the range 1-5.{Style.RESET_ALL}")
                return
        else:
            sca_id = None  # SCA is not selected, check all SCAs
        check_sca_status(ip, ltdb_id, sca_id, display_table=True)
    elif choice == '2':
        handle_mtx_menu(ip)
    elif choice == '3':
        handle_mtrx_menu(ip)
    elif choice == '4':
        handle_gbtx_menu(ip)
    elif choice == '5':
        handle_locx2_menu(ip)
    elif choice == '6':
        handle_adc_menu(ip)

def handle_mtx_menu(ip):
    while True:
        display_mtx_menu()
        mtx_choice = input("Enter your choice: ")
        if mtx_choice == '1':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            read_mtx_data(ip, ltdb_id, sca_id)
        elif mtx_choice == '2':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            mtx_id = input("Enter MTX ID: ")
            values = []
            print("Enter 4 values (hexadecimal or decimal).")
            for i in range(4):
                value = input(f"Enter value {i+1}: ")
                values.append(value)
            write_mtx_data(ip, ltdb_id, sca_id, mtx_id, values)
        elif mtx_choice == '3':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            reset_mtx_data(ip, ltdb_id, sca_id)
        elif mtx_choice == '4':
            break  
        else:
            print(f"{Fore.YELLOW}Invalid choice, please try again.{Style.RESET_ALL}")

def handle_mtrx_menu(ip):
    while True:
        display_mtrx_menu()
        mtrx_choice = input("Enter your choice: ")
        if mtrx_choice == '1':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            read_and_show_mtrx(ip, ltdb_id, sca_id)
        elif mtrx_choice == '2':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            value0 = input("Enter Value 0 (hexadecimal or decimal): ")
            value1 = input("Enter Value 1 (hexadecimal or decimal): ")
            write_mtrx_values(ip, ltdb_id, sca_id, value0, value1)
        elif mtrx_choice == '3':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            reset_mtrx_data(ip, ltdb_id, sca_id)
        elif mtrx_choice == '4':
            break  
        else:
            print(f"{Fore.YELLOW}Invalid choice, please try again.{Style.RESET_ALL}")

def handle_gbtx_menu(ip):
    while True:
        display_gbtx_menu()
        gbtx_choice = input("Enter your choice: ")
        if gbtx_choice == '1':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = 1  # Setting SCA id statically to 1 as per logic
            read_gbtx(ip, ltdb_id, sca_id)
        elif gbtx_choice == '2':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            values = [input(f"Value {i+1}: ") for i in range(5)]
            write_gbtx_values(ip, ltdb_id, sca_id, values)
        elif gbtx_choice == '3':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            reset_gbtx_data(ip, ltdb_id, sca_id)
        elif gbtx_choice == '4':
            break  
        else:
            print(f"{Fore.YELLOW}Invalid choice, please try again.{Style.RESET_ALL}")

def handle_locx2_menu(ip):
    while True:
        display_locx2_menu()
        locx2_choice = input("Enter your choice: ")
        if locx2_choice == '1':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            perform_read_locx2(ip, ltdb_id, sca_id)
        elif locx2_choice == '2':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            locx2 = int(input("Enter LOCX2 number (1-4): "))
            values = []
            print("Enter 4 hexadecimal values for LOCX2:")
            for i in range(4):
                value = input(f"Value {i + 1}: ")
                values.append(value)
            perform_write_locx2(ip, ltdb_id, sca_id, locx2, values)
        elif locx2_choice == '3':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            perform_reset_locx2(ip, ltdb_id, sca_id)
        elif locx2_choice == '4':
            break  # Return to Main Menu
        else:
            print(f"{Fore.YELLOW}Invalid choice, please try again.{Style.RESET_ALL}")

def handle_adc_menu(ip):
    while True:
        display_adc_menu()
        adc_choice = input("Enter your choice: ")
        if adc_choice == '1':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            locx2_id = input("Enter LOCX2 ID (1-4), or press Enter to read all: ")
            locx2_id = int(locx2_id) if locx2_id else None  # Convert to integer if not empty
            perform_read_adc(ip, ltdb_id, sca_id, locx2_id)
        elif adc_choice == '2':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            locx2_id = int(input("Enter LOCX2 ID (1-4): "))
            adc_id = int(input("Enter ADC ID (1-4): "))
            value = input("Enter the hexadecimal value to write: ")
            perform_write_adc(ip, ltdb_id, sca_id, locx2_id, adc_id, value)
        elif adc_choice == '3':
            ltdb_id = input("Enter an LTDB number: ")
            sca_id = input("Enter an SCA number: ")
            locx2_id = int(input("Enter LOCX2 ID (1-4): "))
            adc_id = int(input("Enter ADC ID (1-4): "))
            perform_reset_adc(ip, ltdb_id, sca_id, locx2_id, adc_id)
        elif adc_choice == '4':
            break  
        else:
            print(f"{Fore.YELLOW}Invalid choice, please try again.{Style.RESET_ALL}")

def main():
    ip = "opc.tcp://pc-emf-felix-fe-01:48020"
    while True:
        display_menu()
        choice = input("Enter your choice: ")
        if choice in ['1', '2', '3', '4', '5', '6']:
            handle_choice(choice, ip)
        elif choice == '7':
            print("Exiting...")
            break
        else:
            print(f"{Fore.YELLOW}Invalid choice, please try again.{Style.RESET_ALL}")

if __name__ == "__main__":
    main()
