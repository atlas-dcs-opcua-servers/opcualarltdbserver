import argparse
import sys
from colorama import init, Fore
from tabulate import tabulate

init(autoreset=True)
sys.path.append('/localdisk/test_sofian/ScaOpcUa/PyUaoForQuasar/generated')
from I2cSlave import I2cSlave

def write_adc(ip, ltdb_id, sca_id, locx2_id, adc_id, value):
    base_node_id = f"ltdb_{ltdb_id}.scaFelix{sca_id}"
    node_id = f"ns=2;s={base_node_id}.BusADC_LOCx2_{locx2_id}.write_adc{adc_id}"

    i2c_slave = I2cSlave(ip, node_id)
    try:
        value_int = int(value, 16)
        byte_value = value_int.to_bytes(8, byteorder='big')
        i2c_slave.writeValue(byte_value)
        status_message = f"{Fore.GREEN}Success"
    except Exception as e:
        status_message = f"{Fore.RED}Error: {str(e)}"
    finally:
        print(f"Writing value {value} to ADC {adc_id} of LOCx2 {locx2_id}... {status_message}")
        i2c_slave.disconnect()

def perform_write_adc(ip, ltdb_id, sca_id, locx2_id, adc_id, value):
    print(f"Attempting to write to ADC {adc_id} of LOCx2 {locx2_id} with value {value}...")
    write_adc(ip, ltdb_id, sca_id, locx2_id, adc_id, value)

def main():
    ip = 'opc.tcp://pc-emf-felix-fe-01:48020'  # IP prédéfini
    parser = argparse.ArgumentParser(description='Write ADC value via OPC UA.')
    parser.add_argument('--ltdb_id', required=True, help='LTDB ID')
    parser.add_argument('--sca_id', required=True, help='SCA ID')
    parser.add_argument('--locx2_id', type=int, required=True, help='LOCx2 ID (1-4)')
    parser.add_argument('--adc_id', type=int, required=True, help='ADC ID (1-4)')
    parser.add_argument('--value', required=True, help='Hexadecimal value to write')
    args = parser.parse_args()
    
    perform_write_adc(ip, args.ltdb_id, args.sca_id, args.locx2_id, args.adc_id, args.value)

if __name__ == "__main__":
    main()
