import argparse
import logging
from opcua import Client
import sys
sys.path.append('/localdisk/test_sofian/ScaOpcUa/PyUaoForQuasar/generated')
from prettytable import PrettyTable,ALL
from I2cSlave import I2cSlave 
from colorama import Fore, Style


def read_adc(ip, ltdb_id, sca_id, locx2_id):
    results = []

    base_node_id = f"ltdb_{ltdb_id}.scaFelix{sca_id}"

    for adc_id in range(1, 5):
        node_id = f"ns=2;s={base_node_id}.BusADC_LOCx2_{locx2_id}.read_adc{adc_id}"
        i2c_slave = I2cSlave(ip, node_id)
        value = i2c_slave.readValue()
        if value is not None:
            status_code, read_value, timestamp = value
            if status_code.is_good() and isinstance(read_value, bytes):
                read_value_int = int.from_bytes(read_value, 'big')
                result_text = f"0x{read_value_int:x}"
                results.append([f"ADC {adc_id}", result_text])
            else:
                results.append([f"ADC {adc_id}", Fore.RED + "Error reading" + Style.RESET_ALL])
        else:
            results.append([f"ADC {adc_id}", Fore.RED + "No value" + Style.RESET_ALL])
        i2c_slave.disconnect()

    table = PrettyTable()
    table.field_names = [Fore.BLUE + "Register Address" + Style.RESET_ALL, Fore.BLUE + "Result" + Style.RESET_ALL]
    for row in results:
        table.add_row(row)

    print(f" Results for LOCx2_{locx2_id}:\n{table}")
def perform_read_adc(ip, ltdb_id, sca_id, locx2_id=None):
    """
    Encapsulation de la fonction de lecture ADC pour une utilisation dans le menu.
    """
    if locx2_id:
        read_adc(ip, ltdb_id, sca_id, locx2_id)
    else:
        print("Reading ADC values for all LOCx2 components...")
        for locx2_id in range(1, 5):
            read_adc(ip, ltdb_id, sca_id, locx2_id)

def main():
    parser = argparse.ArgumentParser(description='Read ADC status registers via OPC UA.')
    parser.add_argument('--address', default='opc.tcp://pc-emf-felix-fe-01:48020', help='Address connection')
    parser.add_argument('--ltdb_id', default='1', help='LTDB ID')
    parser.add_argument('--sca_id', default='1', help='SCA ID')
    parser.add_argument('--locx2_id', type=int, choices=range(1, 5), help='LOCx2 ID', required=False)

    args = parser.parse_args()

    if args.locx2_id:
        read_adc(args.address, args.ltdb_id, args.sca_id, args.locx2_id)
    else:
        print("No LOCx2 ID specified. Reading all LOCx2 ADCs.")
        for locx2_id in range(1, 5):
            read_adc(args.address, args.ltdb_id, args.sca_id, locx2_id)

if __name__ == "__main__":
    main()
