import argparse
import sys
from colorama import Fore

# Ajout du chemin vers le module LTDB_tools
sys.path.append('/localdisk/test_sofian')
# Import de la fonction reset_adc depuis le module LTDB_tools
from LTDB_tools import reset_adc 

def print_success_arrow():
    arrow = Fore.GREEN + """
              ///
             ///
            ///
           ///
      \\\  ///
       \\\///
    """ + Fore.RESET
    print(arrow)

def print_reset_status(status):
    if not status:
        print("-" * 40)
        print("   \\  /\n    \\/\n    /\\\n   /  \\")
        print(Fore.RED + "Reset operation failed. Please check the system and try again." + Fore.RESET)
        print("-" * 40)
    else:
        print_success_arrow()
        print(Fore.GREEN + "Reset operation completed successfully." + Fore.RESET)

def perform_reset_adc(ip, ltdb_id, sca_id, locx2_id, adc_id):
    reset_status = reset_adc(ip, ltdb_id, sca_id, locx2_id, adc_id)
    print_reset_status(reset_status)

def main():
    parser = argparse.ArgumentParser(description='Reset I2C Matrix via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='SCA ID', required=True)
    parser.add_argument('--locx2_id', help='LOCx2 ID', required=True, type=int, choices=range(1, 5))
    parser.add_argument('--adc_id', help='ADC ID', required=True, type=int, choices=range(1, 5))
    args = parser.parse_args()
    

    reset_status = reset_adc(args.ip, args.ltdb_id, args.sca_id, args.locx2_id, args.adc_id)

    print_reset_status(reset_status)

if __name__ == "__main__":
    main()
