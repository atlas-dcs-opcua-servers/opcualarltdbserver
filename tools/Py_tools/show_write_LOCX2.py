import argparse
import sys
from colorama import init, Fore
from tabulate import tabulate

# Initialisation de colorama
init(autoreset=True)
# Ajout du chemin vers les modules générés pour l'accès OPC UA
sys.path.append('/localdisk/test_sofian/ScaOpcUa/PyUaoForQuasar/generated')
from I2cSlave import I2cSlave

def map_locx2_to_bus(locx2):
    """
    Mappe un numéro LOCX2 à un numéro de bus I2C correspondant.
    """
    return 5 if locx2 in [1, 2] else 7

def write_to_multiple_registers(ip, ltdb_id, sca_id, locx2, values):
    """
    Écrit des valeurs dans des adresses de registres spécifiques basées sur le numéro LOCX2.
    """
    results = []

    # Déterminer les adresses de registre basées sur le numéro LOCX2
    if locx2 in [1, 3]:
        registers = ["04", "05", "06", "07"]
    elif locx2 in [2, 4]:
        registers = ["08", "09", "0A", "0B"]
    else:
        print(f"{Fore.RED}Invalid LOCX2 number.{Fore.RESET}")
        return

    bus_number = map_locx2_to_bus(locx2)
    base_node_id = f"ltdb_{ltdb_id}.scaFelix{sca_id}"

    for register, value in zip(registers, values):
        node_id = f"ns=2;s={base_node_id}.BusI2C_{bus_number}.register{register}"
        i2c_slave = I2cSlave(ip, node_id)
        try:
            value_int = int(value, 16)
            byte_value = value_int.to_bytes(1, byteorder='big')
            i2c_slave.writeValue(byte_value)
            status_message = f"{Fore.GREEN}Success"
        except Exception as e:
            status_message = f"{Fore.RED}Error: {str(e)}"
        finally:
            results.append([node_id, f"0x{value}", status_message])
            i2c_slave.disconnect()

    print(tabulate(results, headers=["Node ID", "Value Written", "Status"], tablefmt="grid"))


def perform_write_locx2(ip, ltdb_id, sca_id, locx2, register_values):
    write_to_multiple_registers(ip, ltdb_id, sca_id, locx2, register_values)



def main():
    parser = argparse.ArgumentParser(description='Write data to I2C bus and registers via OPC UA.')
    parser.add_argument('--ip', default='opc.tcp://pc-emf-felix-fe-01:48020', help='OPC UA Server IP')
    parser.add_argument('--ltdb_id', required=True, help='LTDB ID')
    parser.add_argument('--sca_id', required=True, help='SCA ID')
    parser.add_argument('--locx2', required=True, type=int, choices=range(1, 5), help='LOCX2 number (1-4)')
    parser.add_argument('--regval', required=True, nargs=2, action='append', help='Pairs of register addresses and values to write (hexadecimal)', metavar=('REGISTER', 'VALUE'))

    args = parser.parse_args()

    # Conversion des paires registre/valeur en dictionnaire
    register_values = {pair[0]: pair[1] for pair in args.regval}

    write_to_multiple_registers(args.ip, args.ltdb_id, args.sca_id, args.locx2, register_values)

if __name__ == "__main__":
    main()
