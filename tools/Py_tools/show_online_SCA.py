import sys
import argparse
from prettytable import PrettyTable
sys.path.append('/localdisk/test_sofian')
from LTDB_tools import create_sca, check_online_status

def check_sca_status(ip, ltdb_id, sca_id=None, display_table=False):
    sca_numbers = range(1, 6) if sca_id is None else [sca_id]
    sca_instances = []

    for sca_id in sca_numbers:
        identifier_string = f"ltdb_{ltdb_id}.scaFelix{sca_id}"
        sca = create_sca(ip, identifier_string)
        sca_instances.append(sca)

    results = check_online_status(sca_instances)

    if display_table:
        table = PrettyTable()
        table.field_names = ["Device", "Status"]
        for identifier_string, status in results:
            table.add_row([identifier_string, status])
        print(table)
    else:
	for identifier_string, status in results:
            print(f"{identifier_string} is {status}")

    for sca in sca_instances:
        sca.disconnect()

def main_cli():
    parser = argparse.ArgumentParser(description="Check the status of specified>
    parser.add_argument('--ltdb_id', required=True, type=int, choices=range(1, >
    parser.add_argument('--sca_id', nargs='?', type=int, choices=range(1, 6), d>
    parser.add_argument('--table', action='store_true', help="Display the table>
    args = parser.parse_args()

    ip = "opc.tcp://pc-emf-felix-fe-01:48020"
    check_sca_status(ip, args.ltdb_id, args.sca_id, args.table)

if __name__ == "__main__":
    main_cli()

