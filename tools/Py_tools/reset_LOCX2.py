import sys
sys.path.append('/localdisk/test_sofian')
from LTDB_tools import reset_locx2 

import argparse
from prettytable import PrettyTable
from colorama import init, Fore

def print_success_arrow():
    arrow = Fore.GREEN + """
              ///
             ///
            ///
           ///
      \\\  ///
       \\\///
    """ + Fore.RESET
    print(arrow)

def print_reset_status(status):
    if not status:
        print("-" * 40)
        print("   \\  /\n    \\/\n    /\\\n   /  \\")
        print(Fore.RED + "Reset operation failed. Please check the system and try again." + Fore.RESET)
        print("-" * 40)
    else:
	print_success_arrow()
        print(Fore.GREEN + "Reset operation completed successfully." + Fore.RESET)

def perform_reset_locx2(ip, ltdb_id, sca_id):
    init(autoreset=True)  # Initialise colorama pour le script
    reset_status = reset_locx2(ip, ltdb_id, sca_id)
    print_reset_status(reset_status)


def main():
    parser = argparse.ArgumentParser(description='Reset I2C Matrix via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='SCA ID', default='1')
    args = parser.parse_args()
    reset_status = reset_locx2(args.ip, args.ltdb_id, args.sca_id)
    print_reset_status(reset_status)

if __name__ == "__main__":
    main()

