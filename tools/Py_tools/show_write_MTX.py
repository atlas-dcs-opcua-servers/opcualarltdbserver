import argparse
import sys
from prettytable import PrettyTable
from LTDB_tools import write_mtx

from colorama import init, Fore
init(autoreset=True)  # Initialise colorama pour qu'il réinitialise automatiquement le style après chaque impression.

def write_mtx_data(ip, ltdb_id, sca_id, mtx_id, values):
    def is_hex(value):
        return value.startswith('0x') and all(char in '0123456789abcdefABCDEF' for char in value[2:])

    def is_decimal(value):
        return value.isdigit()

    table = PrettyTable(["Value", "Type"])
    for value in values:
        if is_hex(value):
            value_type = "Hexadecimal"
        elif is_decimal(value):
            value_type = "Decimal"
        else:
            print(f"{Fore.RED}Error: Invalid value '{value}'{Fore.RESET}")
            sys.exit(1)
        table.add_row([value, value_type])

    print(f"MTX ID: {mtx_id}\n")
    print(table)

    
    for value in values:
        print(f"{Fore.GREEN}Writing value {value} to MTX ID {mtx_id}")

    write_mtx(ip, ltdb_id, sca_id, mtx_id, *values)
def main():
    parser = argparse.ArgumentParser(description='Write data to an I2C matrix via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP (default: opc.tcp://pc-emf-felix-fe-01:48020)', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='SCA ID', required=True)
    parser.add_argument('--mtx_id', help='MTX ID', required=True)
    parser.add_argument('--value0', help='Value 0 (hexadecimal or decimal)', required=True)
    parser.add_argument('--value1', help='Value 1 (hexadecimal or decimal)', required=True)
    parser.add_argument('--value2', help='Value 2 (hexadecimal or decimal)', required=True)
    parser.add_argument('--value3', help='Value 3 (hexadecimal or decimal)', required=True)

    args = parser.parse_args()

    values = [args.value0, args.value1, args.value2, args.value3]
    write_mtx_data(args.ip, args.ltdb_id, args.sca_id, args.mtx_id, values)

if __name__ == "__main__":
    main()
