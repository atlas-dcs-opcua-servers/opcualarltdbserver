import sys
from colorama import init
sys.path.append('/localdisk/test_sofian')
from LTDB_tools import read_locx2 

def perform_read_locx2(ip, ltdb_id, sca_id):
    init(autoreset=True)
    # Ici, vous pourriez mettre en place toute logique supplémentaire nécessaire avant/après l'appel à read_locx2
    read_locx2(ip, ltdb_id, sca_id)

def main():
    parser = argparse.ArgumentParser(description='Read I2C Matrix Data via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='SCA ID', required=True)
    
    args = parser.parse_args()
    perform_read_locx2(args.ip, args.ltdb_id, args.sca_id)

if __name__ == "__main__":
    main()
