import argparse
import sys
from prettytable import PrettyTable
from colorama import Fore, init
from LTDB_tools import write_mtrx  

# Initialise colorama
init(autoreset=True)

def convert_value(value):
    """Convertit une chaîne en un entier, gérant à la fois les formats hexadécimaux et décimaux."""
    try:
        if value.startswith('0x'):
            return int(value, 16)  # Convertit en entier à partir d'hexadécimal
        else:
            return int(value)  # Convertit en entier à partir de décimal ou d'une chaîne numérique
    except ValueError:
        print(Fore.RED + f"Erreur: La valeur '{value}' n'est pas valide. Elle doit être décimale ou hexadécimale." + Fore.RESET)
        sys.exit(1)

def write_mtrx_values(ip, ltdb_id, sca_id, value0, value1):
    """Prend des valeurs hexadécimales ou décimales comme entrées et les écrit dans les composants MTRX."""
    values = [value0, value1]  # Liste des valeurs à convertir et à écrire
    converted_values = [convert_value(value) for value in values]  # Convertit chaque valeur

   
    table = PrettyTable(["Value", "Converted Value", "Type"])
    for value, converted in zip(values, converted_values):
        value_type = "Hexadecimal" if value.startswith('0x') else "Decimal"
        table.add_row([value, converted, value_type])

    print(table)

    # Appel à la fonction d'écriture avec les valeurs converties
    write_mtrx(ip, ltdb_id, sca_id, *converted_values)
    print(Fore.GREEN + "Écriture réussie." + Fore.RESET)

def main():
    parser = argparse.ArgumentParser(description='Écriture de données dans une matrice MTRX via OPC UA')
    parser.add_argument('--ip', help='Adresse IP du serveur OPC UA', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='Identifiant LTDB', required=True)
    parser.add_argument('--sca_id', help='Identifiant SCA', required=True)
    parser.add_argument('--value0', help='Valeur 0 (hexadécimale ou décimale)', required=True)
    parser.add_argument('--value1', help='Valeur 1 (hexadécimale ou décimale)', required=True)
    
    args = parser.parse_args()
    write_mtrx_values(args.ip, args.ltdb_id, args.sca_id, args.value0, args.value1)

if __name__ == "__main__":
    main()
