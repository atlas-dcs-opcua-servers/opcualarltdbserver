import sys
import argparse
from colorama import init
sys.path.append('/localdisk/test_sofian')
from LTDB_tools import  read_gbtx

def main():
    init(autoreset=True)  # Initialise colorama pour auto-reset

    parser = argparse.ArgumentParser(description='Read I2C GBTX Data via OPC UA')
    parser.add_argument('--ip', help='OPC UA Server IP', default='opc.tcp://pc-emf-felix-fe-01:48020')
    parser.add_argument('--ltdb_id', help='LTDB ID', required=True)
    parser.add_argument('--sca_id', help='GBTX IS',default='1')
    
    args = parser.parse_args()
    read_gbtx(args.ip, args.ltdb_id, args.sca_id)

if __name__ == "__main__":
    main()
