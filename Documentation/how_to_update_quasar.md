 - clone the opcualarltdb project
 - `git clone https://gitlab.cern.ch/atlas-dcs-opcua-servers/opcualarltdbserver.git`
 - `cd opcualarltdbserver`
 - create a branch to do the update
 - `git checkout -b update_quasar`

 - clone the quasar project
 - `cd ..`
 - `git clone https://gitlab.cern.ch/quasar-team/quasar.git`
 - `cd quasar/`
 - `git submodule init`
 - `git submodule update`

 - change to the current version (here it's an example, use the last one)
 - `git checkout v1.5.14`

 - use the scripts to update
 - `./quasar.py quasar_version`
 - `./quasar.py upgrade_project ../opcualarltdbserver/`

 - go back to the opcualarltdb project
 - `cd ../opcualarltdbserver/`
 - check the quasar version
 - `./quasar.py quasar_version`
 - commit and push to the repo
 - `git add --all`
 - `git commit -m "upgrade quasar"`
 - `git push --set-upstream origin update_quasar`
