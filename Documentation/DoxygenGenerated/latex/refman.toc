\contentsline {chapter}{\numberline {1}Deprecated List}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Hierarchical Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class Hierarchy}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}File List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Class Documentation}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Adc\discretionary {-}{}{}Id Class Reference}{9}{section.5.1}
\contentsline {section}{\numberline {5.2}Address\discretionary {-}{}{}Space\discretionary {-}{}{}:\discretionary {-}{}{}:A\discretionary {-}{}{}S\discretionary {-}{}{}Delegating\discretionary {-}{}{}Method$<$ Object\discretionary {-}{}{}Type $>$ Class Template Reference}{9}{section.5.2}
\contentsline {section}{\numberline {5.3}Address\discretionary {-}{}{}Space\discretionary {-}{}{}:\discretionary {-}{}{}:A\discretionary {-}{}{}S\discretionary {-}{}{}Delegating\discretionary {-}{}{}Variable$<$ Object\discretionary {-}{}{}Type $>$ Class Template Reference}{10}{section.5.3}
\contentsline {section}{\numberline {5.4}Address\discretionary {-}{}{}Space\discretionary {-}{}{}:\discretionary {-}{}{}:A\discretionary {-}{}{}S\discretionary {-}{}{}Node\discretionary {-}{}{}Manager Class Reference}{10}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Member Function Documentation}{11}{subsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.1.1}make\discretionary {-}{}{}Child\discretionary {-}{}{}Node\discretionary {-}{}{}Id}{11}{subsubsection.5.4.1.1}
\contentsline {section}{\numberline {5.5}Address\discretionary {-}{}{}Space\discretionary {-}{}{}:\discretionary {-}{}{}:A\discretionary {-}{}{}S\discretionary {-}{}{}Source\discretionary {-}{}{}Variable Class Reference}{11}{section.5.5}
\contentsline {section}{\numberline {5.6}Address\discretionary {-}{}{}Space\discretionary {-}{}{}:\discretionary {-}{}{}:A\discretionary {-}{}{}S\discretionary {-}{}{}Source\discretionary {-}{}{}Variable\discretionary {-}{}{}Io\discretionary {-}{}{}Manager Class Reference}{12}{section.5.6}
\contentsline {section}{\numberline {5.7}Base\discretionary {-}{}{}Quasar\discretionary {-}{}{}Server Class Reference}{12}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Member Function Documentation}{13}{subsection.5.7.1}
\contentsline {subsubsection}{\numberline {5.7.1.1}get\discretionary {-}{}{}Node\discretionary {-}{}{}Manager}{13}{subsubsection.5.7.1.1}
\contentsline {section}{\numberline {5.8}Address\discretionary {-}{}{}Space\discretionary {-}{}{}:\discretionary {-}{}{}:Change\discretionary {-}{}{}Notifying\discretionary {-}{}{}Variable Class Reference}{14}{section.5.8}
\contentsline {section}{\numberline {5.9}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Clk\discretionary {-}{}{}Des\discretionary {-}{}{}Scan Class Reference}{14}{section.5.9}
\contentsline {section}{\numberline {5.10}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Fiber\discretionary {-}{}{}Mon Class Reference}{15}{section.5.10}
\contentsline {section}{\numberline {5.11}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}L\discretionary {-}{}{}Ar\discretionary {-}{}{}Backend Class Reference}{15}{section.5.11}
\contentsline {subsection}{\numberline {5.11.1}Member Function Documentation}{16}{subsection.5.11.1}
\contentsline {subsubsection}{\numberline {5.11.1.1}create\discretionary {-}{}{}Adc\discretionary {-}{}{}Id}{16}{subsubsection.5.11.1.1}
\contentsline {subsubsection}{\numberline {5.11.1.2}create\discretionary {-}{}{}Fiber\discretionary {-}{}{}Id}{16}{subsubsection.5.11.1.2}
\contentsline {section}{\numberline {5.12}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Locx2\discretionary {-}{}{}Scan Class Reference}{16}{section.5.12}
\contentsline {section}{\numberline {5.13}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb Class Reference}{17}{section.5.13}
\contentsline {section}{\numberline {5.14}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}C\discretionary {-}{}{}L\discretionary {-}{}{}O\discretionary {-}{}{}Cx2 Class Reference}{18}{section.5.14}
\contentsline {section}{\numberline {5.15}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}C\discretionary {-}{}{}Nevis Class Reference}{18}{section.5.15}
\contentsline {section}{\numberline {5.16}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Fiber Class Reference}{20}{section.5.16}
\contentsline {section}{\numberline {5.17}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Fiber\discretionary {-}{}{}Pos1 Class Reference}{20}{section.5.17}
\contentsline {section}{\numberline {5.18}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Fiber\discretionary {-}{}{}Pos2 Class Reference}{21}{section.5.18}
\contentsline {section}{\numberline {5.19}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}G\discretionary {-}{}{}B\discretionary {-}{}{}Tx Class Reference}{21}{section.5.19}
\contentsline {section}{\numberline {5.20}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Gpio Class Reference}{22}{section.5.20}
\contentsline {subsection}{\numberline {5.20.1}Member Function Documentation}{23}{subsection.5.20.1}
\contentsline {subsubsection}{\numberline {5.20.1.1}call\discretionary {-}{}{}Configure\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}C}{23}{subsubsection.5.20.1.1}
\contentsline {subsubsection}{\numberline {5.20.1.2}call\discretionary {-}{}{}Power\discretionary {-}{}{}Off}{23}{subsubsection.5.20.1.2}
\contentsline {subsubsection}{\numberline {5.20.1.3}call\discretionary {-}{}{}Power\discretionary {-}{}{}On}{23}{subsubsection.5.20.1.3}
\contentsline {subsubsection}{\numberline {5.20.1.4}call\discretionary {-}{}{}Reset\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}C}{23}{subsubsection.5.20.1.4}
\contentsline {subsubsection}{\numberline {5.20.1.5}configure\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}C}{23}{subsubsection.5.20.1.5}
\contentsline {subsubsection}{\numberline {5.20.1.6}power}{24}{subsubsection.5.20.1.6}
\contentsline {subsubsection}{\numberline {5.20.1.7}reset\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}C}{24}{subsubsection.5.20.1.7}
\contentsline {subsubsection}{\numberline {5.20.1.8}reset\discretionary {-}{}{}G\discretionary {-}{}{}B\discretionary {-}{}{}Tx}{24}{subsubsection.5.20.1.8}
\contentsline {subsubsection}{\numberline {5.20.1.9}reset\discretionary {-}{}{}L\discretionary {-}{}{}O\discretionary {-}{}{}Cx}{24}{subsubsection.5.20.1.9}
\contentsline {subsubsection}{\numberline {5.20.1.10}reset\discretionary {-}{}{}M\discretionary {-}{}{}T\discretionary {-}{}{}Rx}{25}{subsubsection.5.20.1.10}
\contentsline {subsubsection}{\numberline {5.20.1.11}reset\discretionary {-}{}{}M\discretionary {-}{}{}Tx}{26}{subsubsection.5.20.1.11}
\contentsline {section}{\numberline {5.21}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}L\discretionary {-}{}{}O\discretionary {-}{}{}Cx2 Class Reference}{26}{section.5.21}
\contentsline {section}{\numberline {5.22}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Mtrx Class Reference}{27}{section.5.22}
\contentsline {section}{\numberline {5.23}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}M\discretionary {-}{}{}Tx Class Reference}{27}{section.5.23}
\contentsline {section}{\numberline {5.24}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Sca Class Reference}{28}{section.5.24}
\contentsline {section}{\numberline {5.25}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Mapping Class Reference}{29}{section.5.25}
\contentsline {section}{\numberline {5.26}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Mon Class Reference}{30}{section.5.26}
\contentsline {section}{\numberline {5.27}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}S\discretionary {-}{}{}C\discretionary {-}{}{}Mon Class Reference}{31}{section.5.27}
\contentsline {section}{\numberline {5.28}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Status\discretionary {-}{}{}Latome Class Reference}{31}{section.5.28}
\contentsline {section}{\numberline {5.29}Device\discretionary {-}{}{}:\discretionary {-}{}{}:D\discretionary {-}{}{}Stress\discretionary {-}{}{}Test Class Reference}{32}{section.5.29}
\contentsline {section}{\numberline {5.30}Fiber\discretionary {-}{}{}Id Class Reference}{32}{section.5.30}
\contentsline {section}{\numberline {5.31}Device\discretionary {-}{}{}:\discretionary {-}{}{}:Node\discretionary {-}{}{}Id Class Reference}{33}{section.5.31}
\contentsline {section}{\numberline {5.32}Opc\discretionary {-}{}{}Server\discretionary {-}{}{}Callback Class Reference}{33}{section.5.32}
\contentsline {subsection}{\numberline {5.32.1}Detailed Description}{34}{subsection.5.32.1}
\contentsline {subsection}{\numberline {5.32.2}Constructor \& Destructor Documentation}{34}{subsection.5.32.2}
\contentsline {subsubsection}{\numberline {5.32.2.1}Opc\discretionary {-}{}{}Server\discretionary {-}{}{}Callback}{34}{subsubsection.5.32.2.1}
\contentsline {subsubsection}{\numberline {5.32.2.2}$\sim $\discretionary {-}{}{}Opc\discretionary {-}{}{}Server\discretionary {-}{}{}Callback}{34}{subsubsection.5.32.2.2}
\contentsline {subsection}{\numberline {5.32.3}Member Function Documentation}{34}{subsection.5.32.3}
\contentsline {subsubsection}{\numberline {5.32.3.1}create\discretionary {-}{}{}Session}{34}{subsubsection.5.32.3.1}
\contentsline {subsubsection}{\numberline {5.32.3.2}create\discretionary {-}{}{}Ua\discretionary {-}{}{}Server}{34}{subsubsection.5.32.3.2}
\contentsline {subsubsection}{\numberline {5.32.3.3}logon\discretionary {-}{}{}Session\discretionary {-}{}{}User}{35}{subsubsection.5.32.3.3}
\contentsline {section}{\numberline {5.33}Quasar\discretionary {-}{}{}Server Class Reference}{36}{section.5.33}
\contentsline {section}{\numberline {5.34}Quasar\discretionary {-}{}{}Server\discretionary {-}{}{}Callback Class Reference}{36}{section.5.34}
\contentsline {subsection}{\numberline {5.34.1}Constructor \& Destructor Documentation}{37}{subsection.5.34.1}
\contentsline {subsubsection}{\numberline {5.34.1.1}Quasar\discretionary {-}{}{}Server\discretionary {-}{}{}Callback}{37}{subsubsection.5.34.1.1}
\contentsline {subsubsection}{\numberline {5.34.1.2}$\sim $\discretionary {-}{}{}Quasar\discretionary {-}{}{}Server\discretionary {-}{}{}Callback}{37}{subsubsection.5.34.1.2}
\contentsline {subsection}{\numberline {5.34.2}Member Function Documentation}{37}{subsection.5.34.2}
\contentsline {subsubsection}{\numberline {5.34.2.1}create\discretionary {-}{}{}Session}{37}{subsubsection.5.34.2.1}
\contentsline {subsubsection}{\numberline {5.34.2.2}create\discretionary {-}{}{}Ua\discretionary {-}{}{}Server}{37}{subsubsection.5.34.2.2}
\contentsline {subsubsection}{\numberline {5.34.2.3}logon\discretionary {-}{}{}Session\discretionary {-}{}{}User}{37}{subsubsection.5.34.2.3}
\contentsline {section}{\numberline {5.35}Server\discretionary {-}{}{}Config\discretionary {-}{}{}Xml Class Reference}{38}{section.5.35}
\contentsline {subsection}{\numberline {5.35.1}Detailed Description}{38}{subsection.5.35.1}
\contentsline {subsection}{\numberline {5.35.2}Constructor \& Destructor Documentation}{38}{subsection.5.35.2}
\contentsline {subsubsection}{\numberline {5.35.2.1}Server\discretionary {-}{}{}Config\discretionary {-}{}{}Xml}{38}{subsubsection.5.35.2.1}
\contentsline {subsubsection}{\numberline {5.35.2.2}$\sim $\discretionary {-}{}{}Server\discretionary {-}{}{}Config\discretionary {-}{}{}Xml}{39}{subsubsection.5.35.2.2}
\contentsline {subsection}{\numberline {5.35.3}Member Function Documentation}{39}{subsection.5.35.3}
\contentsline {subsubsection}{\numberline {5.35.3.1}load\discretionary {-}{}{}Configuration}{39}{subsubsection.5.35.3.1}
\contentsline {subsubsection}{\numberline {5.35.3.2}save\discretionary {-}{}{}Configuration}{39}{subsubsection.5.35.3.2}
\contentsline {section}{\numberline {5.36}Ua\discretionary {-}{}{}Endpoint\discretionary {-}{}{}Xml Class Reference}{39}{section.5.36}
\contentsline {subsection}{\numberline {5.36.1}Detailed Description}{40}{subsection.5.36.1}
\contentsline {subsection}{\numberline {5.36.2}Constructor \& Destructor Documentation}{40}{subsection.5.36.2}
\contentsline {subsubsection}{\numberline {5.36.2.1}Ua\discretionary {-}{}{}Endpoint\discretionary {-}{}{}Xml}{40}{subsubsection.5.36.2.1}
\contentsline {subsubsection}{\numberline {5.36.2.2}$\sim $\discretionary {-}{}{}Ua\discretionary {-}{}{}Endpoint\discretionary {-}{}{}Xml}{40}{subsubsection.5.36.2.2}
\contentsline {subsection}{\numberline {5.36.3}Member Function Documentation}{40}{subsection.5.36.3}
\contentsline {subsubsection}{\numberline {5.36.3.1}set\discretionary {-}{}{}Xml\discretionary {-}{}{}Config}{40}{subsubsection.5.36.3.1}
\contentsline {chapter}{\numberline {6}File Documentation}{41}{chapter.6}
\contentsline {section}{\numberline {6.1}/home/efortin/\discretionary {-}{}{}Opcua/opcualarltdbserver/\discretionary {-}{}{}Device/include/\discretionary {-}{}{}D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Gpio.h File Reference}{41}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Detailed Description}{41}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}/home/efortin/\discretionary {-}{}{}Opcua/opcualarltdbserver/\discretionary {-}{}{}Device/src/\discretionary {-}{}{}D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Gpio.cpp File Reference}{42}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Detailed Description}{42}{subsection.6.2.1}
\contentsline {section}{\numberline {6.3}/home/efortin/\discretionary {-}{}{}Opcua/opcualarltdbserver/\discretionary {-}{}{}Device/src/functions/\discretionary {-}{}{}D\discretionary {-}{}{}Ltdb\discretionary {-}{}{}Gpio\discretionary {-}{}{}\_\discretionary {-}{}{}functions.cpp File Reference}{42}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Detailed Description}{42}{subsection.6.3.1}
\contentsline {part}{Index}{44}{section*.62}
