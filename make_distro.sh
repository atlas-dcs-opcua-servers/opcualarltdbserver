echo "make_distro: trying to create a package for commit id $1"
DIR=OpcUaLarLtdbServer-$1
rm -Rf $DIR $DIR".tar.gz" 
mkdir $DIR 
cp build/bin/OpcUaLarLtdbServer \
bin/ServerConfig.xml \
configGenerator/ConfigGenerator.sh \
build/Configuration/Configuration.xsd \
Design/diagram.pdf \
Documentation/ConfigDocumentation.html \
Documentation/AddressSpaceDoc.html \
Documentation/DoxygenGenerated/latex/refman.pdf \
$DIR || exit 1 
tar cf $DIR".tar" $DIR 
gzip -9 $DIR".tar"  
