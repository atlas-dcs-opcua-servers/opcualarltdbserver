# LICENSE:
# Copyright (c) 2020, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Initial author: Piotr Nikiel <piotr@nikiel.info> <-- specific users: remove this line and put your own name ;-)
# This file *WILL NOT* be overwritten by quasar tools.

set(DEVICE_CUSTOM_SOURCES
	# here place the list of your custom sources from Device module,
	# e.g. src/MyFile1.cpp src/DeviceClass.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/LtdbCommon.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/NodeId.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/xml_parser.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLocx2Scan_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLArBackend_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdb_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbLOCx2_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbADCNevis_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbFiber_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbGBTx_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbGpio_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbMtrx_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbMTx_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DLtdbSca_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DMapping_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DStatusLatome_functions.cpp
    ${PROJECT_SOURCE_DIR}/Device/src/functions/DClkDesScan_functions.cpp
	)
