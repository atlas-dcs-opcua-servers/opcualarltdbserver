/*
 * LtdbCommon.h
 *
 *  Created on: 27 nov. 2018
 *      Author: efortin
 */
#include <cstdint>

#ifndef DEVICE_INCLUDE_LTDBCOMMON_H_
#define DEVICE_INCLUDE_LTDBCOMMON_H_

#define CPPM true

#define METHOD_IN_PROGRESS 1
#define METHOD_GOOD 2
#define METHOD_BAD 3

//define value for monitoring status of component
#define STATUS_UNKNOWN 1
#define STATUS_GOOD 2
#define STATUS_BAD 3
#define STATUS_NOT_SUPERVISED 4


#define STATE_OFF 0
#define STATE_ON 1
#define STATE_INITIAL 2
#define STATE_CONFIGURED 3
#define STATE_UNKNOWN 4

#ifndef DEVICE_INCLUDE_I2CSLAVE_H_
#define DEVICE_INCLUDE_I2CSLAVE_H_
#include <I2cSlave.h>
#include <DigitalIO.h>
#include <DStats.h>

#include <LogIt.h>
#endif



std::string tostr (int x);
std::string tostrdec (int x);

std::vector<std::string> tokenize(std::string value,  std::string separator);
UaStatus secureI2CWrite( UaoClientForOpcUaSca::I2cSlave reg , OpcUa_Byte value, std::string logId="Unknown", int MaxRetry =5 , int flags =0 ,Device::DStats *stats=nullptr);
UaStatus secureGpioWrite( UaoClientForOpcUaSca::DigitalIO gpio , OpcUa_Boolean value, std::string nodeId,std::string logID, int MaxRetry=5);
float readFromFelix(std::string id,int card,std::string parameter);
std::vector<std::string> str_split (std::string s, std::string delimiter);



class FiberId{
	public:

		FiberId(int latomeIdInit,int fiberIdInit);
		int getLatomeId(){return latomeId;}
		int getFiberId(){return fiberId;}
		void logPrint();

	protected:

		int latomeId; //The number of the latome
		int fiberId; //the umber of the latome fiber
};

class AdcId : public FiberId{
public:
	~AdcId(){delete[] channelPosUser;delete[] userStreamId;delete[] channelPosIstage;}
	AdcId(int latomeIdInit,int fiberIdInit,std::string connectionFileInit,int *channelPosUserInit,int *userStreamIdInit,int *channelPosIstageInit,int istageStreamIdInit);
	int getChannelPosUser(int channel){return channelPosUser[channel-1];}
	int getUserStreamId(int channel){return userStreamId[channel-1];}
	int getChannelPosIstage(int channel){return channelPosIstage[channel-1];}
	int getIstageStreamId(){return istageStreamId;}
	void logPrint();
protected:
	int *channelPosUser; //size 4
	int *userStreamId;
	int *channelPosIstage;
	int istageStreamId;
};


Log::LOG_LEVEL logStrtoLog(std::string value);

enum
{
	I2C_IGNORE_ERROR = 1 << 0,
    I2C_NO_READBACK = 1 << 2,
};


std::string exec(const char* cmd);


#include <LogIt.h>
#include <DLArBackend.h>
#include <DLtdb.h>
#include <DLtdbSca.h>
#include <DClkDesScan.h>
#include <DLtdbADCNevis.h>
#include <DLtdbFiber.h>
#include <DLtdbGBTx.h>
#include <DLtdbGpio.h>
#include <DLtdbLOCx2.h>
#include <DLtdbMtrx.h>
#include <DLtdbMTx.h>
#include <DLtdbSca.h>
#include <DMapping.h>
#include <DStatusLatome.h>
#include <NodeId.h>
#include <iostream>
#include <string>
#include <fstream>




#endif /* DEVICE_INCLUDE_LTDBCOMMON_H_ */
