#ifndef XMLPARSERH_INCLUDED
#define XMLPARSERH_INCLUDED



#include <DLtdbMTx.h>
#include <DLtdbLOCx2.h>
#include <DLtdbGBTx.h>
#include <DLtdbMtrx.h>
#include <DLtdbADCNevis.h>
//CLASS FOR PARSER



#define DEGRADED true
#define NORMAL false

typedef struct adc{
  Device::DLtdbADCNevis::Adc_Mode mode = Device::DLtdbADCNevis::NORMAL_MODE;
}Adc;

typedef struct mtx{
  uint8_t register01=0;
  uint8_t register02=0;
  uint8_t register03=0;
  uint8_t register04=0;
  //bool degMode=false;
  Device::DLtdbMTx::Mtx_Mode mode = Device::DLtdbMTx::NORMAL_MODE;
}Mtx;

typedef struct mtrx{
  uint8_t register01=0;
  uint8_t register02=0;
  Device::DLtdbMtrx::Mtrx_Mode mode = Device::DLtdbMtrx::NORMAL_MODE;
}Mtrx;

typedef struct locx{
  uint8_t register01=0;
  uint8_t register02=0;
  Adc adc[4];
  Device::DLtdbLOCx2::Locx_Mode mode = Device::DLtdbLOCx2::NORMAL_MODE;
}Locx;

typedef struct gbtx{
  uint8_t register35=0;
  Device::DLtdbGBTx::Gbtx_Mode mode = Device::DLtdbGBTx::NORMAL_MODE;

}Gbtx;

typedef struct sca{
  Mtx mtx[4];
  Locx locx2[4];
  Mtrx mtrx;
  Gbtx gbtx;
}Sca;

typedef struct ltdb{
  Sca sca[5];
}Ltdb;

//XML PARSER
UaStatus generalXml(Ltdb &ltdb,std::string genConfFileName);
UaStatus specificXml(Ltdb &ltdb,std::string specConfFileName);
void ltdbPrint(Ltdb ltdb);

#endif
