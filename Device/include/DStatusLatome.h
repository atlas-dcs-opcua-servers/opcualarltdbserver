
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceHeader.xslt) 
    on 2019-01-15T14:34:40.963+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */





#ifndef __DStatusLatome__H__
#define __DStatusLatome__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DStatusLatome.h>
#include "uhal/uhal.hpp"
#include <LtdbCommon.h>
#include <LogIt.h>
#include <boost/thread/thread.hpp>
#include <LtdbCommon.h>
using namespace uhal;

namespace Device
{




  class DStatusLatome:public Base_DStatusLatome
  {

  public:
    /* sample constructor */
    explicit DStatusLatome (const Configuration::StatusLatome & config,
			    Parent_DStatusLatome * parent);
    /* sample dtr */
     ~DStatusLatome ();




    /* delegators for
       cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeCallReadChannelReady (OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeCallReadACR (OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeCallChkChannelReadyError (OpcUa_Boolean & v);


    /* delegators for methods */

    UaStatus callReadChannelReady (OpcUa_UInt64 & chReady);

    UaStatus callReadACR (OpcUa_UInt64 & acr);

    UaStatus callChkChannelReadyError (OpcUa_UInt64 & chStatus);

    UaStatus callReadAdcMean (OpcUa_Int32 fiberNum,
			      OpcUa_Int32 SCNum, OpcUa_Int32 & mean);

    UaStatus callScanAdcMean (const UaString & fileName);

    
  private:
    /* Delete copy constructor and assignment operator */
      DStatusLatome (const DStatusLatome &);
      DStatusLatome & operator= (const DStatusLatome & other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

  public:
     UaStatus readChannelReady (OpcUa_UInt64 & chReady);
     UaStatus readACR (OpcUa_UInt64 & acr);
	// UaStatus chkChannelReadyError (OpcUa_UInt64 & chStatus);
	 UaStatus readAdcMean (OpcUa_Int32 fiberNum,
     			      OpcUa_Int32 SCNum, OpcUa_Int32 & mean);
	 UaStatus scanAdcMean (const UaString & fileName);
	 UaStatus chkChannelReadyError (OpcUa_UInt64 & chStatus,int timeChk=-1);
	 UaStatus  activeFiber(int totalFiberNum,bool & status);
      UaStatus statusFiber(int totalFiberNum ,bool & status, bool fastCheck,int timeChk=5);
      UaStatus meanSuperCellIstage(int FiberNum ,int SCNum, int & mean);
      UaStatus meanSuperCellIstage(AdcId *adcId ,int channel, int & mean);
      UaStatus meanSuperCellUser(int userChannel ,int SCNum, int & mean);
      UaStatus meanSuperCellUser(int userChannel , int *mean); // read the 5 mean in the User
      UaStatus meanSuperCellUser(AdcId *adcId ,int channel, int & mean);
      UaStatus readChannelReadyWithoutMapping (OpcUa_UInt64 & chReady,HwInterface *hwUsed);
      UaStatus readScopeIstage(int streamIndex,int *data);
      UaStatus readScopeIstage(int streamIndex,int pos,int &data);
      UaStatus resetAllTransceiver();

      UaStatus getFiberChReadyErrCounter(FiberId* fiberId, OpcUa_Byte &errCounter);
      std::string logID;
      bool connectHardware();

      UaStatus updateReadACR();
      UaStatus updateChkChannelReadyError();


  private:
      const std::string Getfw_version(HwInterface *device);
      std::map<int, HwInterface*> interface;
      //bool connectHardware();
	  HwInterface *hw;
	  bool isConnected;

  };





}

#endif // include guard
