/*
 * NodeId.h
 *
 *  Created on: 21 mars 2019
 *      Author: etienne fortin (CPPM)
 */

#ifndef DEVICE_INCLUDE_NODEID_H_
#define DEVICE_INCLUDE_NODEID_H_

#include <string>
namespace Device
{




  class NodeId
  {
  	  public:
        const static std::string global_load_clear;
	  	  const static std::string ch_ready_0;
	  	  const static std::string ch_ready_1;
	  	  const static std::string active_channel_0;
	  	  const static std::string active_channel_1;
	  	  const static std::string count_load_clear[48];
	  	  const static std::string ch_ready_error[48];
	  	  const static std::string usr_mean[62];
	  	  const static std::string scp_start_bcid;
        const static std::string resetTransceiverBlock1;
        const static std::string resetTransceiverBlock2;
        const static std::string resetTransceiverBlock3;
        const static std::string resetTransceiverBlock4;
  		  const static std::string scp_ctrl_start;
	  	  const static std::string scp_status_ready;
		    const static std::string scp_data[48];
  	  private:

  };

}


#endif /* DEVICE_INCLUDE_NODEID_H_ */
