
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt) 
    on 2019-03-14T13:09:54.083+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */




#include <Configuration.hxx>

#include <DMapping.h>
#include <ASMapping.h>


#include <DLtdbSca.h>
#include <DLtdbLOCx2.h>
#include <DLocx2Scan.h>
#include <DLtdbFiber.h>
#include <DStatusLatome.h>
#include <LtdbCommon.h>
#include <DLtdbADCNevis.h>
#include <iostream>

#include <string>
#include <fstream>

namespace Device
{




  // 1111111111111111111111111111111111111111111111111111111111111111111111111
  // 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
  // 1     Users don't modify this code!!!!                                  1
  // 1     If you modify this code you may start a fire or a flood somewhere,1
  // 1     and some human being may possible cease to exist. You don't want  1
  // 1     to be charged with that!                                          1 
  // 1111111111111111111111111111111111111111111111111111111111111111111111111






  // 2222222222222222222222222222222222222222222222222222222222222222222222222
  // 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
  // 2     (code for which only stubs were generated automatically)          2
  // 2     You should add the implementation but dont alter the headers      2
  // 2     (apart from constructor, in which you should complete initializati2
  // 2     on list)                                                          2 
  // 2222222222222222222222222222222222222222222222222222222222222222222222222

  /* sample ctr */
  DMapping::DMapping (const Configuration::Mapping & config,
		      Parent_DMapping * parent):Base_DMapping (config, parent)
    /* fill up constructor initialization list here */
  {
    /* fill up constructor body here */
	  isGenerated=false;
	  isLoaded=false;
	  reloadMappingFromFiles();
	  isLoaded=true;
	  logID=getParent()->logID + "Mapping: ";
  }

  /* sample dtr */
  DMapping::~DMapping ()
  {
  }

  /* delegators for cachevariables and externalvariables */

  /* Note: never directly call this function. */

   UaStatus DMapping::writeCallCheckMapping (const OpcUa_Boolean & v)
   {
	    OpcUa_Byte result;
	    UaStatus status = OpcUa_Bad;
		getAddressSpaceLink()->setCheckMappingError(METHOD_IN_PROGRESS,OpcUa_Good);
		UaClientSdk::UaSession *session=ClientSessionFactory::connect(getParent()->scaOpcUaEndpoints().c_str());
		if (!session){
			 LOG(Log::ERR)<<"Bad communication Error";
			 status= OpcUa_BadCommunicationError;
		  }
		else{
			status = checkMapping(session,result);
				  }

		if (!status.isGood()){

			getAddressSpaceLink()->setCheckMappingError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setCheckMappingResult((OpcUa_Int32)result,OpcUa_Good);
			getAddressSpaceLink()->setCheckMappingError(METHOD_GOOD,OpcUa_Good);
		}
		return status;
   }

  /* Note: never directly call this function. */

  UaStatus DMapping::writeCallGenerateMapping (const OpcUa_Boolean & v)
  {
	  	getAddressSpaceLink()->setGenerateMappingError(METHOD_IN_PROGRESS,OpcUa_Good);
	  	UaStatus status = OpcUa_Bad;
	  	UaClientSdk::UaSession *session=ClientSessionFactory::connect(getParent()->scaOpcUaEndpoints().c_str());
		  if (!session){
			 LOG(Log::ERR)<<"Bad communication Error";
			 status= OpcUa_BadCommunicationError;
		  }
		  else{
			  status = generateMapping(session);
		  }

		 delete session;



		if (!status.isGood()){
			getAddressSpaceLink()->setGenerateMappingError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setGenerateMappingError(METHOD_GOOD,OpcUa_Good);
		}
		return status;
  }

  /* Note: never directly call this function. */

  UaStatus DMapping::writeCallGenerateMappingFiles (const OpcUa_Boolean & v)
  {
	  getAddressSpaceLink()->setGenerateMappingFilesError(METHOD_IN_PROGRESS,OpcUa_Good);
		UaStatus status = generateMappingFiles();
		if (!status.isGood()){
			getAddressSpaceLink()->setGenerateMappingFilesError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setGenerateMappingFilesError(METHOD_GOOD,OpcUa_Good);
		}
		return status;
  }

  /* Note: never directly call this function. */

  UaStatus DMapping::writeCallReadMappingFiber (const OpcUa_Boolean & v)
  {
	  OpcUa_Byte fiberLatome;
	  getAddressSpaceLink()->setReadMappingFiberError(METHOD_IN_PROGRESS,OpcUa_Good);
	  UaStatus status=readMappingFiber (getAddressSpaceLink()->getReadMappingFiberMode(),
			  getAddressSpaceLink()->getReadMappingFiberFiberNb(),
			  fiberLatome);

		if (!status.isGood()){
			getAddressSpaceLink()->setReadMappingFiberError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setReadMappingFiberError(METHOD_GOOD,OpcUa_Good);
			getAddressSpaceLink()->setReadMappingFiberFiberLatome(fiberLatome,OpcUa_Good);
		}
		return status;
  }

  /* Note: never directly call this function. */

  UaStatus DMapping::writeCallReadMappingSCUser (const OpcUa_Boolean & v)
  {
	  OpcUa_Byte  channelUserLatome;
	  OpcUa_Byte  posUserLatome;
	  getAddressSpaceLink()->setReadMappingSCUserError(METHOD_IN_PROGRESS,OpcUa_Good);
	  UaStatus status=readMappingSCUser (getAddressSpaceLink()->getReadMappingSCUserMode(),
			  getAddressSpaceLink()->getReadMappingSCUserChannelNb(),
			  channelUserLatome,posUserLatome);

		if (!status.isGood()){
			getAddressSpaceLink()->setReadMappingSCUserError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setReadMappingSCUserError(METHOD_GOOD,OpcUa_Good);
			getAddressSpaceLink()->setReadMappingSCUserChannelUserLatome(channelUserLatome,OpcUa_Good);
			getAddressSpaceLink()->setReadMappingSCUserPosUserLatome(posUserLatome,OpcUa_Good);
		}
		return status;
  }

  /* Note: never directly call this function. */

  UaStatus DMapping::writeCallUpdateMappingUsed (const OpcUa_Boolean & v)
  {
	  getAddressSpaceLink()->setUpdateMappingUsedError(METHOD_IN_PROGRESS,OpcUa_Good);
		UaStatus status = updateMappingUsed();
		if (!status.isGood()){
			getAddressSpaceLink()->setUpdateMappingUsedError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setUpdateMappingUsedError(METHOD_GOOD,OpcUa_Good);
		}
		return status;
  }

  /* Note: never directly call this function. */

  UaStatus DMapping::writeCallReloadMappingFromFiles (const OpcUa_Boolean & v)
  {
	  getAddressSpaceLink()->setReloadMappingFromFilesError(METHOD_IN_PROGRESS,OpcUa_Good);
		UaStatus status = reloadMappingFromFiles();
		if (!status.isGood()){
			getAddressSpaceLink()->setReloadMappingFromFilesError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setReloadMappingFromFilesError(METHOD_GOOD,OpcUa_Good);
		}
		return status;
  }

  /* Note: never directly call this function. */

  UaStatus DMapping::writeCallSaveMappingUsed (const OpcUa_Boolean & v)
  {
	  getAddressSpaceLink()->setSaveMappingUsedError(METHOD_IN_PROGRESS,OpcUa_Good);
		UaStatus status = saveMappingUsed();
		if (!status.isGood()){
			getAddressSpaceLink()->setSaveMappingUsedError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setSaveMappingUsedError(METHOD_GOOD,OpcUa_Good);
		}
		return status;
  }

  UaStatus DMapping::callCheckMapping (OpcUa_Byte & result)
    {
	  UaStatus s = OpcUa_Bad;
	  	  UaClientSdk::UaSession *session=ClientSessionFactory::connect(getParent()->scaOpcUaEndpoints().c_str());
	  	 if (!session){
	  		 LOG(Log::ERR)<<"Bad communication Error";
	  		 return OpcUa_BadCommunicationError;
	  	 }
	  s= checkMapping (session ,result);
	  delete session;
	  	 return s;

    }

  UaStatus DMapping::callGenerateMapping ()
  {
	  UaStatus s = OpcUa_Bad;
	  UaClientSdk::UaSession *session=ClientSessionFactory::connect(getParent()->scaOpcUaEndpoints().c_str());
	 if (!session){
		 LOG(Log::ERR)<<"Bad communication Error";
		 return OpcUa_BadCommunicationError;
	 }
	 s= generateMapping (session);
	 delete session;
	 return s;

  }

  UaStatus DMapping::callGenerateMappingFiles ()
  {
	  return generateMappingFiles ();
  }

  UaStatus DMapping::callReadMappingFiber (OpcUa_Byte mode,
					   OpcUa_Byte fiberNb,
					   OpcUa_Byte & fiberLatome)
  {//mode #0 mapping used #1 mapping generated
	 return readMappingFiber (mode,fiberNb,fiberLatome);
  }
  UaStatus DMapping::callReadMappingSCUser (OpcUa_Byte mode,
				    OpcUa_Byte channelNb,
				    OpcUa_Byte & channelUserLatome,
				    OpcUa_Byte & posUserLatome){
	  return readMappingSCUser (mode,channelNb,channelUserLatome,posUserLatome);
  }

  UaStatus DMapping::callUpdateMappingUsed ()
  {
	  return updateMappingUsed ();

  }

  UaStatus DMapping::callReloadMappingFromFiles ()
  {
	  return reloadMappingFromFiles ();
  }

  UaStatus DMapping::callSaveMappingUsed ()
  {
    return saveMappingUsed ();
  }


  // 3333333333333333333333333333333333333333333333333333333333333333333333333
  // 3     FULLY CUSTOM CODE STARTS HERE                                     3
  // 3     Below you put bodies for custom methods defined for this class.   3
  // 3     You can do whatever you want, but please be decent.               3
  // 3333333333333333333333333333333333333333333333333333333333333333333333333


}
