#include <LtdbCommon.h>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <LogIt.h>
#include <DLtdbGBTx.h>
#include <DLtdbMTx.h>
#include <DLtdbMtrx.h>
#include <xml_parser.h>
using boost::property_tree::ptree;


void ltdbPrint(Ltdb ltdb){
  //LOG(Log::INF) << "--------Ltdb" << std::endl;
  for (int scaId =1; scaId<=5; scaId++){
	  //LOG(Log::INF) <<"----Sca " << scaId << std::endl;
    for (int mtxId =1; mtxId<=4; mtxId++){
		//modif ATLLARONL-1191 (all the LOG)
    	//LOG(Log::INF) << "  --Mtx " << mtxId << std::endl;
    	//LOG(Log::INF) << "    Register01: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register01 << std::dec << std::endl;
    	//LOG(Log::INF) << "    Register02: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register02 << std::dec << std::endl;
    	//LOG(Log::INF) << "    Register03: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register03 << std::dec << std::endl;
    	//LOG(Log::INF) << "    Register04: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register04 << std::dec << std::endl;
      if (ltdb.sca[scaId-1].mtx[mtxId-1].mode == Device::DLtdbMTx::DEGRADED_MODE){
    	  //LOG(Log::INF) << "    Mode:DEGRADED" << std::endl;
      }
      else if (ltdb.sca[scaId-1].mtx[mtxId-1].mode == Device::DLtdbMTx::ABSENT_MODE){
        //LOG(Log::INF) << "    Mode:ABSENT" << std::endl;
      }
      else{
    	  //LOG(Log::INF) << "    Mode: NORMAL" << std::endl;
      }
    }  
	LOG(Log::TRC) << "  --GBTx " << std::endl;
	if (ltdb.sca[scaId-1].gbtx.mode == Device::DLtdbGBTx::STABLE_SCA_MODE){
		LOG(Log::TRC) << "    Mode:STABLE_SCA_MODE" << std::endl;
	    LOG(Log::TRC) << "    Register35: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].gbtx.register35 << std::dec << std::endl;
    }
    else{
    	LOG(Log::TRC) << "    Mode: NORMAL" << std::endl;
		LOG(Log::TRC) << "    Default config used" << std::endl;
    }

    LOG(Log::TRC) << "  --Mtrx " << std::endl;
    LOG(Log::TRC) << "    Register01: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].mtrx.register01 << std::dec << std::endl;
    LOG(Log::TRC) << "    Register02: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].mtrx.register02 << std::dec << std::endl;
    for (int locxId =1; locxId<=4; locxId++){
    	//LOG(Log::INF) << "  --Locx " << locxId << std::endl;
    	if (ltdb.sca[scaId-1].locx2[locxId-1].mode == Device::DLtdbLOCx2::IGNORE_ERROR){
		  //LOG(Log::INF) << "    Mode:IGNORE_ERROR" << std::endl;
    	}
    	else if (ltdb.sca[scaId-1].locx2[locxId-1].mode == Device::DLtdbLOCx2::NO_READBACK_MODE){
    		//LOG(Log::INF) << "    Mode:NO_READBACK_MODE" << std::endl;
    	}
    	else{
    		//LOG(Log::INF) << "    Mode: NORMAL" << std::endl;
    	}
    	//LOG(Log::INF) << "    Register01: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].locx2[locxId-1].register01 << std::dec << std::endl;
    	//LOG(Log::INF) << "    Register02: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].locx2[locxId-1].register02 << std::dec << std::endl;
		for (int adcId =1; adcId<=4; adcId++){
			//LOG(Log::INF) << "    --adc " << adcId << std::endl;
			if (ltdb.sca[scaId-1].locx2[locxId-1].adc[adcId-1].mode == Device::DLtdbADCNevis::IGNORE_ERROR){
		        //LOG(Log::INF) << "    Mode:IGNORE_ERROR" << std::endl;
			}
				else if (ltdb.sca[scaId-1].locx2[locxId-1].adc[adcId-1].mode == Device::DLtdbADCNevis::NO_READ_BACK_MODE){
					//LOG(Log::INF) << "      Mode: NO READBACK" << std::endl;
				}
				else{
					//LOG(Log::INF) << "      Mode: NORMAL" << std::endl;
				}


			}
    }
  }

}

UaStatus generalXml(Ltdb &ltdb,std::string genConfFileName)
{
  int id;

  uint8_t defRegMtx[4];
  bool MtxDefined[4]={false,false,false,false};
	
  uint8_t defRegGbtx[1];
  bool GbtxDefined[1]={false};

  uint8_t defRegMtrx[2];
  bool MtrxDefined[2]={false,false};

  uint8_t defRegLocx[2];
  bool LocxDefined[2]={false,false};

  Device::DLtdbMTx::Mtx_Mode defModeMtx = Device::DLtdbMTx::NORMAL_MODE;
  bool ModeMtxDefined=false;

  Device::DLtdbLOCx2::Locx_Mode defModeLocx2=Device::DLtdbLOCx2::NORMAL_MODE;
  //bool ModeLOCxDefined=true;

  Device::DLtdbGBTx:: Gbtx_Mode defModeGbtx = Device::DLtdbGBTx::NORMAL_MODE;
  bool ModeGbtxDefined=false;

  Device::DLtdbMtrx::Mtrx_Mode defModeMtrx = Device::DLtdbMtrx::NORMAL_MODE;
  bool ModeMtrxDefined=false;

  Device::DLtdbADCNevis::Adc_Mode defModeAdc = Device::DLtdbADCNevis::NORMAL_MODE;
  bool ModeAdcDefined=false;

  ptree pt;
  try{
  	  read_xml(genConfFileName,pt);


	  for (ptree::iterator pos = pt.begin(); pos != pt.end(); pos++) {
		ptree &subtree = pos->second;
		for (ptree::iterator itDevice = subtree.begin(); itDevice != subtree.end(); itDevice++) {

		  if(itDevice->first == "mtx"){
			ptree &mtxtree = itDevice->second;
			if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="normal"){
			  defModeMtx=Device::DLtdbMTx::NORMAL_MODE;
			  ModeMtxDefined=true;
			}
			else if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="degraded"){
			  defModeMtx=Device::DLtdbMTx::DEGRADED_MODE;
			  ModeMtxDefined=true;
			}
			else if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="absent"){
				  defModeMtx=Device::DLtdbMTx::ABSENT_MODE;
				  ModeMtxDefined=true;
			}
			else {
				 LOG(Log::ERR) << "Error in General XML file, MTx mode unknow";
				 return OpcUa_Bad;
			}

			if (!ModeMtxDefined){
				LOG(Log::ERR) << "Error in General XML file, MTx mode not defined";
				return OpcUa_Bad;
			}
			for (ptree::iterator itMtx = mtxtree.begin(); itMtx != mtxtree.end(); itMtx++) {
			  if(itMtx->first == "register"){
				id=itMtx->second.get_child("<xmlattr>.id").get_value<int>();
				if ( id > 0 && id < 5  && !MtxDefined[id-1] ){
					defRegMtx[id-1]=(uint8_t) std::stoi(itMtx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
					MtxDefined[id-1]=true;
				}
				else{
					if ( id < 1 || id > 4 ){
						 LOG(Log::ERR) << "Error in General XML file, Mtx Register id must be between 1 and 4";
						 return OpcUa_Bad;
					}
					else{
						LOG(Log::ERR) << "Error in General XML file, Mtx Register "<< id <<" defined several times";
						return OpcUa_Bad;
					}
				}
			  }
			}
			if (!ModeMtxDefined){
				LOG(Log::ERR) << "Error in General XML file, MTx mode not defined";
				return OpcUa_Bad;
			}
			for (int i=0; i < 4 ; i++){
				if(!MtxDefined[i]){
					LOG(Log::ERR) << "Error in General XML file, Mtx Register "<< i+1 <<" not defined";
					return OpcUa_Bad;
				}
			}
		  }


			else if(itDevice->first == "gbtx"){
				ptree &gbtxtree = itDevice->second;
				if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="normal"){
					defModeGbtx=Device::DLtdbGBTx::NORMAL_MODE;
			  		ModeGbtxDefined=true;
				}
				else if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="stable"){
					defModeGbtx=Device::DLtdbGBTx::STABLE_SCA_MODE;
					ModeGbtxDefined=true;
				}
				else {
				 LOG(Log::ERR) << "Error in General XML file, GBTx mode unknow";
				 return OpcUa_Bad;
				}
				if (!ModeGbtxDefined){
					LOG(Log::ERR) << "Error in General XML file, GBTx mode not defined";
					return OpcUa_Bad;
				}
				for (ptree::iterator itGbtx = gbtxtree.begin(); itGbtx != gbtxtree.end(); itGbtx++) {
			  		if(itGbtx->first == "register"){
						id=itGbtx->second.get_child("<xmlattr>.id").get_value<int>();
						if ( id > 0 && id < 3  && !GbtxDefined[id-1] ){
							defRegGbtx[id-1]=(uint8_t) std::stoi(itGbtx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
							GbtxDefined[id-1]=true;
						}
						else{
							if ( id < 1 || id > 2){
								LOG(Log::ERR) << "Error in General XML file, GBTx Register id must be between 1 and 2";
						 		return OpcUa_Bad;
							}
							else{
								LOG(Log::ERR) << "Error in General XML file, GBTx Register "<< id <<" defined several times";
								return OpcUa_Bad;
							}
						}
					}
				}
				for (int i=0; i < 2 ; i++){
					if(!GbtxDefined[i]){
						LOG(Log::ERR) << "Error in General XML file, GBTx Register "<< i+1 <<" not defined";
						return OpcUa_Bad;
					}
				}
			}

		  else if(itDevice->first == "mtrx"){
			ptree &mtrxtree = itDevice->second;
			if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="normal"){
			  defModeMtrx=Device::DLtdbMtrx::NORMAL_MODE;
			  ModeMtrxDefined=true;
			}
			else if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="degraded"){
			  defModeMtrx=Device::DLtdbMtrx::DEGRADED_MODE;
			  ModeMtrxDefined=true;
			}
			else {
				 LOG(Log::ERR) << "Error in General XML file, Mtrx mode unknow";
				 return OpcUa_Bad;
			}

			if (!ModeMtrxDefined){
				LOG(Log::ERR) << "Error in General XML file, MTx mode not defined";
				return OpcUa_Bad;
			}
			for (ptree::iterator itMtrx = mtrxtree.begin(); itMtrx != mtrxtree.end(); itMtrx++) {
			  if(itMtrx->first == "register"){
				id=itMtrx->second.get_child("<xmlattr>.id").get_value<int>();
				if ( id > 0 && id < 3  && !MtrxDefined[id-1] ){
					defRegMtrx[id-1]=(uint8_t) std::stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
					MtrxDefined[id-1]=true;
				}
				else{
					if ( id < 1 || id > 2){
						 LOG(Log::ERR) << "Error in General XML file, Mtrx Register id must be between 1 and 2";
						 return OpcUa_Bad;
					}
					else{
						LOG(Log::ERR) << "Error in General XML file, Mtrx Register "<< id <<" defined several times";
						return OpcUa_Bad;
					}
				}

			  }
			}
			for (int i=0; i < 2 ; i++){
				if(!MtrxDefined[i]){
					LOG(Log::ERR) << "Error in General XML file, Mtrx Register "<< i+1 <<" not defined";
					return OpcUa_Bad;
				}
			}

		  }
		  else if(itDevice->first == "locx2"){
			ptree &locxtree = itDevice->second;
			for (ptree::iterator itLocx = locxtree.begin(); itLocx != locxtree.end(); itLocx++) {
			  if(itLocx->first == "register"){
				id=itLocx->second.get_child("<xmlattr>.id").get_value<int>();
				if ( id > 0 && id < 3  && !LocxDefined[id-1] ){
					defRegLocx[id-1]=(uint8_t) std::stoi(itLocx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
					LocxDefined[id-1]=true;
				}
				else{
					if ( id < 1 || id > 2){
						 LOG(Log::ERR) << "Error in General XML file, Locx2 Register id must be between 1 and 2";
						 return OpcUa_Bad;
					}
					else{
						LOG(Log::ERR) << "Error in General XML file, Locx2 Register "<< id <<" defined several times";
						return OpcUa_Bad;
					}
				}
			  }
			}
			for (int i=0; i < 2 ; i++){
				if(!LocxDefined[i]){
					LOG(Log::ERR) << "Error in General XML file, Locx2 Register "<< i+1 <<" not defined";
					return OpcUa_Bad;
				}
			}
		  }
		  else if(itDevice->first == "adc"){
			//ptree &adctree = itDevice->second;
			if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="normal"){
			  defModeAdc=Device::DLtdbADCNevis::NORMAL_MODE;
			  ModeAdcDefined=true;
			}
			else if(itDevice->second.get_child("<xmlattr>.mode").get_value<std::string>() =="no_readback"){
				defModeAdc=Device::DLtdbADCNevis::NO_READ_BACK_MODE;
				ModeAdcDefined=true;
			}
			else {
				 LOG(Log::ERR) << "Error in General XML file, ADC mode unknow";
				 return OpcUa_Bad;
			}

			if (!ModeAdcDefined){
				LOG(Log::ERR) << "Error in General XML file, ADC mode not defined";
				return OpcUa_Bad;
			}



		  }
		}
	  }
  }catch (const std::exception& e) {
        LOG (Log::ERR) <<"Caught:"<<e.what();
        return OpcUa_Bad;
      }

  for (int scaId =1; scaId<=5; scaId++){
    for (int mtxId =1; mtxId<=4; mtxId++){
      ltdb.sca[scaId-1].mtx[mtxId-1].mode = defModeMtx;
      ltdb.sca[scaId-1].mtx[mtxId-1].register01 = defRegMtx[0];
      ltdb.sca[scaId-1].mtx[mtxId-1].register02 = defRegMtx[1];
      ltdb.sca[scaId-1].mtx[mtxId-1].register03 = defRegMtx[2];
      ltdb.sca[scaId-1].mtx[mtxId-1].register04 = defRegMtx[3];
    }
    ltdb.sca[scaId-1].gbtx.mode = defModeGbtx;
	ltdb.sca[scaId-1].gbtx.register35 = defRegGbtx[0];

    ltdb.sca[scaId-1].mtrx.mode = defModeMtrx;
    ltdb.sca[scaId-1].mtrx.register01 = defRegMtrx[0];
    ltdb.sca[scaId-1].mtrx.register02 = defRegMtrx[1];
    for (int locxId =1; locxId<=4; locxId++){
      ltdb.sca[scaId-1].locx2[locxId-1].mode = defModeLocx2;
      ltdb.sca[scaId-1].locx2[locxId-1].register01 = defRegLocx[0];
      ltdb.sca[scaId-1].locx2[locxId-1].register02 = defRegLocx[1];
      for (int adcId =1; adcId<=4; adcId++){
		ltdb.sca[scaId-1].locx2[locxId-1].adc[adcId-1].mode = defModeAdc;
	  }
    }
  }
  return OpcUa_Good;
  }

UaStatus specificXml(Ltdb &ltdb,std::string specConfFileName){
  ptree pt;
  try{
	  read_xml(specConfFileName,pt);
  }catch (const std::exception& e) {
    LOG (Log::ERR) <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  for (ptree::iterator pos = pt.begin(); pos != pt.end(); pos++) {
    //// cout << pos->first <<"::" << std::endl;
    ptree &subtree = pos->second;
    for (ptree::iterator itSca = subtree.begin(); itSca != subtree.end(); itSca++) {
      if(itSca->first == "sca"){
        ptree &scatree = itSca->second;
        int scaId=itSca->second.get_child("<xmlattr>.id").get_value<int>();
        //// cout << "   "<< itSca->first << ":: id: " << scaId << std::endl;
      for (ptree::iterator itDevice = scatree.begin(); itDevice != scatree.end(); itDevice++) {
        if(itDevice->first == "mtx"){
          int mtxId=itDevice->second.get_child("<xmlattr>.id").get_value<int>();
         // // cout << "   "<< itDevice->first << ":: id: " << mtxId <<std::endl;
          ptree &mtxtree = itDevice->second;
          for (ptree::iterator itMtx = mtxtree.begin(); itMtx != mtxtree.end(); itMtx++) {
            if(itMtx->first == "register"){
             // // cout << "   "<< itMtx->first << std::endl;
              // cout << "   "<< itMtx->first << ":: id: " << itMtx->second.get_child("<xmlattr>.id").get_value<int>()<< std::endl;
              // cout << "   "<< itMtx->first << ":: value:0x "<< std::hex  << std::(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< std::dec << std::endl;
              int registerId=itMtx->second.get_child("<xmlattr>.id").get_value<int>();
              switch (registerId){
                case 1:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register01 = (uint8_t) std::stoi(itMtx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                break;
                case 2:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register02 = (uint8_t) std::stoi(itMtx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                break;
                case 3:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register03 = (uint8_t) std::stoi(itMtx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                break;
                case 4:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register04 = (uint8_t) std::stoi(itMtx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                  break;
              }  
            }
            else if(itMtx->first == "<xmlattr>"){
              ptree &attrtree = itMtx->second;
              for (ptree::iterator itAttr = attrtree.begin(); itAttr != attrtree.end(); itAttr++) {
                if (itAttr->first == "mode"){
                  // cout <<"   " << itDevice->first << ":: mode: "<< itAttr->second.data() << std::endl;
                  if(itAttr->second.get<std::string>("") =="degraded"){
                    //  read_xml(genConfFileName,pt);cout << "Sca " << scaId << " mtx " << mtxId << " : degrated"<< std::endl;
                    ltdb.sca[scaId-1].mtx[mtxId-1].mode = Device::DLtdbMTx::DEGRADED_MODE;
                    
                  }
                  else if (itAttr->second.get<std::string>("") =="absent"){
                	  ltdb.sca[scaId-1].mtx[mtxId-1].mode = Device::DLtdbMTx::ABSENT_MODE;
                  }
                  else if(itAttr->second.get<std::string>("") =="normal"){
                    // cout << "Sca " << scaId << " mtx " << mtxId << " : normal"<< std::endl;
                    ltdb.sca[scaId-1].mtx[mtxId-1].mode = Device::DLtdbMTx::NORMAL_MODE;
                  }

                }
              }
            }
          }
        }

		else if(itDevice->first == "gbtx"){
			ptree &gbtxtree = itDevice->second;
			for(ptree::iterator itGbtx = gbtxtree.begin(); itGbtx != gbtxtree.end(); itGbtx++){
				if(itGbtx->first == "register"){
					int registerId=itGbtx->second.get_child("<xmlattr>.id").get_value<int>();
					switch (registerId){
                		case 1:
                  			ltdb.sca[scaId-1].gbtx.register35 = (uint8_t) std::stoi(itGbtx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                		break;
					}
				}            
				else if(itGbtx->first == "<xmlattr>"){
					ptree &attrtree = itGbtx->second;
					for (ptree::iterator itAttr = attrtree.begin(); itAttr != attrtree.end(); itAttr++) {
						if (itAttr->first == "mode"){
							if(itAttr->second.get<std::string>("") =="stable"){
								ltdb.sca[scaId-1].gbtx.mode = Device::DLtdbGBTx::STABLE_SCA_MODE;
							}
							else if(itAttr->second.get<std::string>("") =="normal"){
								ltdb.sca[scaId-1].gbtx.mode = Device::DLtdbGBTx::NORMAL_MODE;
							}
						}
					}
				}
			}
		}

        else if(itDevice->first == "mtrx"){
         // int mtrxId = itDevice->second.get_child("<xmlattr>.id").get_value<int>();
          // cout << "   "<< itDevice->first << ":: id: " << mtrxId <<std::endl;

          ptree &mtrxtree = itDevice->second;
          for (ptree::iterator itMtrx = mtrxtree.begin(); itMtrx != mtrxtree.end(); itMtrx++) {
            if(itMtrx->first == "register"){
              // cout << "   "<< itMtrx->first << std::endl;
              // cout << "   "<< itMtrx->first << ":: id: " << itMtrx->second.get_child("<xmlattr>.id").get_value<int>()<< std::endl;
              // cout << "   "<< itMtrx->first << ":: value:0x "<< std::hex  << std::(itMtrx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< std::dec << std::endl;
              int registerId=itMtrx->second.get_child("<xmlattr>.id").get_value<int>();
              switch (registerId){
                case 1:
                  ltdb.sca[scaId-1].mtrx.register01 = (uint8_t) std::stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                break;
                case 2:
                  ltdb.sca[scaId-1].mtrx.register02 = (uint8_t) std::stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                break;
              }
            }
            else if(itMtrx->first == "<xmlattr>"){
			  ptree &attrtree = itMtrx->second;
			  for (ptree::iterator itAttr = attrtree.begin(); itAttr != attrtree.end(); itAttr++) {
				if (itAttr->first == "mode"){
				  // cout <<"   " << itDevice->first << ":: mode: "<< itAttr->second.data() << std::endl;
				  if(itAttr->second.get<std::string>("") =="degraded"){
					//  read_xml(genConfFileName,pt);cout << "Sca " << scaId << " mtx " << mtxId << " : degrated"<< std::endl;
					ltdb.sca[scaId-1].mtrx.mode = Device::DLtdbMtrx::DEGRADED_MODE;

				  }
				  else if(itAttr->second.get<std::string>("") =="normal"){
					// cout << "Sca " << scaId << " mtx " << mtxId << " : normal"<< std::endl;
					ltdb.sca[scaId-1].mtrx.mode = Device::DLtdbMtrx::NORMAL_MODE;
				  }

				}
			  }
			}
          }
        }
        else if(itDevice->first == "locx2"){
          int locxId = itDevice->second.get_child("<xmlattr>.id").get_value<int>();
          // cout << "   "<< itDevice->first << ":: id: " << locxId <<std::endl;
          ptree &locxtree = itDevice->second;
          for (ptree::iterator itLocx = locxtree.begin(); itLocx != locxtree.end(); itLocx++) {
        	  if(itLocx->first == "<xmlattr>"){
				  ptree &attrtree = itLocx->second;
				  for (ptree::iterator itAttr = attrtree.begin(); itAttr != attrtree.end(); itAttr++) {
					if (itAttr->first == "mode"){
					  // cout <<"   " << itDevice->first << ":: mode: "<< itAttr->second.data() << std::endl;
					  if(itAttr->second.get<std::string>("") =="noreadback"){
						//  read_xml(genConfFileName,pt);cout << "Sca " << scaId << " mtx " << mtxId << " : degrated"<< std::endl;
						ltdb.sca[scaId-1].locx2[locxId-1].mode = Device::DLtdbLOCx2::NO_READBACK_MODE;
					  }
					  else if(itAttr->second.get<std::string>("") =="ignoreerror"){
						//  read_xml(genConfFileName,pt);cout << "Sca " << scaId << " mtx " << mtxId << " : degrated"<< std::endl;
						ltdb.sca[scaId-1].locx2[locxId-1].mode = Device::DLtdbLOCx2::IGNORE_ERROR;
					  }
					  else if(itAttr->second.get<std::string>("") =="normal"){
						// cout << "Sca " << scaId << " mtx " << mtxId << " : normal"<< std::endl;
						ltdb.sca[scaId-1].locx2[locxId-1].mode = Device::DLtdbLOCx2::NORMAL_MODE;
					  }

					}
				  }
				}
        	  else if(itLocx->first == "register"){
              // cout << "   "<< itLocx->first << std::endl;
              // cout << "   "<< itLocx->first << ":: id: " << itLocx->second.get_child("<xmlattr>.id").get_value<int>()<< std::endl;
              // cout << "   "<< itLocx->first << ":: value:0x "<< std::hex  << std::(itLocx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< std::dec << std::endl;
              int registerId=itLocx->second.get_child("<xmlattr>.id").get_value<int>();
              switch (registerId){
                case 1:
                  ltdb.sca[scaId-1].locx2[locxId-1].register01 = (uint8_t) std::stoi(itLocx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                break;
                case 2:
                  ltdb.sca[scaId-1].locx2[locxId-1].register02 = (uint8_t) std::stoi(itLocx->second.get_child("<xmlattr>.value").get_value<std::string>(),nullptr,0);
                break;
              }
            }
            else if(itLocx->first == "adc"){
            	  int adcId = itLocx->second.get_child("<xmlattr>.id").get_value<int>();
            	  ptree &adctree = itLocx->second;
            	  for (ptree::iterator itAdc = adctree.begin(); itAdc != adctree.end(); itAdc++) {
            	  if(itAdc->first == "<xmlattr>"){
            	  	 ptree &attrtree = itAdc->second;
					  for (ptree::iterator itAttr = attrtree.begin(); itAttr != attrtree.end(); itAttr++) {
						if (itAttr->first == "mode"){
						  // cout <<"   " << itDevice->first << ":: mode: "<< itAttr->second.data() << std::endl;
						  if(itAttr->second.get<std::string>("") =="normal"){
							//  read_xml(genConfFileName,pt);cout << "Sca " << scaId << " mtx " << mtxId << " : degrated"<< std::endl;
							ltdb.sca[scaId-1].locx2[locxId-1].adc[adcId-1].mode = Device::DLtdbADCNevis::NORMAL_MODE;
						  }
						  else if(itAttr->second.get<std::string>("") =="ignoreerror"){
						      ltdb.sca[scaId-1].locx2[locxId-1].adc[adcId-1].mode = Device::DLtdbADCNevis::IGNORE_ERROR;
					      }
						  else if(itAttr->second.get<std::string>("") =="no_readback"){
							// cout << "Sca " << scaId << " mtx " << mtxId << " : normal"<< std::endl;
							  ltdb.sca[scaId-1].locx2[locxId-1].adc[adcId-1].mode = Device::DLtdbADCNevis::NO_READ_BACK_MODE;
						  }

						}
					  }
            	  }

            }
          }
        }
      }
    }
  }           //  // cout << pos->get<string>("<xmlattr>.name");
  }



  }
  return OpcUa_Good;
}

