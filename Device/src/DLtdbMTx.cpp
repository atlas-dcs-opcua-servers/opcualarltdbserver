
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt) 
    on 2019-01-11T09:01:46.895+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */




#include <Configuration.hxx>

#include <DLtdbMTx.h>
#include <ASLtdbMTx.h>

#include <iostream>
#include <string>
#include <memory>
#include <LogIt.h>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>
#include <LtdbCommon.h>
namespace Device
{




  // 1111111111111111111111111111111111111111111111111111111111111111111111111
  // 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
  // 1     Users don't modify this code!!!!                                  1
  // 1     If you modify this code you may start a fire or a flood somewhere,1
  // 1     and some human being may possible cease to exist. You don't want  1
  // 1     to be charged with that!                                          1 
  // 1111111111111111111111111111111111111111111111111111111111111111111111111






  // 2222222222222222222222222222222222222222222222222222222222222222222222222
  // 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
  // 2     (code for which only stubs were generated automatically)          2
  // 2     You should add the implementation but dont alter the headers      2
  // 2     (apart from constructor, in which you should complete initializati2
  // 2     on list)                                                          2 
  // 2222222222222222222222222222222222222222222222222222222222222222222222222

  /* sample ctr */
  DLtdbMTx::DLtdbMTx (const Configuration::LtdbMTx & config,
		      Parent_DLtdbMTx * parent):Base_DLtdbMTx (config, parent)
    /* fill up constructor initialization list here */
  {

	  int MtxId = std::stoi(id());
    /* fill up constructor body here */

	  if(MtxId==1 || MtxId == 3){
		  baseAddress=0x4;
	  }
	  else if(MtxId==2 || MtxId == 4){
		  baseAddress=0x8;
	  }
	  if(MtxId==1 || MtxId == 2){
		  busNumber=4;
	  }
	  else if(MtxId==3 || MtxId == 4){
		  busNumber=6;
	  }


	  logID=getParent()->logID + "MTx_" + id() + ": ";

		default_value[0]=0;
		default_value[1]=0;
		default_value[2]=0;
		default_value[3]=0;
		mtx_default_mode=NORMAL_MODE;

		LOG(Log::INF)<<"For the LtdbMTx("<<MtxId<<") the base Address is: "<< std::hex<< (int)getBaseAddress() <<"and the bus number is :"<<(int)getBusNumber();
  }

  /* sample dtr */
  DLtdbMTx::~DLtdbMTx ()
  {
  }

  // 3333333333333333333333333333333333333333333333333333333333333333333333333
  // 3     FULLY CUSTOM CODE STARTS HERE                                     3
  // 3     Below you put bodies for custom methods defined for this class.   3
  // 3     You can do whatever you want, but please be decent.               3
  // 3333333333333333333333333333333333333333333333333333333333333333333333333






}
