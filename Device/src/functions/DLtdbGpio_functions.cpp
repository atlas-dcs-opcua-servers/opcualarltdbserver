/**
 * \file DLtdbGpio_functions.cpp
 * \brief Gpio control.
 * \author Etienne FORTIN
 *
 * Functions to control the gpio of the SCA.
 *
 *
 */
#include <Configuration.hxx>

#include <DLtdbGpio.h>
#include <ASLtdbGpio.h>
#include <LogIt.h>
#include <DigitalIO.h>
#include <ClientSessionFactory.h>
#include <sstream>
#include <string>
#include <iomanip>

#include <LtdbCommon.h>

#include <UaoClientForOpcUaScaUaoExceptions.h>
namespace Device {

/**
 * \fn UaStatus DLtdbGpio::configureADC (UaClientSdk::UaSession* session)
 * \brief Function to configure all ADC components
 * \param session opened session connected to OpcUaSca
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::configureADC (UaClientSdk::UaSession* session) {
  UaStatus s = OpcUa_Bad;
  s = resetADC (session);
  if (!s.isGood()) {
    return s;
  }
  return OpcUa_Good;

}
/**
 * \fn UaStatus DLtdbGpio::resetADC (UaClientSdk::UaSession* session)
 * \brief Function to reset all ADC components
 * \param session opened session connected to OpcUaSca
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::resetADC (UaClientSdk::UaSession* session) {
  UaStatus s = OpcUa_Bad;
  s = setValuetoAllGpioADC (1,session);
  if (!s.isGood()) {
    return s;
  }
  s = setValuetoAllGpioADC (0,session);
  if (!s.isGood()) {
    return s;
  }
  s = setValuetoAllGpioADC (1,session);
  if (!s.isGood()) {
    return s;
  }
  return s;
}
/**
 * \fn UaStatus DLtdbGpio::setValuetoAllGpioADC (OpcUa_Boolean value,UaClientSdk::UaSession* session)
 * \brief Function to write the same value in all ADC-RST output of SCA
 * \param session opened session connected to OpcUaSca
 * \param value the value wanted
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::setValuetoAllGpioADC (OpcUa_Boolean value,UaClientSdk::UaSession* session) {
  std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();
  UaStatus s;

    std::string nodeId;
    for (int i=1; i<5; i++) {
      for (int j=1; j<5; j++) {
        nodeId = baseNodeId + ".gpio.RSTB_ADCL"+tostr (i)+"A"+tostr (j);
        UaoClientForOpcUaSca::DigitalIO Adcl (session,UaNodeId (nodeId.c_str(),2));
        s=secureGpioWrite( Adcl , value , nodeId, logID,5);
        if (!s.isGood()){LOG (Log::ERR)<< logID<< "setValuetoAllGpioADC FALURE"; return s;}
      }

    }

  return OpcUa_Good;

}
/**
 * \fn UaStatus DLtdbGpio::resetGBTx (UaClientSdk::UaSession* session)
 * \brief Function to reset the GBTx (of SCA+1) components
 * \param session opened session connected to OpcUaSca
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::resetGBTx (UaClientSdk::UaSession* session) {
  OpcUa_Boolean btrue=1;
  OpcUa_Boolean bfalse=0;

  std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();
  try {
    std::string nodeId;
    nodeId=baseNodeId + ".gpio.GBTxRST";
    UaoClientForOpcUaSca::DigitalIO gbtx (session,UaNodeId (nodeId.c_str(),2));
    gbtx.writeValue (btrue);
    boost::this_thread::sleep_for (boost::chrono::milliseconds (100));
    gbtx.writeValue (bfalse);
    getParent()->getParent()->ASLOG ("GBTx reset succes! (" + baseNodeId + ")");

    /*nodeId=baseNodeId + ".gpio.GBTxCFGSEL";
    UaoClientForOpcUaSca::DigitalIO gbtxCfg (session,UaNodeId (nodeId.c_str(),2));
    gbtxCfg.writeValue (btrue);*/
  }
  catch (const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e) {
    LOG (Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    getParent()->getParent()->ASLOG ("GBTx reset failed! (" + baseNodeId + ")");
    return e.statusCode();
  }
  catch (const std::exception& e) {
    LOG (Log::ERR) << logID <<"Caught:"<<e.what();
    getParent()->getParent()->ASLOG ("GBTx reset failed! (" + baseNodeId + ")");
    return OpcUa_Bad;
  }
  return OpcUa_Good;

}
/**
 * \fn UaStatus DLtdbGpio::resetMTx (UaClientSdk::UaSession* session)
 * \brief Function to reset all MTx components
 * \param session opened session connected to OpcUaSca
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::resetMTx (UaClientSdk::UaSession* session) {
  OpcUa_Boolean btrue=1;
  OpcUa_Boolean bfalse=0;
  std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();
  UaStatus s=OpcUa_Bad;
  std::string nodeId;


   nodeId=baseNodeId + ".gpio.MTX-RST";
  UaoClientForOpcUaSca::DigitalIO mtx (session,UaNodeId (nodeId.c_str(),2));
  s=secureGpioWrite( mtx , btrue , nodeId, logID,5);
  if (!s.isGood()){ LOG (Log::ERR)<< logID<< "Reset MTX FAILURE";return s;}
  boost::this_thread::sleep_for (boost::chrono::milliseconds (100));
  s=secureGpioWrite( mtx , bfalse , nodeId, logID,5);
  if (!s.isGood()){ LOG (Log::ERR)<< logID<< "resetMTX FAILURE";return s;}

  return OpcUa_Good;

}
/**
 * \fn UaStatus DLtdbGpio::resetMTRx (UaClientSdk::UaSession* session)
 * \brief Function to reset all MTRx components
 * \param session opened session connected to OpcUaSca
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::resetMTRx (UaClientSdk::UaSession* session) {
  OpcUa_Boolean btrue=1;
  OpcUa_Boolean bfalse=0;
  std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();
  std::string nodeId;
  UaStatus s;
  nodeId=baseNodeId + ".gpio.MTRX-RST";
  UaoClientForOpcUaSca::DigitalIO mtrx (session,UaNodeId (nodeId.c_str(),2));

  s=secureGpioWrite( mtrx , btrue , nodeId, logID,5);
  if (!s.isGood()){ LOG (Log::ERR) << logID << "resetMTRX FAILURE";return s;}
  boost::this_thread::sleep_for (boost::chrono::milliseconds (100));
  s=secureGpioWrite( mtrx , bfalse , nodeId, logID,5);
  if (!s.isGood()){ LOG (Log::ERR) << logID << "resetMTRX FAILURE";return s;}

  return OpcUa_Good;

}
/**
 * \fn UaStatus DLtdbGpio::resetLOCx (UaClientSdk::UaSession* session)
 * \brief Function to reset all Locx2 components
 * \param session opened session connected to OpcUaSca
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::resetLOCx (UaClientSdk::UaSession* session) {
  OpcUa_Boolean btrue=1;
  OpcUa_Boolean bfalse=0;
  std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();

  std::string  nodeId=baseNodeId + ".gpio.LOCx2-RST";
   UaoClientForOpcUaSca::DigitalIO locx (session,UaNodeId (nodeId.c_str(),2));
   UaStatus s;
   s=secureGpioWrite( locx , btrue , nodeId, logID,5);
     if (!s.isGood()){ LOG (Log::ERR)<< logID<< "resetLOCX FAILURE";return s;}
     boost::this_thread::sleep_for (boost::chrono::milliseconds (100));
     s=secureGpioWrite( locx , bfalse , nodeId, logID,5);
     if (!s.isGood()){LOG (Log::ERR)<< logID<< "resetLOCX FAILURE"; return s;}
  return OpcUa_Good;

}
/**
 * \fn UaStatus DLtdbGpio::power (OpcUa_Boolean value,UaClientSdk::UaSession* session)
 * \brief Function to power on/off the board
 *
 * \param value 1 for powerOn, 0 for powerOff
 * \param session opened session connected to OpcUaSca
 * \return UaStatus code OpcUa_Good if all is good
 */
UaStatus DLtdbGpio::power (OpcUa_Boolean value,UaClientSdk::UaSession* session) { //1 for powerOn 0 for powerOff
  OpcUa_Boolean valueInv=!value;

  std::string nodeId;
  std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();
  UaStatus s;
  try {
    for (int i=1; i <= 5 ; i++) {
      nodeId=baseNodeId + ".gpio.Ctrl"+tostr (i);
      UaoClientForOpcUaSca::DigitalIO gpioctrl= UaoClientForOpcUaSca::DigitalIO (session,UaNodeId (nodeId.c_str(),2));
      s=secureGpioWrite( gpioctrl , valueInv , nodeId, logID,5);
	  if (!s.isGood()){
		  LOG (Log::ERR)<< logID<< "Power failed during Ctrl " << i;
		  return s;
	  }

    }
  } catch (const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e) {
    LOG (Log::ERR) << logID <<"Caught(BadStatusCode):"<<e.what();
    return e.statusCode();
  } catch (const std::exception& e) {
    LOG (Log::ERR) << logID <<"Caught:"<<e.what();
    return OpcUa_Bad;
  }
  return OpcUa_Good;

}
}

