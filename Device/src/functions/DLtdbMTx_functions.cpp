#include <Configuration.hxx>

#include <DLtdbMTx.h>
#include <ASLtdbMTx.h>

#include <iostream>
#include <string>
#include <memory>
#include <LogIt.h>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>
#include <LtdbCommon.h>
#include <UaoClientForOpcUaScaUaoExceptions.h>
namespace Device
{
UaStatus DLtdbMTx::configure (UaClientSdk::UaSession* session)
{
    UaStatus s,allGood=OpcUa_Good;

    std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();
    try
    {
        if (mtx_default_mode==DEGRADED_MODE)
        {
            //On Degrated mode the configuration of this MTx depend only of the reset
            LOG(Log::INF)  << logID << " This MTX is running on degrated mode: Config ignored";
            return OpcUa_Good;
        }
        else if (mtx_default_mode==ABSENT_MODE)
        {
            //On absent mode the configuration ignore this MTx
            LOG(Log::TRC) << logID   << " This MTX is running on absent mode: Config ignored";
            return OpcUa_Good;
        }
        else{
        	LOG(Log::TRC) << logID  << " Configuration";
        	for (int i=0; i<4; i++){
				    //Creation of the node
				    std::string nodeId =baseNodeId + ".BusI2C_" + tostr(busNumber) +".register0" + tostr(i+baseAddress);
				    UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
				    s=secureI2CWrite( registerId, default_value[i],"Register " + std::to_string(i+1),5,0,stats());
				
				    if (!s.isGood()){allGood=OpcUa_Bad;}

			    }
        	if(!allGood.isGood()){
        		LOG(Log::WRN)  << logID << ": Configuration failed and depend only on reset ";
        		getParent()->getParent()->ASLOG("Warning : MTx "+id() + "of Sca " + getParent()->id() + ": Configuration failed and depend only on reset ");
        	}
        	return OpcUa_Good;
        }
    }
    catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
    {
        LOG(Log::ERR)  << logID << "Caught(BadStatusCode):" << e.what();
        return e.statusCode();
    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR)  << logID << "Caught:" << e.what();
        return OpcUa_Bad;
    }
    return s;
}

std::string DLtdbMTx::statusString(UaClientSdk::UaSession* session){
	std::string baseNodeId="ltdb_"+getParent()->getParent()->id()+".scaFelix"+getParent()->id();
	std::string ret;
	ret+="-----Mtx " + id() +"\n";
	if(mtx_default_mode==DEGRADED_MODE){
		ret+="Mode: DEGRADED\n";
	}
	else if(mtx_default_mode==ABSENT_MODE){
		ret+="Mode: ABSENT\n";
	}
	else{
		ret+="Mode: NORMAL\n";
	}
	for (int i=0; i<4; i++)
	{
		std::string nodeId =baseNodeId+".BusI2C_" + tostr(busNumber) +".register0" + tostr(i+baseAddress);
		UaoClientForOpcUaSca::I2cSlave registerId(session,UaNodeId(nodeId.c_str(),2));
		try{
			UaByteString readBs(registerId.readValue());
			if (readBs.length()!=1)
			{
				ret+="ERR: Register  " + std::to_string(i+1) + ": Error during reading\n";
				ret+="Default: "+ std::to_string((int) default_value[i]) + "\n";
			}
			if(default_value[i]!=readBs.data()[0])
			{
				ret+="ERR: Register " + std::to_string(i+1) +": Error readed value don't correspond to default\n";
				ret+="Default: "+ std::to_string((int) default_value[i]) + " Readed: "+ std::to_string((int) readBs.data()[0])+"\n";
			}
			else{
				ret+=" Register " + std::to_string(i+1) + ": \n";
				ret+="Default: "+ std::to_string((int) default_value[i]) + " Readed: "+ std::to_string((int) readBs.data()[0])+"\n";
			}
		 } catch (const std::exception& e) {
			 if(mtx_default_mode==DEGRADED_MODE){
				 	 ret+="Register " + std::to_string(i+1) + "Exception during reading:"+e.what() +"\n";
				 			 	ret+="Default: "+ std::to_string((int) default_value[i]) + "\n";
			 	}
			 if(mtx_default_mode==ABSENT_MODE){
				 	 ret+="Register " + std::to_string(i+1) + "Exception during reading:"+e.what() +"\n";
				 			 	ret+="Default: "+ std::to_string((int) default_value[i]) + "\n";
			 	}
			 	else{
			 		ret+="ERR: Register " + std::to_string(i+1) + "Exception during reading:"+e.what() +"\n";
			 					 	ret+="Default: "+ std::to_string((int) default_value[i]) + "\n";
			 	}


		  }

	}
	return ret;


}

OpcUa_Byte DLtdbMTx::getBusNumber() const
{
    return busNumber;
}
OpcUa_Byte DLtdbMTx::getBaseAddress() const
{
    return baseAddress;
}

UaStatus DLtdbMTx::initValue (Mtx_Mode defMode,OpcUa_Byte reg1,OpcUa_Byte reg2,OpcUa_Byte reg3,OpcUa_Byte reg4){
	default_value[0]=reg1;
	default_value[1]=reg2;
	default_value[2]=reg3;
	default_value[3]=reg4;
	mtx_default_mode=defMode;
	return OpcUa_Good;
}




}
