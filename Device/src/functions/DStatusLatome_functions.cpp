/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt)
    on 2019-01-30T17:33:45.421+01:00

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.



 */




#include <Configuration.hxx>

#include <DStatusLatome.h>
#include <ASStatusLatome.h>
#include <DLtdbSca.h>
#include <DLtdbLOCx2.h>
#include <DLtdbFiber.h>
#include <LtdbCommon.h>
#include <DMapping.h>
#include <NodeId.h>
#include <string>
#include "uhal/uhal.hpp"
#include <boost/thread/thread.hpp>
using namespace uhal;
#include <fstream>
#include <iostream>
#include <bitset>
namespace Device {

UaStatus DStatusLatome::resetAllTransceiver(){

  if(!connectHardware()){
    LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
    return OpcUa_Bad;
  }

  std::map<int, HwInterface*>::iterator itr;
  try {
        uhal::setLogLevelTo( uhal::Error() );

        for (itr = interface.begin() ; itr != interface.end() ; itr++) {
            itr->second->getNode ( NodeId::resetTransceiverBlock1 ).write(1);
            itr->second->getNode ( NodeId::resetTransceiverBlock2 ).write(1);
            itr->second->getNode ( NodeId::resetTransceiverBlock3 ).write(1);
            itr->second->getNode ( NodeId::resetTransceiverBlock4 ).write(1);
            itr->second->dispatch();
            itr->second->getNode ( NodeId::resetTransceiverBlock3 ).write(0);
            itr->second->getNode ( NodeId::resetTransceiverBlock1 ).write(0);
            itr->second->getNode ( NodeId::resetTransceiverBlock2 ).write(0);
            itr->second->getNode ( NodeId::resetTransceiverBlock4 ).write(0);
            itr->second->dispatch();

        }

    } catch(const std::exception& e) {
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }


  return OpcUa_Good;

}

UaStatus DStatusLatome::readChannelReady (OpcUa_UInt64 & chReady) {
    std::bitset<64>* bitSet;
    OpcUa_UInt64 chReadyReordered=0;
    OpcUa_UInt64 mask;
    if(!connectHardware()){
    		LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
    		return OpcUa_Bad;
    	}

    std::map<int, HwInterface*>::iterator itr;
    std::map<int, OpcUa_UInt64> chReadyMap;
    std::map<int, OpcUa_UInt64>::iterator itrChReady;

    std::map<int, OpcUa_UInt64> maskMap;
    std::map<int, OpcUa_UInt64>::iterator itrMaskMap;

    // Read the channel ready for all latomes connected to this ltdb
    try {
        uhal::setLogLevelTo( uhal::Error() );

        for (itr = interface.begin() ; itr != interface.end() ; itr++) {
		    itr->second->getNode (NodeId::global_load_clear ).write(1);
		    itr->second->getNode (NodeId::global_load_clear ).write(0);
            itr->second->dispatch();
            ValWord< uint32_t > mem  = itr->second->getNode ( NodeId::ch_ready_0 ).read();
            ValWord< uint32_t > mem2 = itr->second->getNode ( NodeId::ch_ready_1 ).read();
            itr->second->dispatch();

            chReady= (((OpcUa_UInt64)mem2.value())<<32) + mem.value();
            chReadyMap.insert(std::pair<int, OpcUa_UInt64>(itr->first,chReady));
            maskMap.insert(std::pair<int, OpcUa_UInt64>(itr->first,0));


        }

    } catch(const std::exception& e) {
        LOG(Log::ERR) << logID <<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
    //create mask for the latomes depending on the mapping of fibers
    for (DLtdbSca* sca : getParent()->ltdbscas()) {
        for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()) {
            for (DLtdbFiber* fiber : locx2->ltdbfibers()) {
                if (maskMap.find(fiber->getFiberId()->getLatomeId()) != maskMap.end()) {
                    maskMap[fiber->getFiberId()->getLatomeId()]=(maskMap[fiber->getFiberId()->getLatomeId()] | (((OpcUa_UInt64)1)<<(fiber->getFiberId()->getFiberId()-1)));
                }
            }
        }
    }
    chReady=0;
    //For all latome
    for (itrChReady = chReadyMap.begin() ; itrChReady != chReadyMap.end() ; itrChReady++) {
        itrMaskMap=maskMap.find(itrChReady->first);
        if(itrMaskMap==maskMap.end()){
        	LOG(Log::ERR) << logID << "Error latome not find in map";
        	return OpcUa_BadInternalError;
        }
        //chReady=(chReady |(itrChReady->second & itrMaskMap->second));
        //Reading with mask
        chReady=itrChReady->second & itrMaskMap->second;
        bitSet=new std::bitset<64>(itrMaskMap->second);

        //Debug
        LOG(Log::TRC) << logID << "Latome : "<< itrChReady->first <<" Mask          : "<<bitSet->to_string();
        delete bitSet;

        bitSet=new std::bitset<64>(itrChReady->second);
        LOG(Log::TRC) << logID << "Latome : "<< itrChReady->first <<" Channel ready : "<<bitSet->to_string();
        delete bitSet;

        bitSet=new std::bitset<64>(chReady);
		LOG(Log::TRC) << logID << "Latome : "<< itrChReady->first << "Masked Channel ready : "<<bitSet->to_string();
		delete bitSet;

		//Reorder
        for (DLtdbSca* sca : getParent()->ltdbscas()) {
                for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()) {
                    for (DLtdbFiber* fiber : locx2->ltdbfibers()) {
                    	if(fiber->getFiberId()->getLatomeId()==itrChReady->first){
							mask=(((OpcUa_UInt64)1)<<(fiber->getFiberId()->getFiberId()-1));
							if ((chReady & mask) > 0) {
								chReadyReordered=(chReadyReordered | (((OpcUa_UInt64)1)<< (fiber->getTotalFiberNum()-1)));
							}
                    	}
                    }
                }
            }
    }

    chReady=chReadyReordered;
    bitSet=new std::bitset<64>(chReady);
    LOG(Log::INF) << logID << " Channel ready Reordered :"<<bitSet->to_string();
    delete bitSet;


    return OpcUa_Good;

}

UaStatus DStatusLatome::readACR (OpcUa_UInt64 & acr) {
    std::bitset<64>* bitSet;
    OpcUa_UInt64 acrReordered=0;
    OpcUa_UInt64 mask;
    if(!connectHardware()){
    		LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
    		return OpcUa_Bad;
    	}

    std::map<int, HwInterface*>::iterator itr;
    std::map<int, OpcUa_UInt64> acrMap;
    std::map<int, OpcUa_UInt64>::iterator itrACR;

    std::map<int, OpcUa_UInt64> maskMap;
    std::map<int, OpcUa_UInt64>::iterator itrMaskMap;

    try {
        uhal::setLogLevelTo( uhal::Error() );

        for (itr = interface.begin() ; itr != interface.end() ; itr++) {
            ValWord< uint32_t > mem  = itr->second->getNode ( NodeId::active_channel_0 ).read();
            ValWord< uint32_t > mem2 = itr->second->getNode ( NodeId::active_channel_1 ).read();
            itr->second->dispatch();
            acr= (((OpcUa_UInt64)mem2.value())<<32) + mem.value();
            acrMap.insert(std::pair<int, OpcUa_UInt64>(itr->first,acr));

            maskMap.insert(std::pair<int, OpcUa_UInt64>(itr->first,0));
        }

    } catch(const std::exception& e) {
        LOG(Log::ERR) << logID<<"Caught:"<<e.what();
        return OpcUa_Bad;
    }

    for (DLtdbSca* sca : getParent()->ltdbscas()) {
        for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()) {
            for (DLtdbFiber* fiber : locx2->ltdbfibers()) {
                if (maskMap.find(fiber->getFiberId()->getLatomeId()) != maskMap.end()) {
                    maskMap[fiber->getFiberId()->getLatomeId()]= (maskMap[fiber->getFiberId()->getLatomeId()] | (((OpcUa_UInt64)1)<<(fiber->getFiberId()->getFiberId()-1)));
                }

            }
        }
    }
    acr=0;
    for (itrACR = acrMap.begin() ; itrACR != acrMap.end() ; itrACR++) {
        itrMaskMap=maskMap.find(itrACR->first);
        if(itrMaskMap==maskMap.end()){
			LOG(Log::ERR) << logID << "Error latome not find in map";
			return OpcUa_BadInternalError;
		}
        //acr=(acr | (itrACR->second & itrMaskMap->second));
        acr=itrACR->second & itrMaskMap->second;

        bitSet=new std::bitset<64>(itrMaskMap->second);
        LOG(Log::TRC) << logID << logID << "Latome : "<< itrACR->first <<" Mask : "<<bitSet->to_string();
        delete bitSet;

        bitSet=new std::bitset<64>(itrACR->second);
        LOG(Log::TRC) << logID << logID << "Latome : "<< itrACR->first <<" ACR  : "<<bitSet->to_string();
        delete bitSet;

        bitSet=new std::bitset<64>(acr);
		LOG(Log::TRC) << logID << logID << "Latome : "<< itrACR->first << "Masked ACR : "<<bitSet->to_string();
		delete bitSet;
		 for (DLtdbSca* sca : getParent()->ltdbscas()) {
		        for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()) {
		            for (DLtdbFiber* fiber : locx2->ltdbfibers()) {
		            	if(fiber->getFiberId()->getLatomeId()==itrACR->first){
							mask=(((OpcUa_UInt64)1)<<(fiber->getFiberId()->getFiberId()-1));
							if ((acr & mask) > 0) {
								acrReordered= ( acrReordered | (((OpcUa_UInt64)1)<< (fiber->getTotalFiberNum()-1)));
							}
		            	}
		            }
		        }
		    }
    }


    bitSet=new std::bitset<64>(acrReordered);
    LOG(Log::INF) << logID << logID  <<" ACR Reordered           :"<<bitSet->to_string();

    delete bitSet;
    acr=acrReordered;


    return OpcUa_Good;

}

UaStatus DStatusLatome::getFiberChReadyErrCounter(FiberId* fiberId, OpcUa_Byte &errCounter){
	std::map<int, HwInterface*>::iterator itr;
	if(!connectHardware()){
			LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
			return OpcUa_Bad;
		}
	try {

		itr=interface.find(fiberId->getLatomeId());
		itr->second->getNode (NodeId::count_load_clear[fiberId->getFiberId()-1] ).write(1);
		itr->second->getNode (NodeId::count_load_clear[fiberId->getFiberId()-1] ).write(0);
		ValWord< uint32_t > mem = itr->second->getNode (NodeId::ch_ready_error[fiberId->getFiberId()-1] ).read();
		itr->second->dispatch();
		errCounter=mem.value();
		return OpcUa_Good;
	 } catch(const std::exception& e) {
		LOG(Log::ERR) << logID<<"Caught:"<<e.what();
		return OpcUa_Bad;
	}

}
UaStatus DStatusLatome::chkChannelReadyError (OpcUa_UInt64 & chStatus,int timeChk /*Optional in second*/) {
    UaStatus s;
    if(!connectHardware()){
    		LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
    		return OpcUa_Bad;
    	}
    std::bitset<64>* bitSet;
    OpcUa_UInt64 chGoodReordered=0;;



    OpcUa_UInt64 channelReady;

    OpcUa_Byte errCount;
    std::map<int, HwInterface*>::iterator itr;
    if(timeChk==-1){timeChk=(int)delayForCheckErrorCounter();} //Not defined, taking default

    try {
        uhal::setLogLevelTo( uhal::Error() );
        s=readChannelReady(channelReady);
        if ( !s.isGood() ) {
            return s;
        }


        for (DLtdbSca* sca : getParent()->ltdbscas()) {
            for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()) {
                for (DLtdbFiber* fiber : locx2->ltdbfibers()) {
                	if(fiber->getFiberId()->getFiberId() < 49){
                	  getFiberChReadyErrCounter(fiber->getFiberId(),errCount);
                	}

                	/*
                    itr=interface.find(fiber->getFiberId()->getLatomeId());
                    ValWord< uint32_t > mem = itr->second->getNode (NodeId::ch_ready_error[fiber->getFiberId()->getFiberId()-1] ).read();
                    itr->second->dispatch();
                    regErrorBegin[(fiber->getTotalFiberNum()-1)]=mem.value();
                    */
                }
            }
        }
        if (channelReady!=0) {
            boost::this_thread::sleep( boost::posix_time::milliseconds(timeChk*1000) );
        }
        for (DLtdbSca* sca : getParent()->ltdbscas()) {
            for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()) {
                for (DLtdbFiber* fiber : locx2->ltdbfibers()) {
                    if ((channelReady & (((OpcUa_UInt64)1) << (fiber->getTotalFiberNum()-1))) != 0) {
                    	if(fiber->getFiberId()->getFiberId() < 49){
                    		getFiberChReadyErrCounter(fiber->getFiberId(),errCount);
                    	}
                    	else{
                    		errCount=0;
                    	}
                        if(errCount==0){
                        	chGoodReordered=( chGoodReordered |(((OpcUa_UInt64)1) << (fiber->getTotalFiberNum()-1)));
                        }
                        /*
                         * itr=interface.find(fiber->getFiberId()->getLatomeId());
                        ValWord< uint32_t > mem = itr->second->getNode (NodeId::ch_ready_error[fiber->getFiberId()->getFiberId()-1] ).read();
                        itr->second->dispatch();
                        regErrorEnd[(fiber->getTotalFiberNum()-1)]=mem.value();
                        if (regErrorEnd[(fiber->getTotalFiberNum()-1)]==regErrorBegin[(fiber->getTotalFiberNum()-1)]) {
                            chGoodReordered=( chGoodReordered |(((OpcUa_UInt64)1) << (fiber->getTotalFiberNum()-1)));



                        }
                        */
                    }

                }
            }
        }

        bitSet=new std::bitset<64>(chGoodReordered);
        LOG(Log::INF) << logID << " chGoodReordered:"<<bitSet->to_string();
        delete bitSet;
        chStatus = chGoodReordered;



    } catch(const std::exception& e) {
        LOG(Log::ERR) << logID << "Caught:"<<e.what();
        return OpcUa_Bad;
    }
    return OpcUa_Good;

}
UaStatus DStatusLatome::readAdcMean (OpcUa_Int32 fiberNum,
                                     OpcUa_Int32 SCNum, OpcUa_Int32 & mean) {
    return meanSuperCellUser(fiberNum,SCNum, mean);
}
UaStatus DStatusLatome::scanAdcMean (const UaString & fileName) {
    /*
    //int scan[8][8];
    int mean;
    int tmean[6];
    UaStatus s = OpcUa_Bad;
    int fiberLatome;
    std::ofstream file(fileName.toUtf8(), std::ios::out | std::ios::trunc);

    for (int fiberId=1;fiberId<=62;fiberId++){
    	file<<fiberId<<" ";
    	meanSuperCellUser(fiberId ,tmean);
    	for (int scId=1;scId<=6;scId++){
    		file<<tmean[scId-1]<<" ";
    	}
    	file<<std::endl;
    }
    file.close();
    return OpcUa_Good;
    */
    return OpcUa_BadNotImplemented;
}

UaStatus DStatusLatome::readChannelReadyWithoutMapping (OpcUa_UInt64 & chReady ,HwInterface *hwUsed) {
	if(!connectHardware()){
		LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
		return OpcUa_Bad;
	}
    try{
    	uhal::setLogLevelTo( uhal::Error() );
		hwUsed->getNode (NodeId::global_load_clear ).write(1);
		hwUsed->getNode (NodeId::global_load_clear ).write(0);
        hwUsed->dispatch();
    	ValWord< uint32_t > mem =  hwUsed->getNode ( NodeId::ch_ready_0 ).read();
    	ValWord< uint32_t > mem2 = hwUsed->getNode ( NodeId::ch_ready_1 ).read();
    	hwUsed->dispatch();
    	chReady= ((((OpcUa_UInt64)mem2.value())<<32) + mem.value());
      }
      catch(const std::exception& e){
    	LOG(Log::ERR) << logID << "Caught:"<<e.what();
    	return OpcUa_Bad;
      }
      return OpcUa_Good;
}
UaStatus DStatusLatome::statusFiber(int totalFiberNum,bool & status, bool fastCheck,int timeChk) {
    OpcUa_UInt64 chStatus;
    std::bitset<64>* bitSet;

    UaStatus s;
	if(fastCheck || timeChk==0){
		s=readChannelReady(chStatus);
	}
	else {
		s=chkChannelReadyError (chStatus,timeChk);
	}

    if (!s.isGood()){
      return s;
    }

    OpcUa_UInt64 mask=(((OpcUa_UInt64)1)<< (totalFiberNum-1));

    LOG(Log::INF) << logID<<"statusFiber fiber: :"<<totalFiberNum;
    bitSet=new std::bitset<64>(chStatus);
    LOG(Log::INF) << logID<<"chStatus :"<<bitSet->to_string();
    delete bitSet;
    bitSet=new std::bitset<64>(mask);
    LOG(Log::INF) << logID<<"mask     :"<<bitSet->to_string();
    delete bitSet;

    if ((mask & chStatus)>0){
      status=true;
      LOG(Log::INF) << logID<<"status:"<<status;
      return OpcUa_Good;
    }
    else{
      status=false;
      LOG(Log::INF) << logID<<"status:"<<status;
      return OpcUa_Good;
    }
    //return OpcUa_BadNotImplemented;
}
UaStatus DStatusLatome::activeFiber(int totalFiberNum,bool & status) {
    OpcUa_UInt64 chStatus;
    std::bitset<64>* bitSet;

    UaStatus s;
	s=readACR(chStatus);


    if (!s.isGood()){
      return s;
    }

    OpcUa_UInt64 mask=(((OpcUa_UInt64)1)<< (totalFiberNum-1));

    if ((mask & chStatus)>0){
      status=true;
    }
    else{
      status=false;

    }
    LOG(Log::INF) << logID<<"ACR fiber: :"<<totalFiberNum;
   bitSet=new std::bitset<64>(chStatus);
   LOG(Log::INF) << logID<<"ACR  :"<<bitSet->to_string();
   delete bitSet;
   bitSet=new std::bitset<64>(mask);
   LOG(Log::INF) << logID<<"mask :"<<bitSet->to_string();
   delete bitSet;
    LOG(Log::INF) << logID<< logID <<"channel "<< totalFiberNum << " active:"<<status;
      return OpcUa_Good;
}
UaStatus DStatusLatome::meanSuperCellUser(int userChannel,int SCNum, int & mean) {
    /*
    try{
      connectHardware();
      int validityMask=0x1000;
      int dataMask=0xFFF;
      ValVector< uint32_t > dataRead ;
      dataRead=hw->getNode (  NodeId::usr_mean[userChannel-1]).readBlock(6);
      hw->dispatch();
      if ((validityMask & dataRead[SCNum-1])!=0 ){
    	mean=(dataRead[SCNum-1] & dataMask);
      }
      else if(dataRead[SCNum-1]!=0){
    	mean=-1;
      }
      else{
    	mean=0;
      }
    }
    catch(const std::exception& e){
    LOG(Log::ERR)<<"Caught:"<<e.what();
    return OpcUa_Bad;
    }
    return OpcUa_Good;
    */
    return OpcUa_BadNotImplemented;
}
UaStatus DStatusLatome::meanSuperCellUser(int userChannel, int *mean) {
    /* try {
      connectHardware();
      int validityMask=0x1000;
      int dataMask=0xFFF;
      ValVector< uint32_t > dataRead ;
      dataRead=hw->getNode (NodeId::usr_mean[userChannel-1]  ).readBlock(6);
      hw->dispatch();
      for (int i=0 ;i <6 ;i++){
    	  if ((validityMask & dataRead[i])!=0 ){
    		mean[i]=(dataRead[i] & dataMask);
    	  }
    	  else if(dataRead[i]!=0){
    		mean[i]=-1;
    	  }
    	  else {
    		mean[i]=0;
    	  }

      }
     }
    catch(const std::exception& e){
    	LOG(Log::ERR)<<"Caught:"<<e.what();
    	return OpcUa_Bad;
      }

     return OpcUa_Good;*/
    return OpcUa_BadNotImplemented;
}
UaStatus DStatusLatome::meanSuperCellIstage(int FiberNumLatome,int SCNumLatome, int & mean) {
    connectHardware();
    /*	  ValWord< uint32_t > dataRead ;
    	  mean=0;

    	  try{
    		 LOG(Log::INF)<<"reading supercell:"<<SCNumLatome-1;
    		  	  hw->getNode ( "istage.sync_box.scope.control.sc_index" ).write(SCNumLatome-1);
    		  	  hw->dispatch();
    	  LOG(Log::INF)<<"reading supercell:"<<SCNumLatome-1;
    	  hw->getNode ( "istage.sync_box.scope.control.sc_index" ).write(SCNumLatome-1);
    	  hw->dispatch();
    	  LOG(Log::INF)<<"reading for fiber:"<<FiberNumLatome-1;
    	  hw->getNode ( "istage.sync_box.scope.control.fiber_index" ).write(FiberNumLatome-1);
    	  hw->dispatch();

    	  for (int dataIndex=0;dataIndex<16;dataIndex++){
    		  std::string nodeId ="istage.sync_box.scope.data_"+std::to_string(dataIndex)+".value";
    		  LOG(Log::INF)<<"node:"<<nodeId;
    		  dataRead=hw->getNode (  nodeId.c_str()).read();
    		  hw->dispatch();
    		  LOG(Log::INF)<<hw->getNode (  nodeId.c_str()).getPath();
    		  LOG(Log::INF)<<"dataRead("<<dataIndex<<"):"<<dataRead.value();
    		  mean+=dataRead.value();
    	  }
    	  mean+=16;
    	  return OpcUa_Good;
    	  }
     	  catch(const std::exception& e){
     	 		LOG(Log::ERR)<<"Caught:"<<e.what();
     	 		return OpcUa_Bad;
     	 	  }
    */
    return OpcUa_BadNotImplemented;

}
UaStatus DStatusLatome::meanSuperCellIstage(AdcId *adcId,int channel, int & mean) {
	if(!connectHardware()){
			LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
			return OpcUa_Bad;
		}
    return meanSuperCellIstage(adcId->getIstageStreamId(),adcId->getChannelPosIstage(channel),mean);
}
UaStatus DStatusLatome::meanSuperCellUser(AdcId *adcId,int channel, int & mean) {
	if(!connectHardware()){
			LOG(Log::ERR) << logID<< "ERROR LATOME NOT CONNECTED";
			return OpcUa_Bad;
		}
    return meanSuperCellUser(adcId->getUserStreamId(channel),adcId->getChannelPosUser(channel),mean);
}


/**
 * @return the LATOME firmware version.
 */
const std::string DStatusLatome::Getfw_version(HwInterface *device) {
  // Read the firmware version in the FPGA until '\0' found
  std::string fpgaVersion = "";
  size_t labelID = 0;
  const size_t MAX_FPGA_VERSION_LABELS = 32;
  bool stringEndFound = false;
  do {
	  LOG(Log::WRN) << logID<< "Reading register:" << "fpga.firmware_version.label_" << std::to_string(labelID) <<".value" ;
	const uint32_t labelValue=device->getNode("fpga.firmware_version.label_" + std::to_string(labelID)+".value").read();
	device->dispatch();
    //const uint32_t labelValue = ReadRegisterValue("fpga.firmware_version.label_" + std::to_string(labelID));
	LOG(Log::WRN) << logID<< "Reading register:" << "fpga.firmware_version.label_" << std::to_string(labelID) << ".value:" << std::hex << int(labelValue)<<std::dec;
    // Split each read 32bits in 4*8bits characters
    for (size_t i = 0; i < sizeof (labelValue); i++) {
      const char currentByte = (labelValue >> (i * 8)) & 0xff;
      // Escape the loop as soon as we reach an '\0' character
      if (currentByte == '\0') {
        stringEndFound = true;
        break;
      }
      fpgaVersion += currentByte;
    }
    labelID++;
  } while ((labelID < MAX_FPGA_VERSION_LABELS) && (stringEndFound == false));
  if (labelID == MAX_FPGA_VERSION_LABELS) {
    //SetFatalErrorMsg(CONFIGURATION, "Could not decode FPGA firmware version.");
    return "unknown";
  }
  return fpgaVersion;
}



bool DStatusLatome::connectHardware() {
    if (!isConnected) {
        try {
            getParent()->mapping()->reloadMappingUnderId();
            uhal::setLogLevelTo( uhal::Error() );
            ConnectionManager manager (getParent()->connectionLatomeFile() );

            for (DLtdbSca* sca : getParent()->ltdbscas()) {
                for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()) {
                    for (DLtdbFiber* fiber : locx2->ltdbfibers()) {
                        auto search=interface.find(fiber->getFiberId()->getLatomeId());
                        if(search != interface.end()) {
                            //This Latome is already in the map
                            LOG(Log::TRC) << logID<<"Latome :" << fiber->getFiberId()->getLatomeId() << "found in mapping files, Already in the map";
                        } else {
                        	HwInterface *latomeInterface=new HwInterface( manager.getDevice ("latome."+ std::to_string(fiber->getFiberId()->getLatomeId() )));
                        	std::string tagVersion;
                        	try{
                        		tagVersion = latomeInterface->getNode("fpga.firmware_version").getTags();
                        	} catch(const std::exception& e) {
                                    LOG(Log::ERR) << logID<<"Caught During tags taking:"<<e.what();
                                }

                        	//Exemple type=block,version=LATOME_FW-v3.3.0-0-0-0-2020-12-15
                        	std::vector<std::string>  tagList = tokenize(tagVersion,",");
                        	bool tagFound=false;
                        	std::string fwVersion;
                        	for (auto &tag : tagList){
                        		if (tag.find("version=") != std::string::npos) {
                        			tagFound=true;
                        			//Tag founded
                        			tag.erase(0,8);
                                	try{
                                		fwVersion=Getfw_version(latomeInterface);
                                	} catch(const std::exception& e) {
                                            LOG(Log::ERR) << logID<<"Caught fw string reading:"<<e.what();
                                        }


                        			if(fwVersion!=tag){
                        				isConnected=false;
										 //delete(latomeInterface);
										 LOG(Log::ERR) << logID<<"latome."<< std::to_string(fiber->getFiberId()->getLatomeId() )
										 	 << "WRONG FW VERSION Found : XML " <<tag << " DEVICE : ERROR IGNORED"<<fwVersion;
										 //return isConnected;

                        			}
                        		}
                        	}
                        	if(!tagFound){
                        		 isConnected=false;
                        		 delete(latomeInterface);
                        		 LOG(Log::ERR) << logID<<"latome."<< std::to_string(fiber->getFiberId()->getLatomeId() )
                        		 	<< "No tag found";
                        		 return isConnected;
                        	}

                            LOG(Log::INF) << logID<<"Latome :" << fiber->getFiberId()->getLatomeId() << "found in mapping files, added in the map";
                            interface.insert(std::pair<int,HwInterface*>( fiber->getFiberId()->getLatomeId()
                                             , latomeInterface) );
                        }
                    }
                }
            }

            //hw=new HwInterface( manager.getDevice ( getParent()->latomeDevice() ));
            isConnected=true;

        } catch(const std::exception& e) {
            LOG(Log::ERR) << logID<<"Caught:"<<e.what();
            isConnected=false;
        }
    }
    return isConnected;
}
UaStatus DStatusLatome::readScopeIstage(int streamIndex,int *data) {
    ValVector< uint32_t > dataRead ;
    int validityMask=0x80000000;
    int dataMask=0xFFF;
    int i;
    try {
        ValWord< uint32_t > mem;
        bool ready=true;
        hw->getNode ( NodeId::scp_start_bcid ).write(0);
        hw->dispatch();
        hw->getNode ( NodeId::scp_ctrl_start ).write(1);
        hw->dispatch();

        for (i=0; i<10 && !ready; i++) { // wainting 10 read for ready
            ValWord< uint32_t > mem =  hw->getNode ( NodeId::scp_status_ready ).read();
            hw->dispatch();
            ready=(((int)mem.value()&1)==1);

        }
        if (i>=9) {
            LOG(Log::ERR)<<"ready of istage scope time out:";
            for (int i=0 ; i <8 ; i++) {
                data[i]=-1;
            }
            return OpcUa_Good;
        }
        dataRead=hw->getNode (  NodeId::scp_data[streamIndex-1]).readBlock(8);
        hw->dispatch();
        //std::cout <<

        for (int i=0 ; i <8 ; i++) {
            LOG(Log::TRC) << logID<<"data read:"<<std::hex<<dataRead[i];
            if ((validityMask & dataRead[i])!=0 ) {
                data[i]=(dataRead[i] & dataMask);
            } else if(dataRead[i]!=0) {
                data[i]=-1;
            } else {
                data[i]=0;
            }
        }

        return OpcUa_Good;
    } catch(const std::exception& e) {
        LOG(Log::ERR) << logID<<"Caught:"<<e.what();
        return OpcUa_Bad;
    }
}
UaStatus DStatusLatome::readScopeIstage(int streamIndex,int pos,int &data) {
    int dataRead[8];
    UaStatus s=readScopeIstage(streamIndex,dataRead);
    data=dataRead[pos-1];
    return s;
}

 UaStatus DStatusLatome::updateReadACR()
{
    OpcUa_UInt64 acr;
    LOG(Log::INF)<<"Called ReadACR from register";
 	getAddressSpaceLink()->setReadACRError(METHOD_IN_PROGRESS,OpcUa_Good);
 	UaStatus status = readACR(acr);
 	getAddressSpaceLink()->setReadAcrACR(acr,OpcUa_Good);
 	if (!status.isGood()){
 		getAddressSpaceLink()->setReadACRError(METHOD_BAD,OpcUa_Good);
 	}
 	else {
 		getAddressSpaceLink()->setReadACRError(METHOD_GOOD,OpcUa_Good);
 	}
 	return status;
}

  /* Note: never directly call this function. */

UaStatus DStatusLatome::updateChkChannelReadyError()
{
	OpcUa_UInt64 channelReady;
	LOG(Log::INF)<<"Called ReadChannelReady configure from register";
	getAddressSpaceLink()->setChkChannelReadyErrorError(METHOD_IN_PROGRESS,OpcUa_Good);
	UaStatus status = chkChannelReadyError(channelReady);
	getAddressSpaceLink()->setChkChannelReadyErrorStatus(channelReady,OpcUa_Good);
	if (!status.isGood()){
		getAddressSpaceLink()->setChkChannelReadyErrorError(METHOD_BAD,OpcUa_Good);
	}
	else {
		getAddressSpaceLink()->setChkChannelReadyErrorError(METHOD_GOOD,OpcUa_Good);
	}
	return status;
}

}
