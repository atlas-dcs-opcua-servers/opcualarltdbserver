
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt) 
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */




#include <Configuration.hxx>

#include <DLtdbSca.h>
#include <ASLtdbSca.h>


#include <DLtdbMtrx.h>
#include <DLtdbGpio.h>

#include <DLtdbMTx.h>
#include <DLtdbLOCx2.h>

#include <DLtdbLOCx2.h>
#include <DLtdbADCNevis.h>
#include <DLtdbFiber.h>
#include <DLocx2Scan.h>
#include <DLtdbGBTx.h>
#include <DMapping.h>
#include <DLtdb.h>
#include <string>
#include <LogIt.h>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>
#include <LtdbCommon.h>
#include <iostream>
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <SCA.h>
#include <boost/thread/thread.hpp>
#include <fstream>

#include <utility>      // std::move
#include <future>       // std::packaged_task, std::future
#include <thread>       // std::thread

namespace Device
{

	void DLtdbSca::checkGbtxLink(UaClientSdk::UaSession* session){
    std::string nodeId ="ltdb_"+ getParent()->id()+".scaFelix"+id();
		UaoClientForOpcUaSca::SCA SCA(session,UaNodeId(nodeId.c_str(),2));
    getAddressSpaceLink()->setAddress(SCA.readAddress(),OpcUa_Good);
		std::string address=SCA.readAddress().toUtf8();
		if (*address.rbegin() ==  'f'){
			//DIR ELINK
			getAddressSpaceLink()->setLinkGBTxUsed("DIR",OpcUa_Good);
		}
		else  if (*address.rbegin() ==  'd'){
			getAddressSpaceLink()->setLinkGBTxUsed("AUX",OpcUa_Good);
			//AUX ELINK
		}
		else{
			getAddressSpaceLink()->setLinkGBTxUsed("ERR",OpcUa_Good);
			//ERROR
		}

	}

    UaStatus DLtdbSca::configure (UaClientSdk::UaSession* session, bool stopBeforAdc)
    {
        UaStatus adcError = OpcUa_Good;
        UaStatus s = OpcUa_Bad;
        int max_retry = 5;
	  
        for(int nbretry = 0; nbretry < max_retry; nbretry ++)
        {
            adcError = OpcUa_Good; //put back adcError to good
            
            //at the 2dn retry, reset the SCA 
            if(nbretry == 2)
            {
                std::string nodeId ="ltdb_"+ getParent()->id()+".scaFelix"+id();
				UaoClientForOpcUaSca::SCA SCA(session,UaNodeId(nodeId.c_str(),2));
				LOG(Log::INF) << logID << "Reset of SCA " << id();
				SCA.reset();
				sleep(2);
            }
            
            //configure the SCA
            s = this->configure_with_adc_check(session, stopBeforAdc, &adcError);
            
            //if configuration is good
            if (s.isGood())
            {
                s = OpcUa_Good;
                //print if retry and reset have been do
			    if((0 < nbretry) && (nbretry < 2)) { LOG(Log::WRN) << logID << "Configuration of SCA " << id() << " good after " << nbretry << "retry due to an ADC configuration error"; }
			    if(2 < nbretry) { LOG(Log::WRN) << logID << "Configuration of SCA " << id() << "good after " << nbretry << " retry due to an ADC configuration error and a SCA reset"; }
			    return s;
            }
            else //if configuration isn't good
            {
                if(adcError == OpcUa_Good) //if config failed, but not due to an ADC error : impossible to configure SCA
                {
                    s = OpcUa_Bad;
                    LOG(Log::ERR) << logID << "Configuration of SCA " << id() << " failure (not due to an ADC configuration error)";
                    return s;
                }
                //else{} do nothing, just retry with the loop 
            }
        }
        LOG(Log::ERR) << logID << "Configuration of SCA " << id() << " failure due to an ADC configuration error";
        return s;
    }
  
  UaStatus DLtdbSca::configure_with_adc_check (UaClientSdk::UaSession* session,bool stopBeforAdc, UaStatus* adcError)
  {
	  int triedtimes;

	  UaStatus s = OpcUa_Bad;
	  DLtdbGpio* gpio=ltdbgpio();

      LOG(Log::TRC) << logID << "GBTx Configuration:Start";
	  for (DLtdbGBTx* gbtx : ltdbgbtxs()){
		  triedtimes=0;
		  do{
			  triedtimes++;
			  s = gbtx->configure(session);
		  }while(!s.isGood() && triedtimes < 5);
		  if (triedtimes>1 && s.isGood()){
			  LOG(Log::WRN) << logID << "Gbtx: "<<getParent()->id() <<":Configuration SUCCES after :"<< triedtimes << "retry";
		  }
		  else if (!s.isGood()){
			  LOG(Log::ERR) << logID << "Gbtx: "<<getParent()->id() <<":Configuration FAILURE";
			  getParent()->ASLOG("ERR:(0x??) error during configuration of GBTx "+ getParent()->id()+" of sca "+id());
			  return s;
		  }
		  else{
			  LOG(Log::TRC) << logID << "Gbtx: "<<getParent()->id() <<":Configuration SUCCES";
		  }
	  }

	  LOG(Log::TRC) << logID <<  "MTx Reset:Start";
	  s = gpio->resetMTx(session);
	  if (!s.isGood()){
		  LOG(Log::ERR) << logID <<  "MTx Reset: ERROR";
		  getParent()->ASLOG("ERR:(0x4) error during reset of MTx of sca "+id());
		  return s;
	  }
	  LOG(Log::TRC) << logID <<  "MTx Reset:Stop";
	  usleep(10000); //mbiaut, check if this delay resolve the error of the loss of light cause by a 0 in mtx register

	  LOG(Log::TRC) << logID <<  "MTx Configuration:Start";
	  for (DLtdbMTx* mtx : ltdbmtxs()){
		  triedtimes=0;
		 do{
			 triedtimes++;
			 s = mtx->configure(session);

		 }while(!s.isGood() && triedtimes < mtxConfigurationTries() && mtxConfigurationTries()!=255);
		  if (triedtimes>1 && s.isGood()){
			  LOG(Log::WRN) << logID << "Mtx: "<<mtx->id() <<":Configuration SUCCES after :"<< triedtimes << "retry";
		  }
		  else if (!s.isGood()){
			  LOG(Log::ERR) << logID << "Mtx: "<<mtx->id() <<":Configuration FAILURE";
			  getParent()->ASLOG("ERR:(0x5) error during configuration of MTx "+ mtx->id()+"of sca "+id());
			  return s;
		  }
		  else{
			  LOG(Log::TRC) << logID << "Mtx: "<<mtx->id() <<":Configuration SUCCES";
		  }


		  }

	  LOG(Log::TRC) << logID <<  "MTx Configuration:Stop";

	  LOG(Log::TRC) << logID <<  "LOCX2 Reset:Start";
	  s = gpio->resetLOCx(session);
	  if (!s.isGood()){
		  LOG(Log::ERR) << logID <<  "LOCX2 Reset:FAILURE";
		  getParent()->ASLOG("ERR:(0x3) error during reset of Locx2 of sca "+id());
		  return s;
	  }
	  LOG(Log::TRC) << logID <<  "LOCX2 Reset:Stop";

	  LOG(Log::TRC) << logID <<  "LOCX2 Configuration:Start";
	  for (DLtdbLOCx2* locx2 : ltdblocx2s()){
		  triedtimes=0;
		 do{
			 triedtimes++;
			 s = locx2->configure(session);
		 }while(!s.isGood() && triedtimes < 5);
		  if (triedtimes>1 && s.isGood()){
			  LOG(Log::WRN) << logID << "Locx: "<<locx2->id() <<":Configuration SUCCES after :"<< triedtimes << "retry";
		  }
		  else if (!s.isGood()){
			  LOG(Log::ERR) << logID << "Locx: "<<locx2->id() <<":Configuration FAILURE";
			  getParent()->ASLOG("ERR:(0x6) error during configuration of locx2 "+ locx2->id()+" of sca "+id());
			  return s;
		  }
		  else{
			  LOG(Log::TRC) << logID << "Locx: "<<locx2->id() <<":Configuration SUCCES";
		  }


	  }
	  if(stopBeforAdc){
		  return s;
	  }

	  LOG(Log::TRC) << logID << "ADC configuration:Start";
	  s = configureADC(session);
	  if (!s.isGood())
	  {
		  LOG(Log::ERR) << logID << "ADC configuration:FAILURE";
		  *adcError = OpcUa_Bad;
		  return s;
	  }
	  else
	  {
		  *adcError = OpcUa_Good;
	  }
	  LOG(Log::TRC) << logID << "ADC configuration:SUCCES";


	  LOG(Log::TRC) << logID << "Locx2 DelayDefaultWrite :Start";
	  //s = locx2DelayDefaultWrite(session);
	  s= setDelayAllFibers(19,1,session);
	  if (!s.isGood()){LOG(Log::ERR) << logID << "Locx2 DelayDefaultWrite:FAILURE";return s;}
	  LOG(Log::TRC) << logID << "Locx2 DelayDefaultWrite:SUCCES";

	  LOG(Log::TRC) << logID << "corrCoeffADCFromeFiles :Start";
	  s = corrCoeffADCFromeFiles(session);
	  if (!s.isGood()){LOG(Log::ERR) << logID << "corrCoeffADCFromeFiles:FAILURE";return s;}
	  LOG(Log::TRC) << logID << "corrCoeffADCFromeFiles:SUCCES";

	  LOG(Log::TRC) << logID << "selectAdcModeForAdc :Start";
	  s = selectAdcModeAllAdc(DLtdbADCNevis::DATA_MODE,session);
	  if (!s.isGood()){LOG(Log::ERR) << logID << "selectAdcModeForAdc:FAILURE";return s;}
	  LOG(Log::TRC) << logID << "selectAdcModeForAdc:SUCCES";


	  return OpcUa_Good;
  }


  UaStatus DLtdbSca::autoscan(UaClientSdk::UaSession* session){
	  UaStatus s=OpcUa_Good;
	  int i=0;
	  std::future <UaStatus> future_status[8] ;
	  std::thread* threadFiber[8];
	  OpcUa_Byte delay[8];
	  OpcUa_UInt64 result=0;

	  s=configure(session,true);
	  if (!s.isGood()){return s;}

	  for (DLtdbLOCx2* locx2 : ltdblocx2s()){
		  for (DLtdbFiber *fiber :locx2->ltdbfibers()){
			  std::packaged_task<UaStatus(UaClientSdk::UaSession*,OpcUa_Byte *delayGd)> foo;                          // default-constructed
			  std::packaged_task<UaStatus(UaClientSdk::UaSession*,OpcUa_Byte *delayGd)> bar ([fiber](UaClientSdk::UaSession* session,OpcUa_Byte *delayGd){return fiber->autoscan(session,delayGd);}); // initialized
			  foo = std::move(bar);
			  future_status[i] = foo.get_future();
			  threadFiber[i]=new std::thread(std::move(foo),session,delay+i);
			  i++;
		  }


	  }
	 for (int i=0;i<8;i++){
	  future_status[7-i].wait();

	  if(!future_status[7-i].get().isGood()){
		  s=OpcUa_Bad;
	  }
	  else{
		  result=result<<8;
		  result+=delay[7-i];

		  LOG(Log::INF) << logID  << "Autoscan found delay : " << (int)delay[7-i] << " for fiber :" << (std::stoi(id())-1)*8+8-i;
	  }
	  threadFiber[7-i]->join();

	 }
	 if (s.isGood()){
		  LOG(Log::INF) << logID << "Autoscan updated delay";
		  locx2DelayUpdate(result);
	  }
	  return s;
  }
  UaStatus DLtdbSca::selectAdcModeAllAdc (OpcUa_Byte numMode,UaClientSdk::UaSession* session){
	  UaStatus s = OpcUa_Bad;
	  LOG(Log::TRC) << logID << "selectAdcModeAllAdc";
	  for (DLtdbLOCx2* adclocx2 : ltdblocx2s()){
		  s = adclocx2->selectAdcModeAllAdc (numMode,session);
		  if (!s.isGood()){
			  return s;
		  }

	  }
	  return OpcUa_Good;
  }
  UaStatus DLtdbSca::calibrateADC (UaClientSdk::UaSession* session)
      {
  	  UaStatus s = OpcUa_Bad;
	  LOG(Log::INF) << logID << "Calibrate";
    	for (DLtdbLOCx2* locx2 : ltdblocx2s()){
    		for (DLtdbADCNevis* adc : locx2->ltdbadcneviss()){
    			for (int i=1;i<=4;i++){
					s=adc->calibration(i,session);
					if (!s.isGood()){
						return s;
					}
    			}
    		}
    	}
    	  return OpcUa_Good;
      }
  UaStatus DLtdbSca:: corrCoeffADCFromeFiles(UaClientSdk::UaSession* session){
  	  UaStatus s=OpcUa_Good;
  	  int i=0;
  	for (DLtdbLOCx2* locx2 : ltdblocx2s()){
  	  s= locx2->corrCoeffADCFromeFiles(session);
  	  if(!s.isGood()){
		  LOG(Log::ERR) << logID << "ADC from files: Locx2: " << locx2->id() << " Failed";
		  getParent()->ASLOG("ERR:(0x9) error during MDAQ write of ADCs of locx2 "+  locx2->id() +" of sca "+id());
		  s=OpcUa_Bad;
		  return s;
	  }
  	  else{
  		LOG(Log::TRC) << logID << "ADC from files: Locx2: " << i+1 << " Succeed";
  	  }
  	}

  	  return s;
    }
  UaStatus DLtdbSca::configureADC (UaClientSdk::UaSession* session)
    {
	  UaStatus s = OpcUa_Bad;
	  //modif ATLLARONL-1191
  	  LOG(Log::TRC) << logID << "ConfigureADC";
  	int triedtimes=0;
  	for (DLtdbLOCx2* locx2 : ltdblocx2s()){
  		for (DLtdbADCNevis* adc : locx2->ltdbadcneviss()){
  		  triedtimes=0;
		 do{
			 triedtimes++;
			 s=adc->resetADC(session);
		 }while(!s.isGood() && triedtimes < 5);
		  if (triedtimes>1){
			  LOG(Log::WRN) << logID  <<  "Adclocx2_"<<locx2->id() <<":Number of try :"<< triedtimes;
		  }
		  if (!s.isGood()){
			  getParent()->ASLOG("ERR:(0x7) error during configuration of adc "+  std::to_string(adc->getTotalAdcNum())+"of sca "+id());
			  LOG(Log::ERR) << logID  << "error during configuration of adc "+  std::to_string(adc->getTotalAdcNum())+"of sca "+id();
			  return s;
		  }
  		}
  	}
  	  return OpcUa_Good;
    }

  UaStatus DLtdbSca::setDelayAllFibers (OpcUa_Byte delay, OpcUa_Byte clkphase,UaClientSdk::UaSession* session)
  {
	  UaStatus s = OpcUa_Bad;
	  int triedtimes=0;
	  //modif ATLLARONL-1191
	  LOG(Log::TRC) << logID << "setDelayAllFibers : delay=" << (int)delay << "  clkphase=" << (int)clkphase;
	  for (DLtdbLOCx2* locx2 : ltdblocx2s()){
		  triedtimes=0;
		 do{
			 triedtimes++;
			 s = locx2->setDelayAllFiber(delay,clkphase,session);
		 }while(!s.isGood() && triedtimes < 5);
		  if (triedtimes>1){
			  LOG(Log::WRN) << logID  <<  "Adclocx2_"<<locx2->id() <<":Number of try :"<< triedtimes;
		  }
		  if (!s.isGood()){return s;}
	  }
    return OpcUa_Good;
  }

  UaStatus DLtdbSca::selectAdcModeForAdc (OpcUa_UInt64 mode,UaClientSdk::UaSession* session){
	  UaStatus s=OpcUa_Good;
	    	  int i=0;
	    		for (DLtdbLOCx2* locx2 : ltdblocx2s()){
	    	  	  s= locx2->selectAdcModeForAdc(mode,session);
	    	  	  if(!s.isGood()){
	    			  LOG(Log::ERR) << logID << "ADC mode selection: Locx2: " << locx2->id() << " Failed";
	    			  getParent()->ASLOG("ERR:(0x10) error during mode selection of ADCs of locx2 "+  locx2->id() +" of sca "+id());
	    			  s=OpcUa_Bad;
	    			  return s;
	    		  }
	    	  	  else{
	    	  		LOG(Log::TRC) << logID << "ADC mode selection: Locx2: " << i+1 << " Succeed";
	    	  	  }
	    	  	}
	    return s;
  }
  UaStatus DLtdbSca::locx2DelayWrite (UaClientSdk::UaSession* session,OpcUa_UInt64 delays){
	  OpcUa_Byte delay;
	  OpcUa_Byte baseFiberId=(std::stoi(id())-1)*8;
	  UaStatus s;
	  int offset;
	  for (DLtdbLOCx2* locx2 : ltdblocx2s()){
		for (DLtdbFiber* fiber : locx2->ltdbfibers()){
			offset=fiber->getTotalFiberNum()-baseFiberId-1;
			delay=(OpcUa_Byte)((delays >> (8*offset)) & 255);
			if (delay != 255){
				s=fiber->setDelay(delay ,1,session);
				if (!s.isGood()){
					LOG(Log::ERR) << logID <<  "locx2DelayWrite Fiber : " <<   fiber->getTotalFiberNum() << ": Failed";
					return s;
				}
			}
			else{
				LOG(Log::WRN) << logID <<  "locx2DelayWrite Fiber : " <<   fiber->getTotalFiberNum() << ": Ignored";
			}
		}
	  }

	  return OpcUa_Good;


  }
  UaStatus DLtdbSca::locx2DelayUpdate (OpcUa_UInt64 delays){


  	  UaStatus s;
  	  OpcUa_UInt64 actualDelays = getAddressSpaceLink()->getLocx2DefaultDelays();
  	  OpcUa_UInt64 newDelays = 0;
  	  for (int i=0; i < 8 ; i++){
  		  if( ( (delays & (((OpcUa_UInt64)0xFF) << (i*8))) >> (i*8) )!= 255){
  			newDelays = newDelays + (delays & (((OpcUa_UInt64)0xFF) << (i*8)));
  		  }
  		  else{
  			  newDelays = newDelays + (actualDelays & (((OpcUa_UInt64)0xFF) << (i*8)));
  			  LOG(Log::INF) << logID << "Fiber " << i+1 << " Have delay 255 , Update Ignored";
  		  }
  	  }

  	  std::ofstream file(getParent()->mapping()->mappingPath()+"locx2_default_"+ id()+".dat", std::ios::out | std::ios::trunc);
  	  file << newDelays;
  	  file.close();
  	  locx2DelayDefaultReadFile();
  	  return OpcUa_Good;


    }
  UaStatus DLtdbSca::locx2DelayDefaultWrite(UaClientSdk::UaSession* session){
	  return locx2DelayWrite(session,getAddressSpaceLink()->getLocx2DefaultDelays());
  }



  void DLtdbSca::locx2DelayDefaultReadFile(){
	  std::ifstream file(getParent()->mapping()->mappingPath()+"locx2_default_"+ id()+".dat", std::ios::in);
	  OpcUa_UInt64 delays;
	  if (file){
		  file >> delays;
		  file.close();
	  }
	  else{
		  delays=0x1111111111111111;
	  }
	  getAddressSpaceLink()->setLocx2DefaultDelays(delays,OpcUa_Good);
	  file.close();
  }

}
