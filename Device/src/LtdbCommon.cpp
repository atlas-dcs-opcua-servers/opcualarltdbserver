#include <LogIt.h>
#include <DigitalIO.h>
#include <sstream>
#include <string>
#include <vector>
#include <array>
#include <iomanip>
#include <LtdbCommon.h>
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <I2cSlave.h>
#include <DStats.h>


std::string tostr (int x)
{
	std::stringstream str;
	str <<std::hex<< x;
	return str.str();
}
std::string tostrdec (int x)
{
	std::stringstream str;
	str << x;
	return str.str();
}

FiberId::FiberId(int latomeIdInit,int fiberIdInit){
	latomeId =latomeIdInit;
	fiberId = fiberIdInit;
}
AdcId::AdcId(int latomeIdInit,int fiberIdInit,std::string connectionFileInit,
		int *channelPosUserInit,int *userStreamIdInit,int *channelPosIstageInit,int istageStreamIdInit)
						:FiberId(latomeIdInit,fiberIdInit){
	channelPosUser=channelPosUserInit;
	userStreamId=userStreamIdInit;
	channelPosIstage=channelPosIstageInit;
	istageStreamId=istageStreamIdInit;
}
void AdcId::logPrint(){
	  FiberId::logPrint();
	  LOG(Log::INF)<<"istageStreamId"<<istageStreamId;
	  for (int i=0;i<4;i++){
		  LOG(Log::INF)<<"Channel:"<<i+1;
		  LOG(Log::INF)<<"channelPosUser"<<channelPosUser[i];
		  LOG(Log::INF)<<"userStreamId"<<userStreamId[i];
		  LOG(Log::INF)<<"channelPosIstage"<<channelPosIstage[i];
	  }
}
void FiberId::logPrint(){
	  LOG(Log::INF)<<"latomeId"<<latomeId;
	  LOG(Log::INF)<<"fiberId"<<fiberId;
}
std::vector<std::string> tokenize(std::string value,  std::string separator){

	std::vector<std::string> tokens;
   std::string::size_type start = 0, end = 0;
   while ((end = value.find(separator, start)) != std::string::npos){
     std::string token = value.substr(start, end - start);
     if(token.size()) tokens.push_back(token);
     start = end + 1;
   }
   std::string lasttoken = value.substr(start);
   if(lasttoken.size()) tokens.push_back(lasttoken);

   return tokens;

}

UaStatus secureI2CWrite( UaoClientForOpcUaSca::I2cSlave reg, OpcUa_Byte value, std::string logId, int MaxRetry, int flags, Device::DStats *stats){

  UaByteString bs;
  OpcUa_Byte oneByte,readedValue;
  UaByteString readBs;
  oneByte = value;
  bs.setByteString(sizeof oneByte,&oneByte);
  int retry=0;
  for (retry=0; retry <  MaxRetry; retry++){
	  try{
      if (stats!=nullptr){
        stats->incremente_counter(Device::DStats::WRITE_NUMBERS);
      }
		  reg.writeValue(bs);
    }
	  catch(const std::exception& e)
	  {
	    LOG(Log::TRC) << logId <<"Caught:"<<e.what();
	    if( flags &  I2C_IGNORE_ERROR){
			  return OpcUa_Good;
	    }
      if (stats!=nullptr){
        stats->incremente_counter(Device::DStats::WRITE_FAILURES);
      }
      continue;
    }
		if(flags & I2C_NO_READBACK){
      return OpcUa_Good;
    }
		
    try{
      if (stats!=nullptr){
        stats->incremente_counter(Device::DStats::READ_NUMBERS);
		  }
      readBs=reg.readValue();
			readedValue=readBs.data()[0];
    }
	  catch(const std::exception& e)
	  {
      if (stats!=nullptr){
        stats->incremente_counter(Device::DStats::READ_FAILURES);
      }
      continue;
    }
    if (stats!=nullptr){
      stats->incremente_counter(Device::DStats::READBACK_NUMBERS);
		}
    if (readedValue != value){
      if (stats!=nullptr){
        stats->incremente_counter(Device::DStats::READBACK_FAILURES);
      } 
      continue;
    }
		if (retry!=0){
			LOG(Log::WRN) << logId << "Writing  SUCCES after " << retry + 1 << " tries";
		}
		return OpcUa_Good;
  }
  if( flags &  I2C_IGNORE_ERROR){
	  return OpcUa_Good;
  }
  LOG(Log::ERR)<< logId << "Writing  Failed after " << retry << " retries";
  return OpcUa_Bad;
}

UaStatus secureGpioWrite( UaoClientForOpcUaSca::DigitalIO gpio , OpcUa_Boolean value, std::string nodeId,std::string logID, int MaxRetry){


  int retry=0;
  for (retry=0; retry <  MaxRetry; retry++){
	  try{
		  LOG(Log::TRC)<< logID << "Gpio wrrite in "  << nodeId << " :" << (int) value;
		  //Writing
		  gpio.writeValue(value);
		  return OpcUa_Good;
	  }
	  catch(const UaoClientForOpcUaSca::Exceptions::BadStatusCode& e)
	    {
		   LOG(Log::INF)<< logID << nodeId <<"Caught(BadStatusCode):"<<e.what();
	    }
	    catch(const std::exception& e)
	    {
	    	LOG(Log::INF)<< logID << nodeId <<"Caught:"<<e.what();
	    }


  }
  LOG(Log::ERR)<< logID << nodeId << "Gpio write  Failed after " << retry << " retries";
  return OpcUa_Bad;
}


Log::LOG_LEVEL logStrtoLog(std::string value){

	  if (value.compare("TRC")==0){
		  return Log::TRC;
	  }
	  else if (value.compare("INF")==0){
		  return Log::INF;
		  }
	  else if (value.compare("WRN")==0){
		  return Log::WRN;
		  }
	  else if (value.compare("ERR")==0){
		  return Log::ERR;
		  }
	  else{
		  return Log::INF;
	  }
  }


std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

std::vector<std::string> str_split (std::string s, std::string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find (delimiter, pos_start)) != std::string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}


float readFromFelix(std::string id,int card,std::string parameter){
  std::string cmd= "flx-info -c " + std::to_string(card) + " FPGA | grep '" + parameter + "' | awk -F ':' '{print $2}' | awk -F ' ' '{print $1}'";
   float var;
  try{
    var = std::stof(exec(cmd.c_str()));
  }
  catch (const std::exception& e){
    return 0;
  }
  if (var != 0 && var < 5 ){
	  return -1;
  }
  return var;
}
