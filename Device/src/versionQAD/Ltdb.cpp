#include "Ltdb.h"
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using namespace std;
using boost::property_tree::ptree;

int generalXml(Ltdb &ltdb);
int specificXml(Ltdb &ltdb);

void ltdbPrint(Ltdb ltdb){
  cout << "--------Ltdb" << endl;
  for (int scaId =1; scaId<=5; scaId++){
    cout << "----Sca " << scaId << endl;
    for (int mtxId =1; mtxId<=4; mtxId++){
      cout << "  --Mtx " << mtxId << endl;
      cout << "    Register01: 0x"<< hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register01 << dec << endl;
      cout << "    Register02: 0x"<< hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register02 << dec << endl;
      cout << "    Register03: 0x"<< hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register03 << dec << endl;
      cout << "    Register04: 0x"<< hex <<(int) ltdb.sca[scaId-1].mtx[mtxId-1].register04 << dec << endl;
      if (ltdb.sca[scaId-1].mtx[mtxId-1].degMode){
        cout << "    Mode:DEGRADED" << endl;
      }
      else{
        cout << "    Mode: NORMAL" << endl;
      }
    }  
    LOG(Log::INF) << "  --GBTx " << std::endl;
	  if (ltdb.sca[scaId-1].gbtx.mode == Device::DLtdbGBTx::STABLE_SCA_MODE){
		  LOG(Log::INF) << "    Mode:STABLE_SCA_MODE" << std::endl;
	    LOG(Log::INF) << "    Register35: 0x"<< std::hex <<(int) ltdb.sca[scaId-1].gbtx.register35 << std::dec << std::endl;
    }
    else{
    	LOG(Log::INF) << "    Mode: NORMAL" << std::endl;
		  LOG(Log::INF) << "    Default config used" << std::endl;
    }
    cout << "  --Mtrx " << endl;
    cout << "    Register01: 0x"<< hex <<(int) ltdb.sca[scaId-1].mtrx.register01 << dec << endl;
    cout << "    Register02: 0x"<< hex <<(int) ltdb.sca[scaId-1].mtrx.register02 << dec << endl;
    for (int locxId =1; locxId<=4; locxId++){
      cout << "  --Locx " << locxId << endl;
      cout << "    Register01: 0x"<< hex <<(int) ltdb.sca[scaId-1].locx2[locxId-1].register01 << dec << endl;
      cout << "    Register02: 0x"<< hex <<(int) ltdb.sca[scaId-1].locx2[locxId-1].register02 << dec << endl;
    }
  }

}
int main(){
  Ltdb ltdb1;
  ltdbPrint(ltdb1);
  cout << "---------------------------------------------------------------------" << endl; 
  generalXml(ltdb1);
  cout << "---------------------------------------------------------------------" << endl; 
  ltdbPrint(ltdb1);
  cout << "---------------------------------SPECIFIC LOAD -------------------------------" << endl; 
  specificXml(ltdb1);
  cout << "------------------------------------PRINT FINAL---------------------------------" << endl; 
  ltdbPrint(ltdb1);
  return 0;
}


int generalXml(Ltdb &ltdb)
{
  uint8_t defRegMtx[4];
  uint8_t defRegGbtx[1];
  uint8_t defRegMtrx[2];
  uint8_t defRegLocx[2];
  bool defModeMtx;
  ptree pt;
  read_xml("generalConfig.xml",pt);
  for (ptree::iterator pos = pt.begin(); pos != pt.end(); pos++) {
    cout << pos->first <<"::" << endl;
   // cout << pos->first <<"::" <<pos->second.get_child("mtx").data() << endl;

    ptree &subtree = pos->second;
    for (ptree::iterator itDevice = subtree.begin(); itDevice != subtree.end(); itDevice++) {
      if(itDevice->first == "mtx"){
        cout << "   "<< itDevice->first << ":: mode: " << itDevice->second.get_child("<xmlattr>.mode").get_value<string>()<< endl;
        ptree &mtxtree = itDevice->second;
        if(itDevice->second.get_child("<xmlattr>.mode").get_value<string>() =="normal"){
          defModeMtx=NORMAL;
        }
        else if(itDevice->second.get_child("<xmlattr>.mode").get_value<string>() =="degraded"){
          defModeMtx=DEGRADED;
        }
        else {
          cout << "ERR:Unknow mode" << endl;
        }
        for (ptree::iterator itMtx = mtxtree.begin(); itMtx != mtxtree.end(); itMtx++) {
          if(itMtx->first == "register"){
            cout << "   "<< itMtx->first << endl;
            cout << "   "<< itMtx->first << ":: id: " << itMtx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
            cout << "   "<< itMtx->first << ":: value:0x "<< hex  << stoi(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
            defRegMtx[itMtx->second.get_child("<xmlattr>.id").get_value<int>()-1]=(uint8_t) stoi(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);          
          }
        }
      }

      else if(itDevice->first == "gbtx"){
        ptree &gbtxtree = itDevice->second;
        for (ptree::iterator itGbtx = gbtxtree.begin(); itGbtx != gbtxtree.end(); itGbtx++) {
          if(itGbtx->first == "register"){
            cout << "   "<< itGbtx->first << endl;
            cout << "   "<< itGbtx->first << ":: id: " << itGbtx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
            cout << "   "<< itGbtx->first << ":: value:0x "<< hex  << stoi(itGbtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
            defRegGbtx[itGbtx->second.get_child("<xmlattr>.id").get_value<int>()-1]=(uint8_t) stoi(itGbtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);          
          }
        }
      }

      else if(itDevice->first == "mtrx"){
        ptree &mtrxtree = itDevice->second;
        for (ptree::iterator itMtrx = mtrxtree.begin(); itMtrx != mtrxtree.end(); itMtrx++) {
          if(itMtrx->first == "register"){
            cout << "   "<< itMtrx->first << endl;
            cout << "   "<< itMtrx->first << ":: id: " << itMtrx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
            cout << "   "<< itMtrx->first << ":: value:0x "<< hex  << stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
            defRegMtrx[itMtrx->second.get_child("<xmlattr>.id").get_value<int>()-1]=(uint8_t) stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);          
          }
        }
      }
      else if(itDevice->first == "locx2"){
        ptree &locxtree = itDevice->second;
        for (ptree::iterator itLocx = locxtree.begin(); itLocx != locxtree.end(); itLocx++) {
          if(itLocx->first == "register"){
            cout << "   "<< itLocx->first << endl;
            cout << "   "<< itLocx->first << ":: id: " << itLocx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
            cout << "   "<< itLocx->first << ":: value:0x "<< hex  << stoi(itLocx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
            defRegLocx[itLocx->second.get_child("<xmlattr>.id").get_value<int>()-1]=(uint8_t) stoi(itLocx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);          
          }
        }
      }
    }
            //  cout << pos->get<string>("<xmlattr>.name");
  }


  for (int scaId =1; scaId<=5; scaId++){
    for (int mtxId =1; mtxId<=4; mtxId++){
      ltdb.sca[scaId-1].mtx[mtxId-1].register01 = defRegMtx[0];
      ltdb.sca[scaId-1].mtx[mtxId-1].register02 = defRegMtx[1];
      ltdb.sca[scaId-1].mtx[mtxId-1].register03 = defRegMtx[2];
      ltdb.sca[scaId-1].mtx[mtxId-1].register04 = defRegMtx[3];
    }
    ltdb.sca[scaId-1].gbtx.register35 = defRegGbtx[0];
    ltdb.sca[scaId-1].mtrx.register01 = defRegMtrx[0];
    ltdb.sca[scaId-1].mtrx.register02 = defRegMtrx[1];
    for (int locxId =1; locxId<=4; locxId++){
      ltdb.sca[scaId-1].locx2[locxId-1].register01 = defRegLocx[0];
      ltdb.sca[scaId-1].locx2[locxId-1].register02 = defRegLocx[1];
    }
  }
  return 0;
  }

int specificXml(Ltdb &ltdb)
{
  ptree pt;
  read_xml("specialConfig.xml",pt);
  for (ptree::iterator pos = pt.begin(); pos != pt.end(); pos++) {
    cout << pos->first <<"::" << endl;
    ptree &subtree = pos->second;
    for (ptree::iterator itSca = subtree.begin(); itSca != subtree.end(); itSca++) {
      if(itSca->first == "sca"){
        ptree &scatree = itSca->second;
        int scaId=itSca->second.get_child("<xmlattr>.id").get_value<int>();
        cout << "   "<< itSca->first << ":: id: " << scaId << endl;
      for (ptree::iterator itDevice = scatree.begin(); itDevice != scatree.end(); itDevice++) {
        if(itDevice->first == "mtx"){
          int mtxId=itDevice->second.get_child("<xmlattr>.id").get_value<int>();
          cout << "   "<< itDevice->first << ":: id: " << mtxId <<endl;
          ptree &mtxtree = itDevice->second;
          for (ptree::iterator itMtx = mtxtree.begin(); itMtx != mtxtree.end(); itMtx++) {
            if(itMtx->first == "register"){
              cout << "   "<< itMtx->first << endl;
              cout << "   "<< itMtx->first << ":: id: " << itMtx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
              cout << "   "<< itMtx->first << ":: value:0x "<< hex  << stoi(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
              int registerId=itMtx->second.get_child("<xmlattr>.id").get_value<int>();
              switch (registerId){
                case 1:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register01 = (uint8_t) stoi(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
                case 2:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register02 = (uint8_t) stoi(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
                case 3:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register03 = (uint8_t) stoi(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
                case 4:
                  ltdb.sca[scaId-1].mtx[mtxId-1].register04 = (uint8_t) stoi(itMtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                  break;
              }  
            }
            else if(itMtx->first == "<xmlattr>"){
              ptree &attrtree = itMtx->second;
              for (ptree::iterator itAttr = attrtree.begin(); itAttr != attrtree.end(); itAttr++) {
                if (itAttr->first == "mode"){
                  cout <<"   " << itDevice->first << ":: mode: "<< itAttr->second.data() << endl;
                  if(itAttr->second.get<string>("") =="degraded"){
                    cout << "Sca " << scaId << " mtx " << mtxId << " : degrated"<< endl;
                    ltdb.sca[scaId-1].mtx[mtxId-1].degMode = DEGRADED;
                    
                  }
                  else if(itAttr->second.get<string>("") =="normal"){
                    cout << "Sca " << scaId << " mtx " << mtxId << " : normal"<< endl;
                    ltdb.sca[scaId-1].mtx[mtxId-1].degMode = NORMAL;
                  }

                }
              }
            }
          }
        }

        else if(itDevice->first == "gbtx"){
          int gbtxId = itDevice->second.get_child("<xmlattr>.id").get_value<int>();
          cout << "   "<< itDevice->first << ":: id: " << gbtxId <<endl;

          ptree &gbtxtree = itDevice->second;
          for (ptree::iterator itGbtx = gbtxtree.begin(); itGbtx != gbtxtree.end(); itGbtx++) {
            if(itGbtx->first == "register"){
              cout << "   "<< itGbtx->first << endl;
              cout << "   "<< itGbtx->first << ":: id: " << itGbtx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
              cout << "   "<< itGbtx->first << ":: value:0x "<< hex  << stoi(itGbtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
              int registerId=itGbtx->second.get_child("<xmlattr>.id").get_value<int>();
              switch (registerId){
                case 1:
                  ltdb.sca[scaId-1].gbtx.register35 = (uint8_t) stoi(itGbtx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
              }
            }
          }
        }

        else if(itDevice->first == "mtrx"){
          int mtrxId = itDevice->second.get_child("<xmlattr>.id").get_value<int>();
          cout << "   "<< itDevice->first << ":: id: " << mtrxId <<endl;

          ptree &mtrxtree = itDevice->second;
          for (ptree::iterator itMtrx = mtrxtree.begin(); itMtrx != mtrxtree.end(); itMtrx++) {
            if(itMtrx->first == "register"){
              cout << "   "<< itMtrx->first << endl;
              cout << "   "<< itMtrx->first << ":: id: " << itMtrx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
              cout << "   "<< itMtrx->first << ":: value:0x "<< hex  << stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
              int registerId=itMtrx->second.get_child("<xmlattr>.id").get_value<int>();
              switch (registerId){
                case 1:
                  ltdb.sca[scaId-1].mtrx.register01 = (uint8_t) stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
                case 2:
                  ltdb.sca[scaId-1].mtrx.register02 = (uint8_t) stoi(itMtrx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
              }
            }
          }
        }
        else if(itDevice->first == "locx2"){
          int locxId = itDevice->second.get_child("<xmlattr>.id").get_value<int>();
          cout << "   "<< itDevice->first << ":: id: " << locxId <<endl;
          ptree &locxtree = itDevice->second;
          for (ptree::iterator itLocx = locxtree.begin(); itLocx != locxtree.end(); itLocx++) {
            if(itLocx->first == "register"){
              cout << "   "<< itLocx->first << endl;
              cout << "   "<< itLocx->first << ":: id: " << itLocx->second.get_child("<xmlattr>.id").get_value<int>()<< endl;
              cout << "   "<< itLocx->first << ":: value:0x "<< hex  << stoi(itLocx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0)<< dec << endl;
              int registerId=itLocx->second.get_child("<xmlattr>.id").get_value<int>();
              switch (registerId){
                case 1:
                  ltdb.sca[scaId-1].locx2[locxId-1].register01 = (uint8_t) stoi(itLocx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
                case 2:
                  ltdb.sca[scaId-1].locx2[locxId-1].register02 = (uint8_t) stoi(itLocx->second.get_child("<xmlattr>.value").get_value<string>(),nullptr,0);
                break;
              }
            }
          }
        }
      }
    }
  }           //  cout << pos->get<string>("<xmlattr>.name");
  }


  return 0;
  }

