#ifndef LTDB_H
#define LTDB_H

#include <cstdint>

#define DEGRADED true
#define NORMAL false

typedef struct mtx{
  uint8_t register01=0;
  uint8_t register02=0;
  uint8_t register03=0;
  uint8_t register04=0;
  bool degMode=false;
}Mtx;

typedef struct mtrx{
  uint8_t register01=0;
  uint8_t register02=0;
}Mtrx;

typedef struct locx{
  uint8_t register01=0;
  uint8_t register02=0;
}Locx;

typedef struct sca{
  Mtx mtx[4];
  Locx locx2[4];
  Mtrx mtrx;
}Sca;

typedef struct ltdb{
  Sca sca[5];
}Ltdb;


#endif
