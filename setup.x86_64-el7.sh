# Usage: source setup.sh

  # using consistent LCG installation for the different packages we take from LCG
  LCG_version="LCG_102b_ATLAS_11"
  
  if [[ ${HOSTNAME} == *"pc-lar-felix-ltdb"* ]]; then
    # paths valid at point 1
    echo "Setup paths for P1 servers (without cvmfs access)"
    PATH_TO_LCG=/sw/atlas/sw/lcg/releases/
    PATH_TO_TDAQ_COMMON=/sw/atlas/tdaq-common/tdaq-common-10-00-00/
  else
    # paths valid elsewhere
    echo "Setup paths for servers outside P1 (with cvmfs access)"
    PATH_TO_LCG=/cvmfs/sft.cern.ch/lcg/releases/
    PATH_TO_TDAQ_COMMON=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-10-00-00/
    # the latter are only needed for compilation, which we don't do at point 1 anyway
    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.26.2/Linux-x86_64/bin:$PATH
    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/gcc/11.3.0/x86_64-centos7-gcc11-opt/bin:$PATH
    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/binutils/latest/x86_64-centos7/bin:$PATH
  fi

  # setup appropriate python version - coral needs python 3.9
  export PYTHON_LIB_PATH=${PATH_TO_LCG}Python/3.9.6-b0f98/x86_64-centos7-gcc11-opt/lib
  export PYTHON_INCLUDE_PATH=${PATH_TO_LCG}Python/3.9.6-b0f98/x86_64-centos7-gcc11-opt/include
  
  # protobuf
  export PROTOBUF_HEADERS=${PATH_TO_LCG}protobuf/2.5.0-aa8bd/x86_64-centos7-gcc11-opt/include
  export PROTOBUF_DIRECTORIES=${PATH_TO_LCG}protobuf/2.5.0-aa8bd/x86_64-centos7-gcc11-opt/lib
  export PROTOBUF_LIBS="-lprotobuf"

  # compiler and boost
  export CXX="g++"
  export CC="gcc"
  export BOOST_PATH=${PATH_TO_LCG}${LCG_version}/Boost/1.78.0/x86_64-centos7-gcc11-opt/

  # xercesc
  export XERCES_DIRECTORIES=${PATH_TO_LCG}${LCG_version}/XercesC/3.2.3/x86_64-centos7-gcc11-opt/lib
  export XERCES_HEADERS=${PATH_TO_LCG}${LCG_version}/XercesC/3.2.3/x86_64-centos7-gcc11-opt/include

  # opcua toolkit
  export OPCUA_TOOLKIT_PATH=/winccoa/rpms/toolkits/uasdk-1.6.5-gcc11.1.0-with_OPCUA-2561-el7

  # commond libs and includes from tdaq
  export TDAQ_EXTERNALS_LIB_PATH=${PATH_TO_TDAQ_COMMON}installed/external/x86_64-centos7-gcc11-opt/lib
  export TDAQ_EXTERNALS_INCLUDE_PATH=${PATH_TO_TDAQ_COMMON}installed/external/x86_64-centos7-gcc11-opt/include

  # to use COOL and CORAL
  export COOL_LIB_PATH=${TDAQ_EXTERNALS_LIB_PATH}
  export COOL_INCLUDE_PATH=${TDAQ_EXTERNALS_INCLUDE_PATH}
  export CORAL_LIB_PATH=${TDAQ_EXTERNALS_LIB_PATH}
  export CORAL_INCLUDE_PATH=${TDAQ_EXTERNALS_INCLUDE_PATH}

  # specific versions of oracle, mysql, and frontier_client needed by CORAL
  export ORACLE_LIB_PATH=${PATH_TO_LCG}${LCG_version}/oracle/19.11.0.0.0/x86_64-centos7-gcc11-opt/lib
  export ORACLE_INCLUDE_PATH=${PATH_TO_LCG}${LCG_version}/oracle/19.11.0.0.0/x86_64-centos7-gcc11-opt/include
  export MYSQL_LIB_PATH=${PATH_TO_LCG}${LCG_version}/mysql/10.5.13/x86_64-centos7-gcc11-opt/lib
  export MYSQL_INCLUDE_PATH=${PATH_TO_LCG}${LCG_version}/mysql/10.5.13/x86_64-centos7-gcc11-opt/include
  export FRONTIER_LIB_PATH=${PATH_TO_LCG}${LCG_version}/frontier_client/2.9.1/x86_64-centos7-gcc11-opt/lib
  export FRONTIER_INCLUDE_PATH=${PATH_TO_LCG}${LCG_version}/frontier_client/2.9.1/x86_64-centos7-gcc11-opt/include

  ### update LD_LIBRARY_PATH with libraries needed at runtime
  export LD_LIBRARY_PATH=${COOL_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${BOOST_PATH}/lib:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${XERCES_DIRECTORIES}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${ORACLE_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${MYSQL_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${FRONTIER_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${PYTHON_LIB_PATH}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=${TDAQ_EXTERNALS_LIB_PATH}:$LD_LIBRARY_PATH

  # the last export is needed due to gcc version available by default at point 1
  if [[ ${HOSTNAME} == *"pc-lar-felix-ltdb"* ]]; then
    export LD_LIBRARY_PATH=/sw/atlas/tdaq/production/installed/external/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH
  fi
