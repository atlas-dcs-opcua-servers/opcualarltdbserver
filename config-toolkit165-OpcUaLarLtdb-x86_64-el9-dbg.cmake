# LICENSE:
# Copyright (c) 2015, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Author: Paris Moschovakos
# @author pmoschov
# @date 29-Jan-2019
# The purpose of this file is to set default parameters in case no build configuration file (aka toolchain) was specified.

# The approach is to satisfy the requirements as much as possible.

#SET( CMAKE_CXX_COMPILER $ENV{CXX} )
#SET( CMAKE_CC_COMPILER $ENV{CC} )

#-------------------
# LAR LTDB Specific
#-------------------

#-------
#Boost - use the one defined by ATCA-SW in its setup_paths
#-------
#if(DEFINED ENV{BOOST_PATH_HEADERS})
#    message ("BOOST_PATH_HEADERS is in your environment, taking it into account")
#    if((NOT DEFINED ENV{BOOST_PATH_LIBS}) OR (NOT DEFINED ENV{BOOST_LIBS}))
#        message (FATAL_ERROR "You have BOOST_PATH_HEADERS in your environment, but you are missing one of this: BOOST_PATH_LIBS BOOST_LIBS    Please fix it or use custom build configuration file ")
#    endif((NOT DEFINED ENV{BOOST_PATH_LIBS}) OR (NOT DEFINED ENV{BOOST_LIBS}))
#    SET( BOOST_PATH_HEADERS $ENV{BOOST_PATH_HEADERS} )
#    SET( BOOST_PATH_LIBS $ENV{BOOST_PATH_LIBS} )
#    SET( BOOST_LIBS $ENV{BOOST_LIBS} )
#else(DEFINED ENV{BOOST_PATH_HEADERS})   
#    message ("Assuming standard Boost installation as no BOOST_PATH_HEADERS is defined in your environment")
#    SET( BOOST_PATH_HEADERS "" )
#    SET( BOOST_PATH_LIBS "" )
#    if( NOT DEFINED ENV{BOOST_LIBS} )
#      SET( BOOST_LIBS "-lboost_program_options-mt -lboost_thread-mt -lpthread" )
#    else()
#      SET( BOOST_LIBS $ENV{BOOST_LIBS} )
#    endif()
#endif(DEFINED ENV{BOOST_PATH_HEADERS})
#
#message ("using BOOST_LIBS [${BOOST_LIBS}]")

#------
#OPCUA
#------
if(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    message ("Taking OPC UA Toolkit path from the environment: $ENV{OPCUA_TOOLKIT_PATH}")
    SET( OPCUA_TOOLKIT_PATH $ENV{OPCUA_TOOLKIT_PATH} )
else(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    SET( OPCUA_TOOLKIT_PATH "/winccoa/rpms/toolkits/OpcUaToolkit-static-1.6.5" )
endif(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    
set(OPCUA_TOOLKIT_LIBS_DEBUG
    "-luaclientcpp -luamodule -lcoremodule -luabasecpp -luastack -luapkicpp -lxmlparsercpp -lxml2 -lssl -lcrypto -lpthread -lrt"
)
set(OPCUA_TOOLKIT_LIBS_RELEASE
    "-luaclientcpp -luamodule -lcoremodule -luabasecpp -luastack -luapkicpp -lxmlparsercpp -lxml2 -lssl -lcrypto -lpthread -lrt"
)

include_directories(
  ${OPCUA_TOOLKIT_PATH}/include/uastack ${OPCUA_TOOLKIT_PATH}/include/uabasecpp
  ${OPCUA_TOOLKIT_PATH}/include/uaservercpp ${OPCUA_TOOLKIT_PATH}/include/uapkicpp
  ${OPCUA_TOOLKIT_PATH}/include/xmlparsercpp ${OPCUA_TOOLKIT_PATH}/include)

add_custom_target(quasar_opcua_backend_is_ready)

#-----
#XML Libs
#-----

SET( XML_LIBS -lxerces-c )

#----
#COOL and CORAL
#----
set( COOL_INCLUDE_DIR $ENV{COOL_INCLUDE_PATH} )
set( CORAL_INCLUDE_DIR $ENV{CORAL_INCLUDE_PATH} )
set( COOL_LINK_DIR $ENV{COOL_LIB_PATH} )
set( CORAL_LINK_DIR $ENV{CORAL_LIB_PATH} )
set( COOL_LINK_LIBRARIES lcg_CoolApplication lcg_CoolKernel lcg_RelationalCool )
set( CORAL_LINK_LIBRARIES lcg_ConnectionService lcg_CoralMonitor lcg_CoralStubs lcg_OracleAccess lcg_XMLAuthenticationService lcg_CoralAccess lcg_CoralServerBase  lcg_EnvironmentAuthenticationService lcg_PyCoral lcg_XMLLookupService lcg_CoralBase lcg_CoralServerProxy lcg_FrontierAccess lcg_RelationalAccess lcg_CoralCommon lcg_CoralServer lcg_MonitoringService lcg_RelationalService lcg_CoralKernel lcg_CoralSockets lcg_MySQLAccess lcg_SQLiteAccess )

#----
# oracle, mysql, and frontier_client, which are CORAL dependencies
#----
set( ORACLE_INCLUDE_DIR $ENV{ORACLE_INCLUDE_PATH} )
set( MYSQL_INCLUDE_DIR $ENV{MYSQL_INCLUDE_PATH} )
set( FRONTIER_INCLUDE_DIR $ENV{FRONTIER_INCLUDE_PATH} )
set( ORACLE_LINK_DIR $ENV{ORACLE_LIB_PATH} )
set( MYSQL_LINK_DIR $ENV{MYSQL_LIB_PATH} )
set( FRONTIER_LINK_DIR $ENV{FRONTIER_LIB_PATH} )

#-----
#General settings
#-----

add_definitions(-Wall -DBACKEND_UATOOLKIT -Wno-deprecated -std=gnu++20 -Wno-literal-suffix -Wno-unused-local-typedefs ) 

#-----
#protobuf
#-----
SET( LTDBSW_LIBS
  $ENV{PROTOBUF_DIRECTORIES}/libprotobuf.so -lprotobuf
  $ENV{XERCES_DIRECTORIES}/libxerces-c.so -lxerces-c
)
SET( LTDBSW_HEADERS
  $ENV{PROTOBUF_HEADERS}
  $ENV{XERCES_HEADERS}
)
SET( LTDBSW_LIBDIRS
  $ENV{PROTOBUF_DIRECTORIES}
  $ENV{XERCES_DIRECTORIES}
)


set(CMAKE_BUILD_TYPE Debug)

#add_definitions(-Wall -Wno-deprecated) 
#add_compile_definitions(_GLIBCXX_USE_CXX11_ABI=0)
# open62541-compat has no uatrace
#set (LOGIT_HAS_UATRACE FALSE)

# need C++11
#set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17" )

#-------
# Ccache
#-------
if(NOT DEFINED CMAKE_CXX_COMPILER_LAUNCHER)
  find_program(CCACHE_PROGRAM ccache)
  if (CCACHE_PROGRAM)
    set(CMAKE_CXX_COMPILER_LAUNCHER "${CCACHE_PROGRAM}" CACHE STRING "Compiler launcher for CXX")
  endif()
endif()

