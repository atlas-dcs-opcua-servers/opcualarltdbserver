#!/bin/sh
echo '<configuration xmlns="http://cern.ch/quasar/Configuration"'
echo '  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
echo '  xsi:schemaLocation="http://cern.ch/quasar/Configuration ./Configuration.xsd">'
echo '  <LArBackend name="larbackend" logLevel="INF" >'

i=0
ltdb=0
while read crate ltdbName configFile opcUaEndPoint calibFilesPath mappingPath connectionLatomeFile scaTab[1] scaTab[2] scaTab[3] scaTab[4] scaTab[5];
do
i=$(($i+1))
if [ $i -gt 1 ]; then
  ltdb=$((ltdb+1))
  echo "    <Ltdb name=\"ltdb_${crate}\""
  echo "      id=\"$ltdb\""
  echo '      generalConfigFile="./generalConfigFile.xml"'
  echo "      specificLtdbConfigFile=\"$configFile\""
  echo "      scaOpcUaEndpoints=\"$opcUaEndPoint\""
  echo "      ltdb_name=\"${ltdbName}_$crate\""
  echo "      calibFilesPath=\"$calibFilesPath\""
  echo "      connectionLatomeFile=\"$connectionLatomeFile\">" 
  echo '      <StatusLatome name="StatusLatome" id="1" delayForCheckErrorCounter="5"></StatusLatome>'
  echo '      <ClkDesScan name="ClkDesScan"></ClkDesScan>'
  echo '      <Locx2Scan name="Locx2Scan" delayBeginScan="9" delayEndScan="25" delayBeginAutoScan="17" delayEndAutoScan="25" ></Locx2Scan>'
  echo "      <Mapping name="Mapping" mappingPath=\"$mappingPath\"></Mapping>"
  echo '      <StressTest name="stressTest"></StressTest>'
  for sca in `seq 1 5`;
  do
    echo "      <LtdbSca name=\"Sca_$sca\" id=\"$sca\""
    echo '        mtxConfigurationTries="5"'
    echo "        chipId=\"${scaTab[sca]}\">"
    echo '        <LtdbGpio name="gpio"></LtdbGpio>'
    echo '        <LtdbMtrx name="Mtrx"></LtdbMtrx>'
    echo '        <LtdbGBTx name="GBTx"></LtdbGBTx>'
    echo '        <LtdbMTx name="MTx_1" id="1" ></LtdbMTx>'
    echo '        <LtdbMTx name="MTx_2" id="2" ></LtdbMTx>'
    echo '        <LtdbMTx name="MTx_3" id="3" ></LtdbMTx>'
    echo '        <LtdbMTx name="MTx_4" id="4" ></LtdbMTx>'
    for locx in `seq 1 4`;
    do
      echo "        <LtdbLOCx2 name=\"LOCx2_$locx\" id=\"$locx\">"
      echo '          <LtdbFiber name="Fiber_1" fiberPos="1"></LtdbFiber>'
      echo '          <LtdbFiber name="Fiber_2" fiberPos="2"></LtdbFiber>'
      echo '          <LtdbADCNevis name="ADC_NEVIS_1" id="1"></LtdbADCNevis>'
      echo '          <LtdbADCNevis name="ADC_NEVIS_2" id="2"></LtdbADCNevis>'
      echo '          <LtdbADCNevis name="ADC_NEVIS_3" id="3"></LtdbADCNevis>'
      echo '          <LtdbADCNevis name="ADC_NEVIS_4" id="4"></LtdbADCNevis>'
      echo '        </LtdbLOCx2>'
    done
    echo "      </LtdbSca>"
  done #SCA
  echo '    </Ltdb>'
fi
  echo '  </LArBackend>'
  echo '</configuration>'
done < $1

